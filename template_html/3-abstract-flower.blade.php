<div class="gla_slider gla_image_bck gla_fixed" data-stellar-background-ratio="0.2" data-color="#efe5dd">
    <div class="gla_slider_flower">
         <div class="gla_slider_flower_c1 gla_slider_flower_item" data-start="top:0px; left:0px" data-200-start="top:-300px; left:-300px"></div>
        <div class="gla_slider_flower_c2 gla_slider_flower_item" data-start="top:0px; right:0px" data-200-start="top:-300px; right:-300px"></div>
        <div class="gla_slider_flower_c3 gla_slider_flower_item" data-start="bottom:0px; right:0px" data-200-start="bottom:-300px; right:-300px"></div>
        <div class="gla_slider_flower_c4 gla_slider_flower_item" data-start="bottom:0px; left:0px" data-200-start="bottom:-300px; left:-300px"></div>
        <div class="gla_slider_flower_c5 gla_slider_flower_item" data-start="top:0px; left:50%" data-200-start="top:-300px; left:50%"></div>
        <div class="gla_slider_flower_c6 gla_slider_flower_item" data-start="bottom:0px; left:50%" data-200-start="bottom:-300px; left:50%"></div>
    </div>

    <div class="gla_over" data-color="#9abab6" data-opacity="0"></div>
    <div class="container">
        <div class="gla_slide_txt gla_slide_center_middle text-center">
            <div class="gla_slide_midtitle">~*01*~<br>~*02*~</div>
        </div>
    </div>
    <a class="gla_scroll_down gla_go" href="#gla_content">
        <b></b>
        Scroll
    </a>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section gla_lg_padding gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.2" data-image="~*03*~">
         <div class="gla_slider_flower" data-bottom-top="@class:active" data--200-bottom="@class:no_active">
            <div class="gla_slider_flower_c1 gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c2 gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c3 gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c4 gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c5 gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c6 gla_slider_flower_item"></div>
        </div>
        <div class="container text-center">
            <p><img src="~*04*~" data-bottom-top="@src:~*04*~" height="150" alt=""></p>
            <h2>~*05*~</h2>
            <p>~*06*~</p>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed" data-stellar-background-ratio="0.2" data-color="#fff">
        <div class="container text-center">
            <p><img src="~*07*~" data-bottom-top="@src:~*07*~" height="150" alt=""></p>
            <h2>~*02*~</h2>
            <h3 class="gla_subtitle">~*08*~<br>~*09*~</h3>
            <div class="gla_countdown" data-year="~*10*~" data-month="~*11*~" data-day="~*12*~"></div>
        </div>
    </section>

    <section class="gla_section gla_lg_padding gla_image_bck" data-color="#efe5dd">
        <div class="gla_slider_flower" data-bottom-top="@class:active" data--200-bottom="@class:no_active">
             <div class="gla_slider_flower_c1 gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c2 gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c3 gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c4 gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c5 gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c6 gla_slider_flower_item"></div>
        </div>
        <div class="container text-center">
            <p><img src="~*13*~" data-bottom-top="@src:~*13*~" height="150" alt=""></p>
            <div class="gla_slide_midtitle">~*14*~</div>
            <p>~*15*~</p>
            <p><a href="#" class="btn">~*16*~</a></p>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.8" data-image="~*17*~">
        <div class="gla_over" data-color="#1e1d2d" data-opacity="0.4"></div>
        <div class="container text-center">
            <h2>~*18*~</h2>
            <p>~*19*~</p>
        </div>
    </section>

    <section class="gla_section gla_image_bck">
        <div class="container text-center">
            <h2>~*20*~</h2>
            <p>~*21*~</p>
            <div class="gla_portfolio_no_padding grid">
                ~*22*~
             </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.2" data-image="~*23*~">
        <div class="gla_over" data-color="#000" data-opacity="0.4"></div>
        <div class="container text-center">
            <p><img src="~*24*~" alt="" height="200" data-bottom-top="@src:~*24*~"></p>
        </div>
    </section>
</section>
