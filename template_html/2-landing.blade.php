<div class="gla_slider gla_image_bck  gla_wht_txt gla_fixed"  data-image="~*01*~" data-stellar-background-ratio="0.4">
    <div class="gla_over" data-color="#282828" data-opacity="0.5"></div>
    <div class="container">
        <div class="gla_slide_txt gla_slide_center_bottom text-center">
            <p><img src="~*02*~"  alt="" height="350"></p>
        </div>
    </div>
    <a class="gla_scroll_down gla_go" href="#gla_content">
        <b></b>
        Scroll
    </a>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section">
        <div class="container text-center">
            <div class="row text-center">
                <div class="col-md-4 gla_round_block">
                    <div class="gla_round_im gla_image_bck" data-image="~*03*~"></div>
                    <h3>~*04*~</h3>
                </div>
                <div class="col-md-4 gla_round_block">
                    <p><img src="~*05*~" data-bottom-top="@src:~*05*~" height="150" alt=""></p>
                    <h3>Are getting married<br> ~*06*~</h3>
                    ~*07*~ <br> ~*08*~
                </div>
                <div class="col-md-4 gla_round_block">
                    <div class="gla_round_im gla_image_bck" data-image="~*09*~"></div>
                    <h3>~*10*~</h3>
                </div>
            </div>
            <div class="gla_countdown" data-year="~*11*~" data-month="~*12*~" data-day="~*13*~"></div>
        </div>
    </section>

    <section class="gla_section gla_image_bck" data-color="#ecf2f0">
        <div class="container">
            <div class="row gla_auto_height">
                <div class="col-md-6 gla_image_bck" data-color="#fff">
                    <div class="gla_simple_block">
                        <h2>~*10*~</h2>
                        <p>~*15*~</p>
                        <div class="gla_footer_social">
                            <a href="#"><i class="ti ti-facebook gla_icon_box"></i></a>
                            <a href="#"><i class="ti ti-instagram gla_icon_box"></i></a>
                            <a href="#"><i class="ti ti-google gla_icon_box"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 gla_image_bck" data-image="~*16*~">
                </div>
                <div class="col-md-6 col-md-push-6 gla_image_bck" data-color="#fff">
                    <div class="gla_simple_block">
                        <h2>~*04*~</h2>
                        <p>~*18*~</p>
                        <div class="gla_footer_social">
                            <a href=""><i class="ti ti-facebook gla_icon_box"></i></a>
                            <a href=""><i class="ti ti-instagram gla_icon_box"></i></a>
                            <a href=""><i class="ti ti-google gla_icon_box"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-md-pull-6 gla_image_bck" data-image="~*19*~">
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section">
        <div class="container text-center">
            <h2>When & Where</h2>
            <div class="gla_icon_boxes row text-left">
                ~*20*~
            </div>
        </div>
    </section>

    <section class="gla_section">
        <div class="gla_map">
            <iframe src="~*26*~" width="100%" height="480"></iframe>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.8" data-image="images/wedding/carita_lee/15320532559_ee3e0d021d_k.jpg">
        <div class="gla_over" data-color="#282828" data-opacity="0.8"></div>
        <div class="container text-center">
            <h2>Will You Attend?</h2>
            <h3 class="gla_subtitle">Please sign your RSVP</h3>
            <div class="row">
                <div class="col-md-8 col-md-push-2">
                    <form action="https://formspree.io/your@mail.com" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Your name*</label>
                                <input type="text" name="name" class="form-control form-opacity">
                            </div>
                            <div class="col-md-6">
                                <label>Your e-mail*</label>
                                <input type="text" name="email" class="form-control form-opacity">
                            </div>

                            <div class="col-md-6">
                                <label>Will you attend?</label>
                                <input type="radio" name="attend" value="Yes, I will be there">
                                <span>Yes, I will be there</span><br>
                                <input type="radio" name="attend" value="Sorry, I can't come">
                                <span>Sorry, I can't come</span>
                            </div>
                            <div class="col-md-6">
                                <label>Meal preference</label>
                                <select name="meal" class="form-control form-opacity">
                                    <option value="I eat anything">I eat anything</option>
                                    <option value="Beef">Beef</option>
                                    <option value="Chicken">Chicken</option>
                                    <option value="Vegetarian">Vegetarian</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label>Notes</label>
                                <textarea name="message" class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="btn submit" value="Send">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck" data-color="#fff">
        <div class="container text-center">
            <h2>~*27*~</h2>
            <div class="row text-center">
                <div class="col-md-3 gla_round_block">
                    <div class="gla_round_im gla_image_bck" data-image="~*28*~"></div>
                    <h4>~*29*~</h4>
                </div>
                <div class="col-md-3 gla_round_block">
                    <div class="gla_round_im gla_image_bck" data-image="~*30*~"></div>
                    <h4>~*31*~</h4>
                </div>
                <div class="col-md-3 gla_round_block">
                    <div class="gla_round_im gla_image_bck" data-image="~*32*~"></div>
                    <h4>~*33*~</h4>
                </div>
                <div class="col-md-3 gla_round_block">
                    <div class="gla_round_im gla_image_bck" data-image="~*34*~"></div>
                    <h4>~*35*~</h4>
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.8" data-image="~*36*~">
        <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
        <div class="container text-center">
            <p><img src="~*37*~" data-bottom-top="@src:~*37*~" height="150" alt=""></p>
            <h3>~*38*~</h3>
            <p class="gla_subtitle">— ~*39*~</p>
        </div>
    </section>
</section>
