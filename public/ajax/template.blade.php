<?php
require '../../connection/init.php';
$mysqli = mysqli_connect($domain, $username, $password, $database);

$query_template_design = "SELECT template_design.id AS 'id', template_design.nama AS 'nama', template_design.imgpreview AS 'imgpreview', template_design.html AS 'html'
                      FROM template_design
                      WHERE template_design.event_category_id = '4'";
$hasil_template_design = mysqli_query($mysqli, $query_template_design);
$template_design = array();
while($Hasil = mysqli_fetch_assoc($hasil_template_design))
{
  $template_design[] = $Hasil;
}

mysqli_close($mysqli);

?>
<div class="col-md-6">
    <label>Template* <small class="form-text text-muted"> Ex. parallax</small></label>
    <select name="id_template" id="id_template" class="form-control form-opacity" required>
        @foreach ($template_design as $td)
          <option value="{{$td['id']}}" {{(old('id_template',(isset($event)? $event->template_design_id : ''))==$td['id']) ? 'selected' : ''}}>{{$td['nama']}}</option>
        @endforeach
    </select>
</div>
