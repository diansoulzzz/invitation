<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


// Route::get('/', function(){
//   return 'asd';
// });
Route::post('login', 'Api\ApiController@Login');

Route::middleware('auth:api')->group(function () {
  Route::get('/', 'Api\ApiController@Index');
  Route::post('guest/scanqr', 'Api\ApiController@GuestScanQr');
  Route::post('guest/checkin', 'Api\ApiController@GuestCheckIn');
  Route::post('guest/list', 'Api\ApiController@GuestList');
  Route::post('guest/gift', 'Api\ApiController@GuestGift');
});
