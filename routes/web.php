<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::group(['prefix' => 'line'], function (){
  Route::get('linkaccount', 'Line\Bot\MessageController@LinkAccount');
  Route::get('webhook', 'Line\WebhookController@GetWebook');
  Route::post('webhook', 'Line\WebhookController@PostWebook');
  Route::group(['prefix' => 'bot'], function (){
    Route::get('reply', 'Line\Bot\MessageController@DoReply');
  });
});

/*Login*/
Route::get('login','Auth\LoginController@IndexLogin')->name('login');                                             // Done
Route::post('login','Auth\LoginController@Login');                                                                // Done
Route::post('register','Auth\LoginController@Register');                                                          // Done
// Route::get('sendemail','Auth\LoginController@SendEmail');                                                      // Done
Route::get('verification/{eid}','Auth\LoginController@VerifyEmail');                                              // Done
Route::get('logout','Auth\LoginController@Logout');                                                               // Done

/* Home */
Route::get('/','Home\HomeController@Index');                                                                      // Done

/*Time Line*/
Route::get('timeline','Timeline\TimelineController@Index');                                                       // Done, search blog, recent post, subscribe now

/*Category*/
Route::group(['prefix' => 'category'], function (){
  Route::get('{link_url}','Category\CategoryController@Index');                                                   // Done, belum ada isi yang sesuai
});

/*Search*/
Route::get('search','Search\SearchController@Index');
Route::group(['prefix' => 'p'], function (){
  Route::get('{link_url}','Template\TemplateController@Index');
  Route::get('{link_url}/preview','Template\TemplateController@IndexPreview');
  Route::post('{link_url}','Template\TemplateController@Store')->middleware('auth');
});

/*Template*/
Route::group(['prefix' => 'template'], function (){                                                               // Done
  Route::get('animatedflower','Master\Event\Template\TemplateController@IndexAnimatedFlower');                    // Done
  Route::get('pinkanimatedflower','Master\Event\Template\TemplateController@IndexPinkAnimatedFlower');            // Done
  Route::get('goldenbadge','Master\Event\Template\TemplateController@IndexGoldenBadge');                          // Done
  Route::get('greatflower','Master\Event\Template\TemplateController@IndexGreatFlower');                          // Done
  Route::get('simpleflower','Master\Event\Template\TemplateController@IndexSimpleFlower');                        // Done
  Route::get('abstractflower','Master\Event\Template\TemplateController@IndexAbstractFlower');                    // Done
  Route::get('parallax','Master\Event\Template\TemplateController@IndexParallax');                                // Done
  Route::get('landing','Master\Event\Template\TemplateController@IndexLanding');                                  // Done
});

/*Invitation*/
Route::group(['prefix' => 'invitation'], function (){                                                             // Done
  Route::get('pink','Invitation\InvitationController@IndexPink');                                                 // Done
  Route::get('purple','Invitation\InvitationController@IndexPurple');                                             // Done
  Route::get('red','Invitation\InvitationController@IndexRed');                                                   // Done
});

/*Profile*/
Route::group(['prefix' => 'profile'], function (){                                                                // Done
  Route::get('data','Profile\ProfileController@IndexPersonalData');                                               // Done
  Route::post('data','Profile\ProfileController@StorePersonalData');                                              // Done
  Route::get('account','Profile\ProfileController@IndexAccount');                                                 // Done
  Route::post('account','Profile\ProfileController@StoreAccount');                                                // Done
});

Route::group(['middleware'=>['auth']], function (){
  Route::group(['prefix' => 'event'], function (){
    Route::get('/','Event\EventController@Index');                                                                  // Done
    Route::get('preview/{eid}','Event\EventController@IndexPreview');
    Route::post('/','Event\EventController@Store');                                                                 // Done
    Route::get('x','Event\EventController@IndexEL');                                                                // Done
    Route::get('dt','Event\EventController@DataTable');                                                             // Done
    Route::group(['prefix' => 'entry'], function (){
      Route::get('/','Event\EventController@IndexCreate');                                                          // Done
      Route::get('{eid}','Event\EventController@IndexEdit');
      Route::post('{eid}','Event\EventController@StoreEdit');
    });

    Route::group(['prefix' => 'guest'], function (){
      Route::get('dt/{event_id}','Event\EventController@DataTableGuest');                                           // Done
      Route::get('edit/{eid}/{guest_id}','Event\EventController@EditGuest');                                        // Done
      Route::post('edit/{eid}/{guest_id}','Event\EventController@UpdateGuest');
      Route::get('delete/{eid}/{guest_id}','Event\EventController@DeleteGuest');                                    // Done
      Route::get('{eid}','Event\EventController@IndexGuest');                                                       // Done
      Route::post('{eid}','Event\EventController@StoreGuest');                                                      // Done
    });
  });

  /*Master Data*/
  Route::group(['prefix' => 'master'], function (){                                                                 // Done
    // User
    Route::group(['prefix' => 'user'], function (){                                                                 // Done
      Route::get('/','Master\User\UserController@Index');                                                           // Done
      Route::get('dt','Master\User\UserController@DataTable');                                                      // Done

      Route::group(['prefix' => 'entry'], function (){                                                              // Done
        Route::get('/','Master\User\UserController@IndexCreate');                                                   // Done
        Route::post('/','Master\User\UserController@Store');                                                        // Done
        Route::get('{eid}','Master\User\UserController@IndexEdit');                                                 // Done
        Route::post('{eid}','Master\User\UserController@StoreEdit');                                                // Done
      });
    });

    // Event Category
    Route::group(['prefix' => 'category'], function (){                                                            // Done
      Route::get('/','Master\Event\Category\CategoryController@Index');                                            // Done
      Route::get('dt','Master\Event\Category\CategoryController@DataTable');                                       // Done, Status masih 1 0

      Route::group(['prefix' => 'entry'], function (){                                                             // Done
        Route::get('/','Master\Event\Category\CategoryController@IndexCreate');                                    // Done
        Route::post('/','Master\Event\Category\CategoryController@Store');                                         // Done
        Route::get('{eid}','Master\Event\Category\CategoryController@IndexEdit');                                  // Done
        Route::post('{eid}','Master\Event\Category\CategoryController@StoreEdit');                                 // Done
      });
    });

    // Event Template
    Route::group(['prefix' => 'template'], function (){                                                            // Done
      Route::get('/','Master\Event\Template\TemplateController@Index');                                            // Done
      Route::get('dt','Master\Event\Template\TemplateController@DataTable');                                       // Done

      Route::group(['prefix' => 'entry'], function (){                                                             // Done
        Route::get('/','Master\Event\Template\TemplateController@IndexCreate');                                    // Done
        Route::post('/','Master\Event\Template\TemplateController@Store');                                         // Done
        Route::get('{eid}','Master\Event\Template\TemplateController@IndexEdit');                                  // Done
        Route::post('{eid}','Master\Event\Template\TemplateController@StoreEdit');                                 // Done
      });
    });

    // Event Detail
    Route::group(['prefix' => 'detail'], function (){                                                              // Done
      Route::get('/','Master\Event\Detail\DetailController@Index');                                                // Done
      Route::get('dt','Master\Event\Detail\DetailController@DataTable');                                           // Done

      Route::group(['prefix' => 'entry'], function (){                                                             // Done
        Route::get('/','Master\Event\Detail\DetailController@IndexCreate');                                        // Done
        Route::post('/','Master\Event\Detail\DetailController@Store');                                             // Done
        Route::get('{eid}','Master\Event\Detail\DetailController@IndexEdit');                                      // Done
        Route::post('{eid}','Master\Event\Detail\DetailController@StoreEdit');                                     // Done
      });
    });

    // Wedding Planner
    Route::group(['prefix' => 'weddingplanner'], function (){
      Route::get('/','WeddingPlanner\WeddingPlannerController@Index');
      Route::get('dt','WeddingPlanner\WeddingPlannerController@DataTable');

      Route::group(['prefix' => 'entry'], function (){
        Route::get('/','WeddingPlanner\WeddingPlannerController@IndexCreate');
        Route::post('/','WeddingPlanner\WeddingPlannerController@Store');

        Route::get('{eid}','WeddingPlanner\WeddingPlannerController@IndexEdit');
        Route::post('{eid}','WeddingPlanner\WeddingPlannerController@StoreEdit');
      });
    });

    // Wedding Flower
    Route::group(['prefix' => 'weddingflower'], function (){
      Route::get('/','WeddingFlower\WeddingFlowerController@Index');
      Route::get('dt','WeddingFlower\WeddingFlowerController@DataTable');

      Route::group(['prefix' => 'entry'], function (){
        Route::get('/','WeddingFlower\WeddingFlowerController@IndexCreate');
        Route::post('/','WeddingFlower\WeddingFlowerController@Store');

        Route::get('{eid}','WeddingFlower\WeddingFlowerController@IndexEdit');
        Route::post('{eid}','WeddingFlower\WeddingFlowerController@StoreEdit');
      });
    });

    // Wedding Cake
    Route::group(['prefix' => 'weddingcake'], function (){
      Route::get('/','WeddingCake\WeddingCakeController@Index');
      Route::get('dt','WeddingCake\WeddingCakeController@DataTable');

      Route::group(['prefix' => 'entry'], function (){
        Route::get('/','WeddingCake\WeddingCakeController@IndexCreate');
        Route::post('/','WeddingCake\WeddingCakeController@Store');

        Route::get('{eid}','WeddingCake\WeddingCakeController@IndexEdit');
        Route::post('{eid}','WeddingCake\WeddingCakeController@StoreEdit');
      });
    });

    // Wedding Item
    Route::group(['prefix' => 'item'], function (){
      Route::get('/','Item\ItemController@Index');
      Route::get('dt','Item\ItemController@DataTable');

      Route::group(['prefix' => 'entry'], function (){
        Route::get('/','Item\ItemController@IndexCreate');
        Route::post('/','Item\ItemController@Store');

        Route::get('{eid}','Item\ItemController@IndexEdit');
        Route::post('{eid}','Item\ItemController@StoreEdit');
      });
    });
  });

});

/*Partner & Shop*/
Route::group(['prefix' => 'partner'], function (){                                                               // Done
  Route::get('weddingplanner','Partner\PartnerController@IndexPlanner');                                         // Done, belum ada partner asli
  Route::get('weddingflower','Partner\PartnerController@IndexFlower');                                           // Done, belum ada partner asli
  Route::get('weddingcake','Partner\PartnerController@IndexCake');                                               // Done, belum ada partner asli
  Route::get('shop','Shop\ShopController@Index');                                                                // Done, belum ada item asli
});


/*About Us*/
Route::get('about-us','AboutUs\AboutUsController@Index');                                                       // Done, foto belum sesuai
Route::get('contact-us','AboutUs\AboutUsController@Index');                                                     // Done, belum tau isinya apa
