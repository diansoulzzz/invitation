<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29 Aug 2018 05:26:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Crypt;
/**
 * Class TemplateDesignDetail
 *
 * @property int $id
 * @property string $unique_name
 * @property string $html
 * @property string $types
 * @property int $template_design_detail_id
 * @property int $template_design_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\TemplateDesign $template_design
 * @property \App\Models\TemplateDesignDetail $template_design_detail
 * @property \Illuminate\Database\Eloquent\Collection $event_designs
 * @property \Illuminate\Database\Eloquent\Collection $template_design_details
 *
 * @package App\Models
 */
class TemplateDesignDetail extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'template_design_detail';

	protected $casts = [
		'template_design_detail_id' => 'int',
		'template_design_id' => 'int'
	];

	protected $fillable = [
		'unique_name',
		'html',
		'types',
		'template_design_detail_id',
		'template_design_id'
	];

	protected $appends = [
		'eid'
	];

	public function template_design()
	{
		return $this->belongsTo(\App\Models\TemplateDesign::class);
	}

	public function template_design_detail()
	{
		return $this->belongsTo(\App\Models\TemplateDesignDetail::class);
	}

	public function event_designs()
	{
		return $this->hasMany(\App\Models\EventDesign::class);
	}

	public function template_design_details()
	{
		return $this->hasMany(\App\Models\TemplateDesignDetail::class);
	}
	// 
	// public function event_design()
  // {
  //   return $this->hasManyThrough('App\Models\Event', 'App\Models\EventDesign','id','event_id','event_id');
  // }

	// public function event_design()
  // {
  //   return $this->hasManyThrough('App\Models\EventDesign', 'App\Models\Event','event_id','event_id');
  // }

  public function getEidAttribute()
  {
    $eid = Crypt::encryptString($this->id);
    return $eid;
  }
}
