<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29 Aug 2018 05:26:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Crypt;

/**
 * Class EventImage
 *
 * @property int $id
 * @property string $gambar
 * @property int $event_id
 *
 * @property \App\Models\Event $event
 *
 * @package App\Models
 */
class EventImage extends Eloquent
{
	protected $table = 'event_image';
	public $timestamps = false;

	protected $casts = [
		'event_id' => 'int'
	];

	protected $fillable = [
		'gambar',
		'event_id'
	];

	protected $appends = [
		'eid'
	];

	public function event()
	{
		return $this->belongsTo(\App\Models\Event::class);
	}

  public function getEidAttribute()
  {
    $eid = Crypt::encryptString($this->id);
    return $eid;
  }
}
