<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29 Aug 2018 05:26:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Crypt;

/**
 * Class EventCategory
 *
 * @property int $id
 * @property string $nama
 * @property string $keterangan
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $link_url
 * @property int $publish
 *
 * @property \Illuminate\Database\Eloquent\Collection $events
 * @property \Illuminate\Database\Eloquent\Collection $template_designs
 *
 * @package App\Models
 */
class EventCategory extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'event_category';

	protected $casts = [
		'publish' => 'int'
	];

	protected $fillable = [
		'nama',
		'keterangan',
		'link_url',
		'publish'
	];

	protected $appends = [
		'eid'
	];

	public function events()
	{
		return $this->hasMany(\App\Models\Event::class);
	}

	public function template_designs()
	{
		return $this->hasMany(\App\Models\TemplateDesign::class);
	}

  public function getEidAttribute()
  {
    $eid = Crypt::encryptString($this->id);
    return $eid;
  }
}
