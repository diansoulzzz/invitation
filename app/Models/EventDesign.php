<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29 Aug 2018 05:26:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Crypt;

/**
 * Class EventDesign
 *
 * @property int $id
 * @property int $template_design_detail_id
 * @property int $event_id
 * @property string $value
 *
 * @property \App\Models\Event $event
 * @property \App\Models\TemplateDesignDetail $template_design_detail
 *
 * @package App\Models
 */
class EventDesign extends Eloquent
{
	protected $table = 'event_design';
	public $timestamps = false;

	protected $casts = [
		'template_design_detail_id' => 'int',
		'event_id' => 'int'
	];

	protected $fillable = [
		'template_design_detail_id',
		'event_id',
		'value'
	];

	protected $appends = [
		'eid'
	];

	public function event()
	{
		return $this->belongsTo(\App\Models\Event::class);
	}

	public function template_design_detail()
	{
		return $this->belongsTo(\App\Models\TemplateDesignDetail::class);
	}

  public function getEidAttribute()
  {
    $eid = Crypt::encryptString($this->id);
    return $eid;
  }
}
