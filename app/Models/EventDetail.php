<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29 Aug 2018 05:26:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Crypt;

/**
 * Class EventDetail
 *
 * @property int $id
 * @property int $event_id
 * @property string $nama_pria
 * @property string $nama_wanita
 * @property string $nama_acara
 * @property string $judul_cerita
 * @property string $cerita_awal
 * @property string $cerita_tengah
 * @property string $cerita_akhir
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\Event $event
 *
 * @package App\Models
 */
class EventDetail extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'event_detail';

	protected $casts = [
		'event_id' => 'int'
	];

	protected $fillable = [
		'event_id',
		'nama_pria',
		'nama_wanita',
		'nama_acara',
		'judul_cerita',
		'cerita_awal',
		'cerita_tengah',
		'cerita_akhir'
	];

	protected $appends = [
		'eid'
	];

	public function event()
	{
		return $this->belongsTo(\App\Models\Event::class);
	}

  public function getEidAttribute()
  {
    $eid = Crypt::encryptString($this->id);
    return $eid;
  }
}
