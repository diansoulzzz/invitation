<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29 Aug 2018 05:26:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Crypt;

/**
 * Class Guest
 *
 * @property int $id
 * @property string $nama
 * @property string $alamat
 * @property string $telepon
 * @property string $email
 * @property string $whatsapp
 * @property string $line
 * @property int $jenis
 * @property int $kehadiran
 * @property string $qrcode
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $event_id
 *
 * @property \App\Models\Event $event
 *
 * @package App\Models
 */
class Guest extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'guest';

	protected $casts = [
		'jenis' => 'int',
		'kehadiran' => 'int',
		'event_id' => 'int'
	];

	protected $fillable = [
		'nama',
		'alamat',
		'telepon',
		'email',
		'whatsapp',
		'line',
		'jenis',
		'kehadiran',
		'ucapan',
		'nama_gift',
		'qrcode',
		'event_id'
	];

	protected $appends = [
		'eid'
	];

	public function event()
	{
		return $this->belongsTo(\App\Models\Event::class);
	}

  public function getEidAttribute()
  {
    $eid = Crypt::encryptString($this->id);
    return $eid;
  }
}
