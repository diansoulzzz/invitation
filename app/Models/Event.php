<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29 Aug 2018 05:26:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Crypt;

/**
 * Class Event
 *
 * @property int $id
 * @property string $nama
 * @property string $quote
 * @property string $author
 * @property string $background_home_1
 * @property string $background_home_2
 * @property string $background_schedule
 * @property string $backgorund_quotes
 * @property string $background_ending
 * @property int $users_id
 * @property int $template_design_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property string $link_url
 * @property int $publish
 * @property int $event_category_id
 *
 * @property \App\Models\EventCategory $event_category
 * @property \App\Models\TemplateDesign $template_design
 * @property \App\Models\User $user
 * @property \Illuminate\Database\Eloquent\Collection $event_designs
 * @property \Illuminate\Database\Eloquent\Collection $event_details
 * @property \Illuminate\Database\Eloquent\Collection $event_images
 * @property \Illuminate\Database\Eloquent\Collection $event_schedules
 * @property \Illuminate\Database\Eloquent\Collection $guests
 *
 * @package App\Models
 */
class Event extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'event';

	protected $casts = [
		'users_id' => 'int',
		'template_design_id' => 'int',
		'publish' => 'int',
		'event_category_id' => 'int'
	];

	protected $fillable = [
		'nama',
		'quote',
		'author',
		'background_home_1',
		'background_home_2',
		'background_schedule',
		'backgorund_quotes',
		'background_ending',
		'users_id',
		'template_design_id',
		'link_url',
		'publish',
		'event_category_id'
	];

	protected $appends = [
		'eid'
	];

	public function event_category()
	{
		return $this->belongsTo(\App\Models\EventCategory::class);
	}

	public function template_design()
	{
		return $this->belongsTo(\App\Models\TemplateDesign::class);
	}

	public function user()
	{
		return $this->belongsTo(\App\Models\User::class, 'users_id');
	}

	public function event_designs()
	{
		return $this->hasMany(\App\Models\EventDesign::class);
	}

	public function event_details()
	{
		return $this->hasMany(\App\Models\EventDetail::class);
	}

	public function event_images()
	{
		return $this->hasMany(\App\Models\EventImage::class);
	}

	public function event_schedules()
	{
		return $this->hasMany(\App\Models\EventSchedule::class);
	}

	public function guests()
	{
		return $this->hasMany(\App\Models\Guest::class);
	}

	public function event_detail()
	{
		return $this->hasOne(\App\Models\EventDetail::class);
	}

  public function getEidAttribute()
  {
    $eid = Crypt::encryptString($this->id);
    return $eid;
  }
}
