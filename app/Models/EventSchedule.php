<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29 Aug 2018 05:26:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Crypt;

/**
 * Class EventSchedule
 *
 * @property int $id
 * @property string $judul
 * @property string $tempat
 * @property string $kota
 * @property string $negara
 * @property string $telepon
 * @property \Carbon\Carbon $jam_mulai
 * @property \Carbon\Carbon $jam_selesai
 * @property int $utama
 * @property string $gambar
 * @property float $latiitude
 * @property float $longitude
 * @property string $dresscode
 * @property string $dresscode_detail
 * @property string $gift
 * @property string $gift_detail
 * @property int $event_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 *
 * @property \App\Models\Event $event
 *
 * @package App\Models
 */
class EventSchedule extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'event_schedule';

	protected $casts = [
		'utama' => 'int',
		'latiitude' => 'float',
		'longitude' => 'float',
		'event_id' => 'int'
	];

	protected $dates = [
		'jam_mulai',
		'jam_selesai'
	];

	protected $fillable = [
		'judul',
		'tempat',
		'kota',
		'negara',
		'telepon',
		'jam_mulai',
		'jam_selesai',
		'utama',
		'gambar',
		'latiitude',
		'longitude',
		'dresscode',
		'dresscode_detail',
		'gift',
		'gift_detail',
		'event_id'
	];

	protected $appends = [
		'eid'
	];

	public function event()
	{
		return $this->belongsTo(\App\Models\Event::class);
	}

  public function getEidAttribute()
  {
    $eid = Crypt::encryptString($this->id);
    return $eid;
  }
}
