<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 29 Aug 2018 05:26:16 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Crypt;

/**
 * Class TemplateDesign
 *
 * @property int $id
 * @property string $nama
 * @property string $imgpreview
 * @property string $html
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $deleted_at
 * @property int $event_category_id
 *
 * @property \App\Models\EventCategory $event_category
 * @property \Illuminate\Database\Eloquent\Collection $events
 * @property \Illuminate\Database\Eloquent\Collection $template_design_details
 *
 * @package App\Models
 */
class TemplateDesign extends Eloquent
{
	use \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table = 'template_design';

	protected $casts = [
		'event_category_id' => 'int'
	];

	protected $fillable = [
		'nama',
		'imgpreview',
		'html',
		'event_category_id'
	];

	protected $appends = [
		'eid'
	];

	public function event_category()
	{
		return $this->belongsTo(\App\Models\EventCategory::class);
	}

	public function events()
	{
		return $this->hasMany(\App\Models\Event::class);
	}

	public function template_design_details()
	{
		return $this->hasMany(\App\Models\TemplateDesignDetail::class);
	}

  public function getEidAttribute()
  {
    $eid = Crypt::encryptString($this->id);
    return $eid;
  }
}
