<?php

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Partner;

class PartnerController extends Controller
{
  public function IndexPlanner(Request $Request) {
      return view('menus.partner.weddingplanner.index');
  }

  public function IndexFlower(Request $Request) {
      return view('menus.partner.weddingflower.index');
  }

  public function IndexCake(Request $Request) {
      return view('menus.partner.weddingcake.index');
  }
}
