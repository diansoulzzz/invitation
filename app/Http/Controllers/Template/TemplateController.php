<?php

namespace App\Http\Controllers\Template;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\EventDesign;
use App\Models\EventCategory;
use App\Models\TemplateDesign;
use App\Models\TemplateDesignDetail;
use Auth;
use DataTables;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;

class TemplateController extends Controller
{
  public function Index(Request $request, $link_url) {
    $template_design = TemplateDesign::with('template_design_details')->where('link_url',$link_url)->first();
    $rendered = $template_design->html;
    foreach ($template_design->template_design_details as $key => $detail) {
      $rendered = str_replace($detail->unique_name, $detail->html , $rendered);
    }
    $template_design->rendered = $rendered;
    return view('menus.template.detail', compact('template_design','link_url'));
  }

  public function IndexPreview(Request $request, $link_url) {
    if (!$request->session()->has('previews'.$link_url)) {
      return redirect('p/'.$link_url);
    }
    $previews = $request->session()->get('previews'.$link_url);
    // return response()->json($previews);
    $template_design = TemplateDesign::with('template_design_details')->where('link_url',$link_url)->first();
    $rendered = $template_design->html;
    foreach ($previews->event->event_design->text as $key => $detail) {
      $rendered = str_replace($detail->template_design_detail->unique_name, $detail->value , $rendered);
    }
    foreach ($previews->event->event_design->file as $key => $detail) {
      $rendered = str_replace($detail->template_design_detail->unique_name, $detail->value , $rendered);
    }
    $template_design->rendered = $rendered;
    return view('menus.template.detail', compact('template_design','link_url','previews'));
  }
  public function SavePreview(Request $request, $link_url) {
    if ($request->session()->has('previews'.$link_url)) {
      $template_design = TemplateDesign::where('link_url',$link_url)->first();
      $previews = $request->session()->get('previews'.$link_url);
      $event = $previews->event;
      $event->nama = $request->input('event_title');
      $event->users_id = Auth::user()->id;
      $event->template_design_id = $template_design->id;
      $event->link_url = 'p/'.$request->input('event_name');
      $event->publish = 1;
      $event->event_category_id = $template_design->event_category_id;
      $previews = (object)[
        'event' => $event
      ];
      $design_text = $event->event_design->text;
      foreach ($request->input('template.text') as $id => $value) {
        $template_design_detail = TemplateDesignDetail::find($id);
        $event_design = new EventDesign();
        $event_design->makeHidden('eid');
        $event_design->template_design_detail_id = $id;
        $event_design->value = $value;
        $event_design->template_design_detail = $template_design_detail;
        // $event_design->unique_name = $template_design_detail->unique_name;
        $design_text[$id]=$event_design;
      }

      $design_file = $event->event_design->file;
      // return $design_file;
      if ($request->hasFile('template.file')) {
        foreach ($request->file('template.file') as $id => $url_photo) {
          $template_design_detail = TemplateDesignDetail::find($id);
          $event_design = new EventDesign();
          $event_design->makeHidden('eid');
          $event_design->template_design_detail_id = $id;
          $event_design->value = asset(Storage::disk('public')->url(Storage::disk('public')->put('preview/'.$link_url, $url_photo)));
          $event_design->template_design_detail = $template_design_detail;
          // $event_design->unique_name = $template_design_detail->unique_name;
          $design_file[$id]=$event_design;
        }
      }
      $design = (object)[
        'text' => $design_text,
        'file' => $design_file,
      ];
      // return response()->json($design);
      $previews->event->event_design = $design;
      $request->session()->put('previews'.$link_url, $previews);
      return $previews;

      // return redirect('p/'.$link_url.'/preview')->withStatus([
      //   'alert'=>'alert-success',
      //   'status'=>'Saved !',
      //   'message'=>'Event has been saved !'
      // ]);
    }
    $template_design = TemplateDesign::where('link_url',$link_url)->first();
    $event = new Event();
    $event->makeHidden('eid');
    $event->nama = $request->input('event_title');
    $event->users_id = Auth::user()->id;
    $event->template_design_id = $template_design->id;
    $event->link_url = 'p/'.$request->input('event_name');
    $event->publish = 1;
    $event->event_category_id = $template_design->event_category_id;
    $previews = (object)[
      'event' => $event
    ];
    // return response()->json($template_text);
    $design_text = [];
    foreach ($request->input('template.text') as $id => $value) {
      $template_design_detail = TemplateDesignDetail::find($id);
      $event_design = new EventDesign();
      $event_design->makeHidden('eid');
      $event_design->template_design_detail_id = $id;
      $event_design->value = $value;
      $event_design->template_design_detail = $template_design_detail;
      // $event_design->unique_name = $template_design_detail->unique_name;
      $design_text[$id]=$event_design;
    }

    $design_file = [];
    if ($request->hasFile('template.file')) {
      foreach ($request->file('template.file') as $id => $url_photo) {
        $template_design_detail = TemplateDesignDetail::find($id);
        $event_design = new EventDesign();
        $event_design->makeHidden('eid');
        $event_design->template_design_detail_id = $id;
        $event_design->value = asset(Storage::disk('public')->url(Storage::disk('public')->put('preview/'.$link_url, $url_photo)));
        $event_design->template_design_detail = $template_design_detail;
        // $event_design->unique_name = $template_design_detail->unique_name;
        $design_file[$id]=$event_design;
      }
    }
    $design = (object)[
      'text' => $design_text,
      'file' => $design_file,
    ];
    $previews->event->event_design = $design;
    $request->session()->put('previews'.$link_url, $previews);
    return $previews;
    // return redirect('p/'.$link_url.'/preview')->withStatus([
    //   'alert'=>'alert-success',
    //   'status'=>'Saved !',
    //   'message'=>'Event has been saved !'
    // ]);
  }

  public function Store(Request $request, $link_url) {
    $SavedPreview = $this->SavePreview($request,$link_url);
    if ($request->has('preview')) {
      return redirect('p/'.$link_url.'/preview')->withStatus([
        'alert'=>'alert-success',
        'status'=>'Saved !',
        'message'=>'Event has been saved !'
      ]);
    }
    $message = [
      // 'name.required' => 'The email field is required.',
      // 'name.min' => 'Minimum length is 3',
    ];
    $this->validate($request, [
        // 'name' => 'required|min:3|max:100',
        // 'description' => 'required|min:10',
        // 'link_url' => 'required|min:3',
    ], $message);
    $savedEvent = $SavedPreview->event;

    $event = new Event();
    $event->nama = $savedEvent->nama;
    $event->users_id = Auth::user()->id;
    $event->template_design_id = $savedEvent->template_design_id;
    $event->link_url = $savedEvent->link_url;
    $event->publish = $savedEvent->publish;
    $event->event_category_id = $savedEvent->event_category_id;
    $event->save();

    foreach ($savedEvent->event_design->text as $id => $value) {
      $event_design = new EventDesign();
      $event_design->template_design_detail_id = $id;
      $event_design->event_id = $event->id;
      $event_design->value = $value->value;
      $event_design->save();
    }

    foreach ($savedEvent->event_design->file as $id => $value) {
      $event_design = new EventDesign();
      $event_design->template_design_detail_id = $id;
      $event_design->event_id = $event->id;
      $event_design->value = $value->value;
      $event_design->save();
    }

    return redirect('event')->withStatus([
      'alert'=>'alert-success',
      'status'=>'Saved !',
      'message'=>'Event has been saved !'
    ]);
  }
}
