<?php

namespace App\Http\Controllers\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Auth;
use Hash;

class ProfileController extends Controller
{
  public function IndexPersonalData(Request $request) {
      $user = User::find(Auth::user()->id);
      return view('menus.profile.data', compact('user'));
  }

  public function StorePersonalData(Request $request) {

      $message = [
        // 'name.required' => 'The email field is required.',
        // 'name.min' => 'Minimum length is 3',
      ];

      $this->validate($request, [
          'name' => 'required|min:3|max:100',
          'telephone' => 'required|max:13',
          'address' => 'required|max:200',
      ], $message);

      $user = User::find(Auth::user()->id);
      $user->nama = $request->name;
      $user->jenis_kelamin = $request->gender;
      $user->tanggal_lahir = $request->birthday;
      $user->telepon = $request->telephone;
      $user->alamat = $request->address;
      $user->save();

      return redirect('profile/data')->withStatus([
        'alert'=>'alert-success',
        'status'=>'Updated !',
        'message'=>'User has been updated !'
      ]);
  }

  public function IndexAccount(Request $request) {
      $user = User::find(Auth::user()->id);
      return view('menus.profile.account', compact('user'));
  }

  public function StoreAccount(Request $request) {

      $message = [
        // 'name.required' => 'The email field is required.',
        // 'name.min' => 'Minimum length is 3',
      ];

      $this->validate($request, [
          'password' => 'required|confirmed|min:6'
      ], $message);

      $user = User::find(Auth::user()->id);

      if (Hash::check($request->last_password, $user->password)) {
        $user->password = Hash::make($request->password);
        $user->save();

        return redirect('profile/account')->withStatus([
          'alert'=>'alert-success',
          'status'=>'Updated !',
          'message'=>'User Account has been updated !'
        ]);
      } else {
        return redirect('profile/account')->withStatus([
          'alert'=>'alert-danger',
          'status'=>'Error !',
          'message'=>'Please insert your last password correctly !'
        ]);
      }



  }
}
