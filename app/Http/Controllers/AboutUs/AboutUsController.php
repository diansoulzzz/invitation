<?php

namespace App\Http\Controllers\AboutUs;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\AboutUs;

class AboutUsController extends Controller
{
  public function Index(Request $Request) {
      return view('menus.aboutus.index');
  }
}
