<?php

namespace App\Http\Controllers\Shop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Shop;

class ShopController extends Controller
{
  public function Index(Request $Request) {
      return view('menus.shop.index');
  }
}
