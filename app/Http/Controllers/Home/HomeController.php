<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventDetail;
use Carbon\Carbon;

class HomeController extends Controller
{
  public function Index() {
    return view('menus.home.index');
  }
}
