<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Validator;
use App\Models\User;
use App\Models\Guest;

class ApiController extends Controller
{
  public function Index(Request $request) {
    return $request->user();
  }
  public function setSuccessResponse($data=[],$input){
    return response([
      'data'=> $data,
      'input'=>$input,
    ]);
  }
  public function setErrorResponse($validate=[],$input,$messages='',$code=400){
    return response([
      'error'=>[
        'code'=>$code,
        'message'=>$messages,
        'validate'=>$validate,
      ],
      'input'=>$input,
    ]);
    // ],$code);
  }

  public function Login(Request $request) {
    $messages = [
      'email.required' => 'Email required',
      'password.required' => 'Password required'
    ];
    $validator = Validator::make($request->all(), [
      'email' => 'required|email',
      'password' => 'required|min:6'
    ], $messages);
    if ($validator->fails()) {
      return $this->setErrorResponse($validator->errors(),$request->all(),'Error',400);
    }
    $email = $request->input('email');
    $password = $request->input('password');
    if (Auth::attempt(['email' => $email, 'password' => $password, 'status' => '2'])) {
      return $this->setSuccessResponse(Auth::user()->only(['id','nama','email','api_token']),$request->all());
    } else {
      return $this->setErrorResponse([],$request->all(),'Please check your email or password',401);
    }
  }

  public function GuestScanQr(Request $request) {
    $messages = [
      'qrcode.required' => 'Qrcode required',
    ];
    $validator = Validator::make($request->all(), [
      'qrcode' => 'required|exists:guest,qrcode',
    ], $messages);
    if ($validator->fails()) {
      return $this->setErrorResponse($validator->errors(),$request->all(),'Error',400);
    }
    $qrcode = $request->input('qrcode');
    $guest = Guest::with('event')->where('qrcode',$qrcode)->first()->only(['id','nama','alamat','telepon','email','whatsapp','line','jenis','kehadiran','event']);
    return $this->setSuccessResponse($guest,$request->all());
  }

  public function GuestList(Request $request) {
    $messages = [
      'event_id.required' => 'Event Id not found',
    ];
    $validator = Validator::make($request->all(), [
      'event_id' => 'required|exists:guest,event_id',
    ], $messages);
    if ($validator->fails()) {
      return $this->setErrorResponse($validator->errors(),$request->all(),'Error',400);
    }
    $event_id = $request->input('event_id');
    $guest = Guest::where('event_id',$event_id)->get();
    return $this->setSuccessResponse($guest,$request->all());
  }

  public function GuestCheckIn(Request $request) {
    $messages = [
      'id.required' => 'Guest Id not found',
    ];
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:guest,id',
    ], $messages);
    if ($validator->fails()) {
      return $this->setErrorResponse($validator->errors(),$request->all(),'Error',400);
    }
    $id = $request->input('id');
    $guest = Guest::find($id);
    $guest->kehadiran=1;
    $guest->ucapan = $request->input('ucapan');
    $guest->nama_gift = $request->input('nama_gift');
    $guest->foto_gift = $request->input('nama_gift');
    $guest->save();
    return $this->setSuccessResponse($guest,$request->all());
  }

// unused
  public function GuestGift(Request $request) {
    $messages = [
      'id.required' => 'Guest Id not found',
    ];
    $validator = Validator::make($request->all(), [
      'id' => 'required|exists:guest,id',
    ], $messages);
    if ($validator->fails()) {
      return $this->setErrorResponse($validator->errors(),$request->all(),'Error',400);
    }
    $id = $request->input('id');
    $guest = Guest::find($id);
    $guest->kehadiran=1;
    $guest->save();
    return $this->setSuccessResponse($guest,$request->all());
  }

}
