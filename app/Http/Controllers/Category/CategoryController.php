<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\EventCategory;
use DataTables;
use Illuminate\Support\Facades\Crypt;
use App\Models\TemplateDesign;

class CategoryController extends Controller
{
  public function Index(Request $request, $link_url) {
      // $keyword = $request->input('q');
      // $category = $request->input('c');
      // $schedule = $request->input('s');
      // $p_min = $request->input('p_min');
      // $p_max = $request->input('p_max');
      // $order_by = $request->input('sb');
      //
      // $items = Item::with('item_images_primary');
      // $items = $items->where('name','like','%'.$keyword.'%');
      // // $order_by = $items->orderBy('')
      // if ($p_min) {
      //   $items = $items->where('price','>=',$p_min);
      // }
      // if ($p_max) {
      //   $items = $items->where('price','<=',$p_max);
      // }
      // if ($schedule) {
      //   $items = $items->whereIn('schedule_id',$schedule);
      // }
      // if ($category) {
      //   $items = $items->whereIn('item_category_id',$category);
      // }
      // $items = $items->where('publish', true);
      // if ($order_by) {
      //   if($order_by=='price_asc') {
      //     $order = ['price','asc'];
      //   } else if ($order_by=='price_desc') {
      //     $order = ['price','desc'];
      //   }
      //   // return $order[1];
      //   $items = $items->orderBy($order[0], $order[1]);
      // }
      // $items = $items->paginate(6);
      // $schedules = Schedule::get();
      // $items_category = ItemCategory::get();
      $event_category = EventCategory::where('link_url',$link_url)->first();
      $template_designs = TemplateDesign::with('template_design_details');
      $template_designs = $template_designs->paginate(10);
      return view('menus.category.index',compact('template_designs','event_category'));
  }
}
