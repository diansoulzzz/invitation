<?php

namespace App\Http\Controllers\Master\Event\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\EventCategory;
use DataTables;
use Illuminate\Support\Facades\Crypt;

class CategoryController extends Controller
{
  public function Index(Request $request) {
      return view('menus.masterdata.category.index');
  }

  public function DataTable(Request $request) {
    $field = ['id', 'nama', 'keterangan', 'link_url', 'publish'];
    $model = EventCategory::select($field);

    // if($model->publish == 1){
    //     $model->publish = 'Publish';
    // } else {
    //     $model->publish = 'Not Publish';
    // }

    $datatable = Datatables::of($model)
    // ->addColumn('publish', function ($item) {
    //   return $model->publish;
    // })
    ->addColumn('action', function ($item) {
      return '<a href="'.url('master/category/entry/'.$item->eid).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
    })
    ->make(true);
    return $datatable;
  }

  public function IndexCreate(Request $request) {
      return view('menus.masterdata.category.create');
  }

  public function IndexEdit(Request $request, $eid = null) {
      $did = Crypt::decryptString($eid);
      $category = EventCategory::find($did);
      return view('menus.masterdata.category.create', compact('category'));
  }

  public function Store(Request $request) {
      $message = [
        // 'name.required' => 'The email field is required.',
        // 'name.min' => 'Minimum length is 3',
      ];

      $this->validate($request, [
          'name' => 'required|min:3|max:100',
          'description' => 'required|min:10',
          'link_url' => 'required|min:3',
      ], $message);

      $category = new EventCategory();
      $category->nama = $request->name;
      $category->keterangan = $request->description;
      $category->link_url = $request->link_url;
      $category->publish = (1);
      $category->save();

      return redirect('master/category')->withStatus([
        'alert'=>'alert-success',
        'status'=>'Saved !',
        'message'=>'Template category has been saved !'
      ]);
  }

  public function StoreEdit(Request $request, $eid) {

      $message = [
        // 'name.required' => 'The email field is required.',
        // 'name.min' => 'Minimum length is 3',
      ];

      $this->validate($request, [
          'name' => 'required|min:3|max:100',
          'description' => 'required|min:10',
          'link_url' => 'required|min:3',
      ], $message);

      $did = Crypt::decryptString($eid);
      $category = EventCategory::find($did);
      $category->nama = $request->name;
      $category->keterangan = $request->description;
      $category->link_url = $request->link_url;
      $category->publish = $request->publish;
      $category->save();

      return redirect('master/category')->withStatus([
        'alert'=>'alert-success',
        'status'=>'Updated !',
        'message'=>'Template category has been updated !'
      ]);
  }
}
