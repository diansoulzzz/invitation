<?php

namespace App\Http\Controllers\Master\Event\Template;

use App\Http\Controllers\Controller;
use App\Models\TemplateDesign;
use App\Models\EventCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DataTables;
use Validator;

class TemplateController extends Controller
{
  public function SaveTheDate() {
      $data['date'] = Carbon::now()->addMonths(3)->format('F d, Y');
      $data['day'] = Carbon::now()->addMonths(3)->format('d');
      $data['month'] = Carbon::now()->addMonths(3)->format('m');
      $data['year'] = Carbon::now()->addMonths(3)->format('Y');

      return $data;
  }

  public function IndexAnimatedFlower(Request $Request) {
      $date = $this->SaveTheDate();
      return view('menus.template.animatedflowers.index', compact('date'));
  }

  public function IndexPinkAnimatedFlower(Request $Request) {
      $date = $this->SaveTheDate();
      return view('menus.template.pinkanimatedflowers.index', compact('date'));
  }

  public function IndexGoldenBadge(Request $Request) {
      $date = $this->SaveTheDate();
      return view('menus.template.goldenbadges.index', compact('date'));
  }

  public function IndexGreatFlower(Request $Request) {
      $date = $this->SaveTheDate();
      return view('menus.template.greatflowers.index', compact('date'));
  }

  public function IndexSimpleFlower(Request $Request) {
      $date = $this->SaveTheDate();
      return view('menus.template.simpleflowers.index', compact('date'));
  }

  public function IndexAbstractFlower(Request $Request) {
    $date = $this->SaveTheDate();
      return view('menus.template.abstractflowers.index', compact('date'));
  }

  public function IndexParallax(Request $Request) {
      $date = $this->SaveTheDate();
      return view('menus.template.parallax.index', compact('date'));
  }

  public function IndexLanding(Request $Request) {
      $date = $this->SaveTheDate();
      return view('menus.template.landing.index', compact('date'));
  }

  public function Index(Request $request) {
      return view('menus.masterdata.template.index');
  }

  public function DataTable(Request $request) {
    $field = ['template_design.id', 'template_design.nama', 'template_design.link_url', 'event_category.nama as kategori'];
    $model = TemplateDesign::select($field)->join('event_category', 'template_design.event_category_id', '=', 'event_category.id');
    $datatable = Datatables::of($model)
    ->addColumn('action', function ($item) {
      return '<a href="'.url('master/template/entry/'.$item->eid).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
    })
    ->make(true);
    return $datatable;
  }

  public function IndexCreate(Request $request) {
    $category = EventCategory::all();
    return view('menus.masterdata.template.create', compact('category'));
  }

  public function IndexEdit(Request $request, $eid = null) {
      $did = Crypt::decryptString($eid);
      $template = TemplateDesign::find($did);
      $category = EventCategory::all();
      return view('menus.masterdata.template.create', compact('template', 'category'));
  }

  public function Store(Request $request) {
      $message = [
          // 'photo_image.required' => 'Photo required',
          // 'photo_image.image' => 'File must be an image',
      ];

      $this->validate($request, [
          'name' => 'required|min:3|max:100',
          'html' => 'required|min:25',
      ], $message);

      $template = new TemplateDesign();
      $template->nama = $request->name;
      $template->html = $request->html;
       $template->event_category_id = $request->category;
      $template->link_url = $request->link_url;
      $template->save();

      $validator = Validator::make($request->all(), [
           'photo_image' => 'required|image', //|dimensions:min_width=80,min_height=80
       ], $message);

       if ($validator->fails()) {
           // return $validator->errors();
           return redirect()->back()->withError($validator->errors())->withInput();
       }

       if ($request->hasFile('photo_image'))
       {
         if ($request->file('photo_image')->isValid())
         {
             $photo_image = $request->file('photo_image');
             $template->imgpreview = Storage::disk('public')->put('template/'.$template->id, $photo_image);
             $template->save();
         }
       }

      return redirect('master/template')->withStatus([
        'alert'=>'alert-success',
        'status'=>'Saved !',
        'message'=>'Event template has been saved !'
      ]);
  }

  public function StoreEdit(Request $request, $eid) {
    $message = [
        // 'photo_image.required' => 'Photo required',
        // 'photo_image.image' => 'File must be an image',
    ];

    $this->validate($request, [
        'name' => 'min:3|max:100',
        'html' => 'min:25',
    ], $message);

    $validator = Validator::make($request->all(), [
         'photo_image' => 'image', //|dimensions:min_width=80,min_height=80
     ], $message);

     if ($validator->fails()) {
         // return response()->json($validator->errors());
         // $errors = json_encode(json_decode($validator->errors(), true));
         // // return $errors;
         // return $validator->errors();
         return redirect()->back()->withError($validator->errors())->withInput();
     }

     if ($request->hasFile('photo_image'))
     {
       $did = Crypt::decryptString($request->input('eid'));
       if ($request->file('photo_image')->isValid())
       {
           $photo_image = $request->file('photo_image');
           $template = TemplateDesign::find($did);

           Storage::delete($template->imgpreview);
           unlink(storage_path('app/public/'.$template->imgpreview));

           $template->imgpreview = Storage::disk('public')->put('template/'.$template->id, $photo_image);
           $template->save();
       }
     }

     $did = Crypt::decryptString($eid);
     $template = TemplateDesign::find($did);
     $template->nama = $request->name;
     $template->html = $request->html;
     $template->event_category_id = $request->category;
     $template->link_url = $request->link_url;
     $template->save();

    return redirect('master/template')->withStatus([
      'alert'=>'alert-success',
      'status'=>'Saved !',
      'message'=>'Event template has been saved !'
    ]);
  }
}
