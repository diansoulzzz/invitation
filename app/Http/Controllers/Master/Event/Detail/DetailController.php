<?php

namespace App\Http\Controllers\Master\Event\Detail;

use App\Http\Controllers\Controller;
use App\Models\TemplateDesign;
use App\Models\TemplateCategory;
use App\Models\TemplateDesignDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use DataTables;
use Validator;

class DetailController extends Controller
{
  public function Index(Request $request) {
      return view('menus.masterdata.detail.index');
  }

  public function DataTable(Request $request) {
    $field = ['template_design_detail.id', 'template_design_detail.unique_name', 'template_design_detail.name', 'template_design_detail.example',
              'template_design_detail.types as tipe',
              'b.unique_name as parent_name', 'template_design.nama as template', 'event_category.nama as category'];
    $model = TemplateDesignDetail::select($field)
                                                 ->leftjoin('template_design_detail as b', 'template_design_detail.template_design_detail_id', '=', 'b.id')
                                                 ->join('template_design', 'template_design.id', '=', 'template_design_detail.template_design_id')
                                                 ->join('event_category', 'event_category.id', '=', 'template_design.event_category_id');
    $datatable = Datatables::of($model)
    ->addColumn('action', function ($item) {
      return '<a href="'.url('master/detail/entry/'.$item->eid).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
    })
    ->make(true);
    return $datatable;
  }

  public function IndexCreate(Request $request) {
    $detailfk = TemplateDesignDetail::all();
    $template = TemplateDesign::all();
    return view('menus.masterdata.detail.create', compact('detailfk', 'template'));
  }

  public function Store(Request $request) {
      $message = [
          // 'photo_image.required' => 'Photo required',
          // 'photo_image.image' => 'File must be an image',
      ];

      $this->validate($request, [
          'unique_name' => 'required|min:3|max:100',
          'html' => 'required|min:3|max:100',
      ], $message);

      $detail = new TemplateDesignDetail();
      $detail->unique_name = $request->unique_name;
      $detail->html = $request->html;
      $detail->name = $request->name;
      $detail->example = $request->example;
      $detail->types = $request->types;
      $detail->template_design_id = $request->template_design_id;
      if ($request->template_design_detail_id != '0') {
        $detail->template_design_detail_id = $request->template_design_detail_id;
      }
      $detail->save();

      return redirect('master/detail')->withStatus([
        'alert'=>'alert-success',
        'status'=>'Saved !',
        'message'=>'Event detail has been saved !'
      ]);
  }

  public function IndexEdit(Request $request, $eid = null) {
      $did = Crypt::decryptString($eid);
      $detail = TemplateDesignDetail::find($did);
      $detailfk = TemplateDesignDetail::all();
      $template = TemplateDesign::all();
      return view('menus.masterdata.detail.create', compact('detail', 'detailfk', 'template'));
  }

  public function StoreEdit(Request $request, $eid) {
    $message = [
        // 'photo_image.required' => 'Photo required',
        // 'photo_image.image' => 'File must be an image',
    ];

    $this->validate($request, [
      'unique_name' => 'required|min:3|max:100',
      'html' => 'required|min:3|max:100',
    ], $message);

    $validator = Validator::make($request->all(), [
         'photo_image' => 'required|image', //|dimensions:min_width=80,min_height=80
     ], $message);

     $did = Crypt::decryptString($eid);
     $detail = TemplateDesignDetail::find($did);
     $detail->unique_name = $request->unique_name;
     $detail->html = $request->html;
     $detail->name = $request->name;
     $detail->example = $request->example;
     $detail->types = $request->types;
     $detail->template_design_id = $request->template_design_id;
     if ($request->template_design_detail_id != '0') {
       $detail->template_design_detail_id = $request->template_design_detail_id;
     } else {
       $detail->template_design_detail_id = null;
     }
     $detail->save();

    return redirect('master/detail')->withStatus([
      'alert'=>'alert-success',
      'status'=>'Saved !',
      'message'=>'Event detail has been saved !'
    ]);
  }
}
