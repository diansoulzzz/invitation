<?php

namespace App\Http\Controllers\Master\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Event;
use App\Mail\UserVerificationMail;
use DataTables;
use Hash;
use Mail;
use Illuminate\Support\Facades\Crypt;

class UserController extends Controller
{
  public function Index(Request $request) {
      // $user = User::first();
      // return $user->eid;
      // $did = Crypt::decryptString($user->eid);
      // return $did;
      // return User::get();
      // return $this->DataTable($request);
      // return Event::with(['user','event_details','event_images','event_schedules','guests'])->get();
      return view('menus.masterdata.user.index');
  }

  public function DataTable(Request $request) {
    $field = ['id', 'nama', 'email', 'alamat','telepon'];
    $model = User::select($field);
    $datatable = Datatables::of($model)
    ->addColumn('action', function ($item) {
      return '<a href="'.url('master/user/entry/'.$item->eid).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
    })
    ->make(true);
    return $datatable;
  }

  public function IndexCreate(Request $request) {
      return view('menus.masterdata.user.create');
  }

  public function IndexEdit(Request $request, $eid = null) {
      $did = Crypt::decryptString($eid);
      $user = User::find($did);
      return view('menus.masterdata.user.create', compact('user'));
  }

  public function Store(Request $request) {
      $message = [
        // 'name.required' => 'The email field is required.',
        // 'name.min' => 'Minimum length is 3',
      ];

      $this->validate($request, [
          'name' => 'required|min:3|max:100',
          'email' => 'required|email|unique:users',
          'telephone' => 'required|max:13',
          'address' => 'required|max:200',
          'password' => 'required|confirmed|min:6',
      ], $message);

      $user = new User();
      $user->nama = $request->name;
      $user->telepon = $request->telephone;
      $user->jenis_kelamin = $request->gender;
      $user->tanggal_lahir = $request->birthday;
      $user->email = $request->email;
      $user->jabatan = $request->role;
      $user->password =  Hash::make($request->password);
      $user->api_token =  str_random(200);
      $user->alamat = $request->address;
      $user->status = 1;
      $user->save();

      $this->SendEmail($request);

      return redirect('master/user')->withStatus([
        'alert'=>'alert-success',
        'status'=>'Saved !',
        'message'=>'User has been saved !'
      ]);
  }

  protected function SendEmail(Request $request)
  {
    $did = User::max('id');
    $eid = Crypt::encryptString($did);
    Mail::to($request->email)->send(new UserVerificationMail($eid));
  }

  public function StoreEdit(Request $request, $eid) {

      $message = [
        // 'name.required' => 'The email field is required.',
        // 'name.min' => 'Minimum length is 3',
      ];

      $this->validate($request, [
          'name' => 'required|min:3|max:100',
          'telephone' => 'required|max:13',
          'address' => 'required|max:200',
      ], $message);

      $did = Crypt::decryptString($eid);
      $user = User::find($did);
      $user->nama = $request->name;
      $user->jenis_kelamin = $request->gender;
      $user->tanggal_lahir = $request->birthday;
      $user->telepon = $request->telephone;
      $user->jabatan = $request->role;
      $user->alamat = $request->address;
      $user->save();

      return redirect('master/user')->withStatus([
        'alert'=>'alert-success',
        'status'=>'Updated !',
        'message'=>'User has been updated !'
      ]);
  }
}
