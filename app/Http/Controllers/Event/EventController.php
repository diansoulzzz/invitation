<?php

namespace App\Http\Controllers\Event;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Guest;
use App\Models\EventDesign;
use App\Models\EventCategory;
use App\Models\TemplateDesign;
use App\Models\TemplateDesignDetail;
use Auth;
use DataTables;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

class EventController extends Controller
{
  public function IndexPreview(Request $request, $eid) {
    $did = Crypt::decryptString($eid);
    $event = Event::with(['event_designs','template_design.template_design_details'])->where('id',$did)->first();
    return $event;
    $rendered = $template_design->html;
    foreach ($template_design->template_design_details as $key => $detail) {
      $rendered = str_replace($detail->unique_name, $detail->html , $rendered);
    }
    $template_design->rendered = $rendered;
    return view('menus.template.detail', compact('template_design','link_url'));
  }

  public function IndexGuest(Request $request, $eid) {
    $did = Crypt::decryptString($eid);
    $event = Event::with(['event_designs','template_design.template_design_details'])->where('id',$did)->first();
    $guest = Guest::where('event_id', '=', $did)->get();
    return view('menus.event.guest', compact('event', 'did', 'guest'));
  }

  public function StoreGuest(Request $request) {
      $message = [
          // 'name.required' => 'The email field is required.',
      ];

      $this->validate($request, [
          // 'name' => 'required|min:3|max:100',
      ], $message);

      if ($request->id_action == 1) {
        $guest = new Guest();
        $guest->nama = $request->guest_name;
        $guest->alamat = $request->address;
        $guest->telepon = $request->phone;
        $guest->email = $request->email;
        $guest->whatsapp = $request->whatsapp;
        $guest->line = $request->line;
        $guest->jenis = $request->id_category;
        $guest->event_id = $request->id_event;
        $guest->save();

        $did = Crypt::encryptString($request->id_event);

        return redirect('event/guest/'.$did)->withStatus([
          'alert'=>'alert-success',
          'status'=>'Saved !',
          'message'=>'Guest has been saved !'
        ]);

      }

      // elseif ($request->id_action == 2) {
      //
      //   $data_guest = $request->id_deleted_guest;
      //   $guest = Guest::where('telepon', '=', $data_guest)->get();
      //
      //   $did = Crypt::encryptString($request->id_event);
      //
      //   return redirect('event/guest/'.$did)->withStatus([
      //     'alert'=>'alert-success',
      //     'status'=>'Saved !',
      //     'message'=>'Guest has been deleted !'
      //   ]);
      //
      // } elseif ($request->id_action == 3) {
      //
      //   $data_guest = $request->id_deleted_guest;
      //   $guest = Guest::where('telepon', '=', $data_guest)->delete();
      //
      //   $did = Crypt::encryptString($request->id_event);
      //
      //   return redirect('event/guest/'.$did)->withStatus([
      //     'alert'=>'alert-success',
      //     'status'=>'Saved !',
      //     'message'=>'Guest has been deleted !'
      //   ]);
      // }
  }

  public function Index(Request $request) {
       if ($request->has('design')) {
         $template_design = TemplateDesign::with('template_design_details')->where('link_url',$request->design)->first();
         $rendered = $template_design->html;
         foreach ($template_design->template_design_details as $key => $detail) {
           $rendered = str_replace($detail->unique_name, $detail->html , $rendered);
         }
         $template_design->rendered = $rendered;

         $template_detail = TemplateDesignDetail::join('template_design', 'template_design.id', '=', 'template_design_detail.template_design_id')
                       ->select('template_design.id', 'template_design.nama', 'template_design.link_url', 'template_design.html',
                                'template_design_detail.unique_name', 'template_design_detail.id as id_detail', 'template_design_detail.name as detail_name', 'template_design_detail.example',
                                'template_design_detail.html as detail_html', 'template_design_detail.types', 'template_design_detail.template_design_detail_id')
                       ->where('template_design.link_url', '=', $request->design)
                       ->orderBy('template_design_detail.unique_name', 'asc')
                       ->get();
         return view('menus.event.create', compact('template_design', 'template_detail'));
       } else {
         return view('menus.event.index');
       }
  }

  public function IndexEL(Request $request) {
    return redirect('login')->withStatus([
      'alert'=>'alert-danger',
      'status'=>'Information !',
      'message'=>'Please login first !'
    ]);
  }

  public function DataTable(Request $request) {
    $field = ['event.id', 'event.nama', 'template_design.nama as template', 'event_category.nama as category'];
    $model = Event::join('template_design', 'event.template_design_id', '=', 'template_design.id')
                  ->join('event_category', 'event.event_category_id', '=', 'event_category.id')
                  ->select($field)->where('users_id', '=', Auth::user()->id);
    $datatable = Datatables::of($model)
    ->addColumn('action', function ($item) {
      return '<a href="'.url('event/entry/'.$item->eid).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>'.
      '<a href="'.url('event/preview/'.$item->eid).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-fullscreen"></i> Preview</a>'.
      '<a href="'.url('event/guest/'.$item->eid).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i> Guest</a>';
    })
    ->make(true);
    return $datatable;
  }

  public function DataTableGuest(Request $request, $event_id) {
    $field = ['guest.id', 'guest.nama', 'guest.alamat', 'guest.telepon', 'guest.email', 'guest.whatsapp', 'guest.line', 'guest.jenis', 'guest.event_id'];
    $model = Guest::join('event', 'guest.event_id', '=', 'event.id')
                  ->select($field)->where('guest.event_id', '=', $event_id);
    $datatable = Datatables::of($model)
    ->addColumn('action', function ($item) {
      return '<a href="'.url('event/guest/edit/'.$item->event_id.'/'.$item->eid).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>'.
      '<a href="'.url('event/guest/delete/'.$item->event_id.'/'.$item->eid).'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-plus"></i> Delete</a>';
    })
    ->make(true);
    return $datatable;
  }

  public function EditGuest(Request $request, $event_id, $guest_id) {
      $guest_did = Crypt::decryptString($guest_id);

      $did = $event_id;
      $event = Event::with(['event_designs','template_design.template_design_details'])->where('id',$did)->first();
      $guest = Guest::where('id', '=', $guest_did)->first();
      $eid = Crypt::encryptString($did);
      // print_r($guest); exit();
      return view('menus.event.edit_guest', compact('event', 'guest', 'eid', 'did'));
  }

  public function UpdateGuest(Request $request, $event_id, $guest_id) {

      $message = [
        // 'name.required' => 'The email field is required.',
        // 'name.min' => 'Minimum length is 3',
      ];

      $did = Crypt::decryptString($guest_id);
      $guest = Guest::find($did);
      $guest->nama = $request->guest_name;
      $guest->alamat = $request->address;
      $guest->telepon = $request->phone;
      $guest->email = $request->email;
      $guest->whatsapp = $request->whatsapp;
      $guest->line = $request->line;
      $guest->jenis = $request->id_category;
      $guest->event_id = $request->id_event;
      $guest->save();
      $eid = Crypt::encryptString($event_id);

      return redirect('event/guest/'.$eid)->withStatus([
        'alert'=>'alert-success',
        'status'=>'Saved !',
        'message'=>'Guest has been updated !'
      ]);
  }

  public function DeleteGuest(Request $request, $event_id, $guest_id) {
      $guest_did = Crypt::decryptString($guest_id);

      $guest = Guest::where('id', '=', $guest_did)->delete();

      $did = $event_id;
      $event = Event::with(['event_designs','template_design.template_design_details'])->where('id',$did)->first();
      $guest = Guest::where('event_id', '=', $did)->get();
      $eid = Crypt::encryptString($did);

      return redirect('event/guest/'.$eid)->withStatus([
        'alert'=>'alert-success',
        'status'=>'Saved !',
        'message'=>'Guest has been deleted !'
      ]);
  }

  public function IndexCreate(Request $request) {
      $category = TemplateDesign::join('event_category', 'template_design.event_category_id', '=', 'event_category.id')
                    ->select('template_design.id', 'template_design.nama', 'template_design.link_url', 'template_design.event_category_id', 'event_category.nama as category_name')
                    ->orderBy('event_category.nama', 'asc')
                    ->get();
      return view('menus.event.template', compact('category'));
  }

  public function Store(Request $request) {
      $message = [
        // 'name.required' => 'The email field is required.',
        // 'name.min' => 'Minimum length is 3',
      ];

      $this->validate($request, [
          // 'name' => 'required|min:3|max:100',
          // 'description' => 'required|min:10',
          // 'link_url' => 'required|min:3',
      ], $message);

      $event = new Event();
      $event->nama = $request->event_title;
      $event->users_id = Auth::user()->id;
      $event->template_design_id = $request->event_id;
      $event->link_url = 'p/'.$request->event_name;
      $event->publish = 1;

      $event_category = TemplateDesign::select('template_design.id', 'template_design.event_category_id')->where('template_design.id', '=', $request->event_id)->get();

      $event->event_category_id = $event_category[0]['event_category_id'];
      $event->save();

      $template = $request->template;
      foreach ($template as $id => $value) {
        $event_design = new EventDesign();
        $event_design->template_design_detail_id = $id;
        $event_design->event_id = $event->id;
        $event_design->value = $value;
        $event_design->save();
      }

      return redirect('event')->withStatus([
        'alert'=>'alert-success',
        'status'=>'Saved !',
        'message'=>'Event has been saved !'
      ]);
  }
}
