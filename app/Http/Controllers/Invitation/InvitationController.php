<?php

namespace App\Http\Controllers\Invitation;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Invitation;

class InvitationController extends Controller
{
  public function SaveTheDate() {
      $data['date'] = Carbon::now()->addMonths(3)->format('F d, Y');
      $data['day'] = Carbon::now()->addMonths(3)->format('d');
      $data['month'] = Carbon::now()->addMonths(3)->format('m');
      $data['year'] = Carbon::now()->addMonths(3)->format('Y');

      return $data;
  }

  public function IndexPink(Request $Request) {
    $date = $this->SaveTheDate();
    return view('menus.invitation.pink.index', compact('date'));
  }

  public function IndexPurple(Request $Request) {
    $date = $this->SaveTheDate();
    return view('menus.invitation.purple.index', compact('date'));
  }

  public function IndexRed(Request $Request) {
    $date = $this->SaveTheDate();
    return view('menus.invitation.red.index', compact('date'));
  }
}
