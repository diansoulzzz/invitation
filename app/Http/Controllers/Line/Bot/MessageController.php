<?php

namespace App\Http\Controllers\Line\Bot;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;

class MessageController extends Controller
{
  public function DoReply(Request $request, $reply_token=null, $message_builder=null){
    $line_access_token = config('app.line_access_token');
    $line_channel_secret = config('app.line_channel_secret');
    $line_reply_token = $reply_token;
    $textMessageBuilder = $message_builder;
    $httpClient = new \LINE\LINEBot\HTTPClient\CurlHTTPClient($line_access_token);
    $bot = new \LINE\LINEBot($httpClient, ['channelSecret' => $line_channel_secret]);
    $response = $bot->replyMessage($line_reply_token, $textMessageBuilder);
    return $response->getHTTPStatus() . ' ' . $response->getRawBody();
  }
  public function GetProfile(Request $request, $userId=null){
    $line_user_id = $userId;
    // $line_user_id='U849cd94e466672d7f72b0b2c0829e2e1';
    $line_access_token = config('app.line_access_token');
    $url = 'https://api.line.me/v2/bot/profile/'.$line_user_id;
    $client = new Client([
      'headers' => [
        'Authorization' => 'Bearer '.$line_access_token,
      ]
    ]);
    $res = $client->request('GET', $url);
    $user_profile = json_decode($res->getBody(),true);
    return $user_profile;
  }
  // public function PushMessage(Request $request, $userId=null){
  //   $line_user_id = $userId;
  //   // $line_user_id='U849cd94e466672d7f72b0b2c0829e2e1';
  //   $line_access_token = config('app.line_access_token');
  //   $url = 'https://api.line.me/v2/bot/message/push';
  //   $client = new Client([
  //     'headers' => [
  //       'Content-Type' => 'application/json',
  //       'Authorization' => 'Bearer '.$line_access_token,
  //     ]
  //   ]);
  //   $res = $client->request('POST', $url);
  //   $link_token = json_decode($res->getBody(),true);
  //   return $link_token['linkToken'];
  // }
  public function LinkAccount(Request $request, $userId=null){
    $line_user_id = $userId;
    // $line_user_id='U849cd94e466672d7f72b0b2c0829e2e1';
    $line_access_token = config('app.line_access_token');
    $url = 'https://api.line.me/v2/bot/user/'.$line_user_id.'/linkToken';
    $client = new Client([
      'headers' => [
        'Authorization' => 'Bearer '.$line_access_token,
      ]
    ]);
    $res = $client->request('POST', $url);
    $link_token = json_decode($res->getBody(),true);
    return $link_token['linkToken'];
    // return json_decode($res->getBody(),true);
  }
}
