<?php

namespace App\Http\Controllers\Line;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Line\Bot\MessageController;
use QrCode;
class WebhookController extends Controller
{
  public function GetWebook(Request $request){
    // $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder('asd');
    // $imageMessageBuilder = new \LINE\LINEBot\MessageBuilder\ImageMessageBuilder('asd','asd');
    // return response()->json($imageMessageBuilder->buildMessage());
    // return response()->json($textMessageBuilder->buildMessage());
    // return QrCode::format('png')->size(100)->generate('Kode123123!');
    // Log::info(['post'=>$request->all()]);
    $messages = $request->input('events');
    // $messages =
    // array (
    //   0 =>
    //     array (
    //       'type' => 'message',
    //       'replyToken' => '0e91dd7354af4e4cbd9861fa259ca4dc',
    //       'source' =>
    //       array (
    //         'userId' => 'U849cd94e466672d7f72b0b2c0829e2e1',
    //         'type' => 'user',
    //       ),
    //       'timestamp' => 1536823383067,
    //       'message' =>
    //       array (
    //         'type' => 'text',
    //         'id' => '8565296410477',
    //         'text' => 'll',
    //       ),
    //     ),
    // );
    $reply = new MessageController();
    foreach ($messages as $key => $message) {
      $reply->DoReply($request,$message['replyToken']);
    }
    return response('Success', 200);
  }
  public function PostWebook(Request $request){
    // Log::info(['post'=>$request->all()]);
    Log::info($request->all());
    // $create_message = (object)[
    //   'type' => 'image',
    //   'originalContentUrl' => 'https://www.qrstuff.com/images/default_qrcode.png',
    //   'previewImageUrl' => 'https://www.qrstuff.com/images/default_qrcode.png',
    // ];
    // $en_create_message = json_encode($create_message);
    $messages = $request->input('events');
    $chat_bot = new MessageController();
    // $textMessageBuilder = new \LINE\LINEBot\MessageBuilder\TextMessageBuilder($messages);
    $imageMessageBuilder = new \LINE\LINEBot\MessageBuilder\ImageMessageBuilder('https://www.qrstuff.com/images/default_qrcode.png','https://www.qrstuff.com/images/default_qrcode.png');
    $results = [];
    foreach ($messages as $key => $message) {
      $profile = $chat_bot->GetProfile($request,$message['source']['userId']);
      $reply_message = 'Hello : '.$profile['displayName'];
      // $results[]=$chat_bot->DoReply($request,$message['replyToken'],$reply_message);
      $results[]=$chat_bot->DoReply($request,$message['replyToken'],$imageMessageBuilder);
    }
    Log::info(['post'=>$results]);
    return response('Success', $results);
  }
}
