<?php

namespace App\Http\Controllers\Timeline;

use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\EventDetail;
use Carbon\Carbon;

class TimelineController extends Controller
{
  public function Index() {
    $event = Event::with(['event_detail','event_images','event_schedules','guests','template_design','template_kategori','user'])
                    ->whereMonth('created_at', '>=', Carbon::now()->format('M'))
                    ->paginate(1);

    $post = Event::with(['event_detail','event_images','event_schedules','guests','template_design','template_kategori','user'])
                    ->whereMonth('created_at', '>=', Carbon::now()->format('M'))
                    ->limit(5)->get();

    return view('menus.timeline.index', compact('event', 'post'));
  }
}
