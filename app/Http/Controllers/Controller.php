<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\EventCategory;
use App\Models\TemplateDesign;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    private $event_category;
    private $template_trending;
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            $this->event_category = EventCategory::get();
            $this->template_trending = TemplateDesign::get();
            view()->share('gbl_event_category', $this->event_category);
            view()->share('gbl_template_trending', $this->template_trending);
            return $next($request);
        });
    }
}
