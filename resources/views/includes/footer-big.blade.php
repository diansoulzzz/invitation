<section class="gla_section gla_image_bck gla_wht_txt gla_section_extra_sml" data-color="#292929">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4">
        <h4>Contact Us</h4>
        <p>
          Kompleks Pergudangan Margomulyo Jaya G-5 <br>
          Jln Sentong Asri, Surabaya <br>
          +6281 330 966 619<br>
          info@3vite.com
        </p>
      </div>
      <div class="col-md-1 col-sm-1">
      </div>
      <div class="col-md-3 col-sm-3">
        <h4>Latest Posts</h4>
        <ul class="list-unstyled">
          <li><a href="">Made with Love in Toronto</a></li>
          <li><a href="">Startup News & Emerging Tech</a></li>
          <li><a href="">Bitcoin Will Soon Rule The World</a></li>
          <li><a href="">Wearable Technology On The Rise</a></li>
          <li><a href="">Learn Web Design in 30 Days!</a></li>
        </ul>
      </div>
      <div class="col-md-1 col-sm-1">
      </div>
      <div class="col-md-3 col-sm-3">
        <h4>Our Newsletter</h4>
        <form action="">
          <input placeholder="Enter Your Email" class="form-control form-opacity no-margin newsletter_input" type="email" required/>
          <a href="" class="btn">Subscribe</a>
        </form>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="gla_block text-center">
          <div class="gla_social_btns">
            <a href=""><i class="ti ti-twitter"></i></a>
            <a href=""><i class="ti ti-facebook"></i></a>
            <a href=""><i class="ti ti-youtube"></i></a>
            <a href=""><i class="ti ti-jsfiddle"></i></a>
            <a href=""><i class="ti ti-vimeo"></i></a>
            <a href=""><i class="ti ti-pinterest"></i></a>
            <a href=""><i class="ti ti-instagram"></i></a>
            <a href=""><i class="ti ti-soundcloud"></i></a>
          </div>
          <p>© 3Vite 2018</p>
        </div>
      </div>
    </div>
  </div>
</section>
