<meta charset="utf-8">
<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel="icon" href="{{asset('assets/images/logo/favicon.ico')}}" type="image/x-icon">
<link href="{{asset('assets/css/glanz_library.css')}}" rel="stylesheet">
<link href="{{asset('assets/fonts/themify-icons.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/glanz_style.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/dosis-font.css')}}" rel="stylesheet">
<link href="{{asset('assets/fonts/marsha/stylesheet.css')}}" rel="stylesheet">
