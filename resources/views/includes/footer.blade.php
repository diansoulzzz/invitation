<section id="simple" class="gla_image_bck gla_section_extra_sml" data-color="#e5e5e5">
  <div class="container">
    <div class="row">
      <div class="col-md-4 col-sm-4">
        <p>© 3Vite. 2018</p>
      </div>
      <div class="col-md-4 col-sm-4 text-center">
        <div class="gla_social_btns">
          <a href="#"><i class="ti ti-twitter"></i></a>
          <a href="#"><i class="ti ti-facebook"></i></a>
          <a href="#"><i class="ti ti-youtube"></i></a>
          <a href="#"><i class="ti ti-jsfiddle"></i></a>
          <a href="#"><i class="ti ti-vimeo"></i></a>
          <a href="#"><i class="ti ti-pinterest"></i></a>
          <a href="#"><i class="ti ti-instagram"></i></a>
          <a href="#"><i class="ti ti-soundcloud"></i></a>
        </div>
      </div>
      <div class="col-md-4 col-sm-4">
      </div>
    </div>
  </div>
</section>
