<header>
  <nav {!! $nav !!}>
    <!-- gla_transp_nav -->
    <div class="container">
      <div class="gla_logo_container clearfix">
        <img src="{{asset('assets/images/logo/logo.png')}}" alt="" class="gla_logo_rev">
        <div class="gla_logo_txt">
          <a href="{{url('/')}}" class="gla_logo">3Vite</a>
          <div class="gla_logo_und">The Best Digital Invitation Card</div>
        </div>
      </div>
      <div class="gla_main_menu gla_main_menu_mobile">
        <div class="gla_main_menu_icon">
          <i></i><i></i><i></i><i></i>
          <b>Menu</b>
          <b class="gla_main_menu_icon_b">Back</b>
        </div>
      </div>
      <div class="gla_main_menu_content gla_image_bck" data-color="rgba(0,0,0,0.9)"></div>
      <div class="gla_main_menu_content_menu gla_wht_txt text-right">
        <div class="container">
          @include('includes.navigation')
          <div class="gla_main_menu_content_menu_copy">
            <form>
              <input type="text" class="form-control" placeholder="Enter Your Keywords">
              <button type="submit" class="btn">Search</button>
            </form>
            <br>
            <p>© 3Vite 2018</p>
            <div class="gla_footer_social">
              <a href=""><i class="ti ti-facebook gla_icon_box"></i></a>
              <a href=""><i class="ti ti-instagram gla_icon_box"></i></a>
              <a href=""><i class="ti ti-google gla_icon_box"></i></a>
              <a href=""><i class="ti ti-youtube gla_icon_box"></i></a>
              <a href=""><i class="ti ti-twitter gla_icon_box"></i></a>
              <a href=""><i class="ti ti-pinterest gla_icon_box"></i></a>
            </div>
          </div>
        </div>
      </div>
      <div class="gla_default_menu">
        @include('includes.navigation')
      </div>
      <!-- <div class="gla_search_block">
        <div class="gla_search_block_link gla_search_parent">
          <i class="ti ti-search"></i>
          <ul>
            <li>
              <form>
                <input type="text" class="form-control" placeholder="Enter Your Keywords">
                <button type="submit" class="btn">
                <i class="ti ti-search"></i>
                </button>
              </form>
            </li>
          </ul>
        </div>
      </div> -->
      <div class="gla_search_block">
        <div class="gla_search_block_link gla_search_parent">
          <form method="get" action="{{url('search')}}">
            <div class="form-group has-feedback" style="margin-top:-10px;">
              <input type="text" name="q" class="form-control" style="height:35px;" value="{{Request::input('q')}}">
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
          </form>
        </div>
      </div>
    </div>
  </nav>
</header>
