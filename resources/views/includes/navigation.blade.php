 <ul>
   <!-- <li><a href="{{ url('/')}}">Home</a></li> -->
   <li class="gla_parent">
      <a>Invitation</a>
      <ul>
         @foreach ($gbl_event_category as $key => $category)
           <li><a href="{{ url('category/'.$category->link_url) }}">{{$category->nama}}</a></li>
         @endforeach
      </ul>
   </li>
    @if (!(Auth::check()) || (Auth::check() && Auth::user()->jabatan == 2))
      <!-- <li class="gla_parent">
         <a href="">Template</a>
         <ul>
            <li><a href="{{ url('template/animatedflower') }}">Animated Flower</a></li>
            <li><a href="{{ url('template/pinkanimatedflower') }}">Pink Animated Flower</a></li>
            <li><a href="{{ url('template/goldenbadge') }}">Golden Badge</a></li>
            <li><a href="{{ url('template/greatflower') }}">Great Flower</a></li>
            <li><a href="{{ url('template/simpleflower') }}">Simple Flower</a></li>
            <li><a href="{{ url('template/abstractflower') }}">Abstract Flower</a></li>
            <li><a href="{{ url('template/parallax') }}">Parallax</a></li>
            <li><a href="{{ url('template/landing') }}">Landing</a></li>
         </ul>
      </li> -->
      <!-- <li class="gla_parent">
         <a href="">Partner</a>
         <ul>
            <li><a href="{{ url('partner/weddingplanner') }}">Wedding Planner</a></li>
            <li><a href="{{ url('partner/weddingflower') }}">Wedding Flower</a></li>
            <li><a href="{{ url('partner/weddingcake') }}">Wedding Cake</a></li>
            <li><a href="{{ url('partner/shop')}}">Wedding Shop</a></li>
         </ul>
      </li> -->
    @endif

    @if (Auth::check())
    <!-- <li><a href="{{ url('event')}}">Make Event</a></li> -->
    @else
    <!-- <li><a href="{{ url('event/x')}}">Make Event</a></li> -->
    @endif

    <li><a href="{{ url('timeline')}}">Timeline</a></li>

    @if (Auth::check())
      @if(Auth::user()->jabatan == 1)
      <li class="gla_parent">
         <a href="">Master Data</a>
         <ul>
            <li><a href="{{ url('master/user') }}">User</a></li>
            <li><a href="{{ url('master/category') }}">Event Category</a></li>
            <li><a href="{{ url('master/template') }}">Event Template</a></li>
            <li><a href="{{ url('master/detail') }}">Event Detail</a></li>
            <li><a href="{{ url('master/weddingplanner') }}">Wedding Planner</a></li>
            <li><a href="{{ url('master/weddingflower') }}">Wedding Flower</a></li>
            <li><a href="{{ url('master/weddingcake') }}">Wedding Cake</a></li>
            <li><a href="{{ url('master/item') }}">Item</a></li>
         </ul>
      </li>
      @endif
    @endif

    <li class="gla_parent">
       <a href="">Help</a>
       <ul>
          <li><a href="{{ url('contact-us') }}">Contact Us</a></li>
          <li><a href="{{ url('about-us') }}">About Us</a></li>
       </ul>
    </li>
    <!-- <li><a href="{{ url('aboutus') }}">About Us</a></li> -->

    @if (!(Auth::check()))
      <li><a href="{{ url('login')}}">Login / Register</a></li>
    @else
      <li class="gla_parent">
         <a href="">{{ucwords(Auth::user()->nama)}}</a>
         <ul>
            <li><a href="{{ url('event')}}">Event</a></li>
            <li><a href="{{ url('profile/data') }}">Profile</a></li>
            <li><a href="{{ url('profile/account') }}">Change Password</a></li>
            <li><a href="{{ url('logout')}}">Logout</a></li>
         </ul>
      </li>
    @endif

 </ul>
