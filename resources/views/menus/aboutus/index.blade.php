@extends('layouts.default')

@section('title','About Us')

@section('music')
<div class="gla_music_icon">
    <i class="ti ti-music"></i>
</div>
<div class="gla_music_icon_cont">
    <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/108238095&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" allow="autoplay"></iframe>
</div>
@endsection

@section('content')
<div class="gla_page_title gla_image_bck gla_wht_txt" data-color="#282828">
  <div class="container text-left">
    <div class="row">
      <div class="col-md-8">
        <h1 class="gla_h1_title">About Us</h1>
      </div>
    </div>
  </div>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section gla_image_bck" data-color="#fafafd">
        <div class="container text-center">
            <p><img src="{{asset('assets/images/animations/flower5.gif')}}" data-bottom-top="@src:images/animations/flower5.gif; opacity:1" class="gla_animated_flower" height="150" alt=""></p>
            <h2>Paperless means reducing expenses</h2>
            <h3 class="gla_subtitle">What we do</h3>
            <h4>We create experiences that make inviting people and <br /> attending events vecome simpler and easier.</h4>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.4" data-image="{{asset('assets/images/wedding/mark_liz/10502734756_d295257d36_k.jpg')}}">
        <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
        <div class="container text-left">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <h2>Paperless means reducing expenses</h2>
                    <h3 class="gla_subtitle">How it works</h3>
                    <h4>Creating and spreading the invitations right on your fingertips. <br /> <br /> Attending events without queuing to write the guest - book, but showing and scanning the barcode in the invitations.</h4>
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed" data-stellar-background-ratio="0.2" data-image="{{asset('assets/images/wedding/ian_kelsey/14815177364_46f0b9d71e_k.jpg')}}">
        <div class="container">
            <div class="row gla_auto_height">
              <div class="col-md-6 gla_image_bck" data-color="#eee">
                  <div class="gla_simple_block">
                      <h2>Fransisca Alexandra Stefanie</h2>
                      <h3>Founder | CEO</h3>
                      <h4>Connect with me</h4> <br>
                      <div class="gla_footer_social">
                          <a href="#"><i class="ti ti-facebook gla_icon_box"></i></a>
                          <a href="#"><i class="ti ti-instagram gla_icon_box"></i></a>
                          <a href="#"><i class="ti ti-google gla_icon_box"></i></a>
                      </div>
                  </div>
              </div>
              <div class="col-md-6 gla_image_bck" data-image="{{asset('assets/images/wedding/ian_kelsey/14818410145_0413998881_k.jpg')}}">
              </div>
                <div class="col-md-6 col-md-push-6 gla_image_bck" data-color="#eee">
                    <div class="gla_simple_block">
                        <h2>Daivalentineno Janitra Salim</h2>
                        <h3>Co - Founder - COO</h3>
                        <h4>Connect with me</h4> <br>
                        <div class="gla_footer_social">
                            <a href="#"><i class="ti ti-facebook gla_icon_box"></i></a>
                            <a href="#"><i class="ti ti-instagram gla_icon_box"></i></a>
                            <a href="#"><i class="ti ti-google gla_icon_box"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-md-pull-6 gla_image_bck" data-image="{{asset('assets/images/wedding/ian_kelsey/14815312201_0608beeccd_k.jpg')}}">
                </div>
                <div class="col-md-6 gla_image_bck" data-color="#eee">
                    <div class="gla_simple_block">
                        <h2>Dian Yulius</h2>
                        <h3>Co - Founder - CTO</h3>
                        <h4>Connect with me</h4> <br>
                        <div class="gla_footer_social">
                            <a href="#"><i class="ti ti-facebook gla_icon_box"></i></a>
                            <a href="#"><i class="ti ti-instagram gla_icon_box"></i></a>
                            <a href="#"><i class="ti ti-google gla_icon_box"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 gla_image_bck" data-image="{{asset('assets/images/wedding/ian_kelsey/14818410145_0413998881_k.jpg')}}">
                </div>
            </div>
        </div>
    </section>
</section>
@endsection
