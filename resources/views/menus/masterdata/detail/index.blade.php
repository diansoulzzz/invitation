@extends('layouts.default')

@section('title','Master Data Template Detail')

@section('content')
<div class="gla_page_title gla_image_bck gla_wht_txt" data-color="#282828">
   <div class="container text-left">
      <div class="row">
         <div class="col-md-12">
            <h1 class="gla_h1_title">Master Data Template Detail</h1>
            <h3>This page contains of template detail information</h3>
         </div>
      </div>
   </div>
</div>
<section id="gla_content" class="gla_content">
  <section class="gla_section">
     <div class="container">
       <div class="gla_shop_header">
           <div class="row">
               <div class="col-md-12 text-right">
                  <a href="{{url('master/detail/entry')}}" class="btn btn-success pull-right" data-toggle>Add New Template Detail</a>
               </div>
           </div>
       </div>

       @if(session('status'))
       <div class="alert {{session('status')['alert']}}">
         <span aria-hidden="true" class="alert-icon icon_blocked"></span><strong>{{session('status')['status']}} </strong>{{session('status')['message']}}
       </div>
       @endif

       <div class="row">
          <div>
             <div class="col-md-12 gla_main_sidebar">
                <table class="table" id="table">
                   <thead>
                      <tr>
                         <th>Unique Name</th>
                         <th>Name</th>
                         <th>Example</th>
                         <th>Type</th>
                         <th>Parent Name</th>
                         <th>Template</th>
                         <th>Category</th>
                         <th>Action</th>
                      </tr>
                   </thead>
                   <tfoot>
                      <tr>
                        <th>Unique Name</th>
                        <th>Name</th>
                        <th>Example</th>
                        <th>Type</th>
                        <th>Parent Name</th>
                        <th>Template</th>
                        <th>Category</th>
                        <th>Action</th>
                      </tr>
                   </tfoot>
                </table>
             </div>
          </div>
       </div>
     </div>
  </section>
</section>
@endsection

@push('css')
<link href="{{asset('assets/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/jquery.dataTables.min.css')}}" rel="stylesheet">
@endpush

@push('script')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script>
$(function () {
  thisform.init();
}), thisform = {
  init : function () {
    $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{url('master/detail/dt')}}",
        columns: [
            {data: 'unique_name', name: 'unique_name'},
            {data: 'name', name: 'name'},
            {data: 'example', name: 'example'},
            {data: 'tipe', name: 'tipe'},
            {data: 'parent_name', name: 'parent_name'},
            {data: 'template', name: 'template'},
            {data: 'category', name: 'category'},
            {data: 'action', name: 'action'},
        ]
    });
  },
}
</script>
@endpush
