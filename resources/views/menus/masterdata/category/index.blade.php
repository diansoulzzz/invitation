@extends('layouts.default')

@section('title','Master Data Event Category')

@section('content')
<div class="gla_page_title gla_image_bck gla_wht_txt" data-color="#282828">
   <div class="container text-left">
      <div class="row">
         <div class="col-md-8">
            <h1 class="gla_h1_title">Master Data Event Category</h1>
            <h3>This page contains of event category information</h3>
         </div>
      </div>
   </div>
</div>
<section id="gla_content" class="gla_content">
  <section class="gla_section">
     <div class="container">
       <div class="gla_shop_header">
           <div class="row">
               <div class="col-md-12 text-right">
                  <a href="{{url('master/category/entry')}}" class="btn btn-success pull-right" data-toggle>Add New Event Category</a>
               </div>
           </div>
       </div>

       @if(session('status'))
       <div class="alert {{session('status')['alert']}}">
         <span aria-hidden="true" class="alert-icon icon_blocked"></span><strong>{{session('status')['status']}} </strong>{{session('status')['message']}}
       </div>
       @endif

       <div class="row">
          <div>
             <div class="col-md-12 gla_main_sidebar">
                <table class="table" id="table">
                   <thead>
                      <tr>
                         <th>Name</th>
                         <th>Description</th>
                         <th>Link URL</th>
                         <th>Status</th>
                         <th>Action</th>
                      </tr>
                   </thead>
                   <tfoot>
                      <tr>
                         <th>Name</th>
                         <th>Description</th>
                         <th>Link URL</th>
                         <th>Status</th>
                         <th>Action</th>
                      </tr>
                   </tfoot>
                </table>
             </div>
          </div>
       </div>
     </div>
  </section>
</section>
@endsection

@push('css')
<link href="{{asset('assets/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/jquery.dataTables.min.css')}}" rel="stylesheet">
@endpush

@push('script')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script>
$(function () {
  thisform.init();
}), thisform = {
  init : function () {
    $('#table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "{{url('master/category/dt')}}",
        columns: [
            {data: 'nama', name: 'nama'},
            {data: 'keterangan', name: 'keterangan'},
            {data: 'link_url', name: 'link_url'},
            {data: 'publish', name: 'publish'},
            {data: 'action', name: 'action'},
        ]
    });
  },
}
</script>
@endpush
