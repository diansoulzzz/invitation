@extends('layouts.default')

@section('title','Master Data Event Category')

@section('content')
<div class="gla_page_title gla_image_bck gla_wht_txt" data-color="#282828">
   <div class="container text-left">
      <div class="row">
         <div class="col-md-12">
            <h1 class="gla_h1_title">Create Data Event Category</h1>
            <h3>This page allows you to create and update an event category</h3>
         </div>
      </div>
   </div>
</div>
<section id="gla_content" class="gla_content">
  <section class="gla_section">
     <div class="container">
       <div class="row">
         <div class="col-md-12 gla_main_sidebar">
           <form id="form-validated" action="{{url()->current()}}" method="POST">
               @csrf
               @if ($errors->any())
                  <div class="alert alert-danger">
                    <span aria-hidden="true" class="alert-icon icon_blocked"></span><strong>Error! </strong>There are some problems.
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (isset($category))
              <input type="hidden" name="eid" value="{{$category->eid}}">
              @endif
               <div class="form-group row">
                   <div class="col-md-6">
                       <label>Name* <small class="form-text text-muted"> Ex. Wedding Party</small></label>
                       <input type="text" name="name" class="form-control form-opacity" required data-parsley-maxlength="100" data-parsley-minlength="3" value="{{old('name',(isset($category)?$category->nama:''))}}">
                   </div>
               </div>
               <div class="form-group row">
                   <div class="col-md-12">
                       <label>Description* <small class="form-text text-muted"> Ex. Wedding is an . . .</small></label>
                       <textarea name="description" rows="3" cols="80" class="form-control form-opacity" data-parsley-minlength="15" required>{{old('description',(isset($category)? $category->keterangan : ''))}}</textarea>
                   </div>
               </div>
               <div class="form-group row">
                   <div class="col-md-3">
                       <label>Link URL* <small class="form-text text-muted"> Ex. Wedding</small></label>
                       <input type="text" name="link_url" class="form-control form-opacity" required data-parsley-maxlength="100" data-parsley-minlength="3" value="{{old('link_url',(isset($category)?$category->link_url:''))}}">
                   </div>
               </div>

               @if(isset($category))
               <div class="form-group row">
                   <div class="col-md-3">
                       <label>Status* <small class="form-text text-muted"> Ex. Publish</small></label>
                       <select class="form-control form-opacity" name="publish">
                         <option value="1">Publish</option>
                         <option value="2">Not Publish</option>
                       </select>
                   </div>
               </div>
               @endif
               
               <div class="form-group row">
                   <div class="col-md-12 text-center">
                       <input type="submit" class="btn submit" value="Save">
                   </div>
               </div>
           </form>
         </div>
       </div>
     </div>
  </section>
</section>
@endsection

@push('css')
<style>
  select.form-control {
    border-radius: 25px !important;
  }

  input.parsley-success,
  select.parsley-success,
  textarea.parsley-success {
    /* color: green; */
    background-color: #DFF0D8;
    border: 1px solid #D6E9C6;
  }

  input.parsley-error,
  select.parsley-error,
  textarea.parsley-error {
    /* color: red; */
    background-color: #F2DEDE;
    border: 1px solid #EED3D7;
  }

  .parsley-errors-list {
    /* margin: 0px 0 3px; */
    padding: 0;
    color: red;
    font-weight: bold;
    list-style-type: none;
    font-size: 0.9em;
    line-height: 0.9em;
    opacity: 0;

    transition: all .3s ease-in;
    -o-transition: all .3s ease-in;
    -moz-transition: all .3s ease-in;
    -webkit-transition: all .3s ease-in;
  }

  .parsley-errors-list.filled {
    opacity: 1;
  }
</style>
@endpush

@push('script')
<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/js/parsley.min.js')}}"></script>
<script>
$(function () {
  thisform.init();
}), thisform = {
  init : function () {
    $('#form-validated').parsley();
    $('.form-masked').inputmask( {'autoUnmask' : true});
  },
}
</script>
@endpush
