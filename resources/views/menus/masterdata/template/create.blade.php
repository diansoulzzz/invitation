@extends('layouts.default')

@section('title','Master Data Event Template')

@section('content')
<div class="gla_page_title gla_image_bck gla_wht_txt" data-color="#282828">
   <div class="container text-left">
      <div class="row">
         <div class="col-md-12">
            <h1 class="gla_h1_title">Create Data Event Template</h1>
            <h3>This page allows you to create and update an event template</h3>
         </div>
      </div>
   </div>
</div>
<section id="gla_content" class="gla_content">
  <section class="gla_section">
     <div class="container">
       <div class="row">
         <div class="col-md-12 gla_main_sidebar">
           <form id="form-validated" action="{{url()->current()}}" method="POST" enctype="multipart/form-data">
               @csrf
               @if ($errors->any())
                  <div class="alert alert-danger">
                    <span aria-hidden="true" class="alert-icon icon_blocked"></span><strong>Error! </strong>There are some problems.
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              @if (isset($template))
              <input type="hidden" name="eid" value="{{$template->eid}}">
              @endif
               <div class="form-group row">
                   <div class="col-md-6">
                       <label>Name* <small class="form-text text-muted"> Ex. Parallax</small></label>
                       <input type="text" name="name" class="form-control form-opacity" required data-parsley-maxlength="100" data-parsley-minlength="3" value="{{old('name',(isset($template)?$template->nama : ''))}}">
                   </div>
                   <div class="col-md-6">
                       <label>Category* <small class="form-text text-muted"> Ex. Wedding Party</small></label>
                       <select name="category" class="form-control form-opacity" required>
                        @foreach($category as $cat)
                           <option value="{{$cat['id']}}" {{(old('category',(isset($template)? $template->template_kategori_id : '')) == $cat['id']) ? 'selected' : ''}}>{{$cat['nama']}}</option>
                        @endforeach
                       </select>
                   </div>
               </div>
               <div class="form-group row">
                   <div class="col-md-12">
                       <label>HTML*</label>
                       <textarea name="html" rows="5" cols="100" class="form-control form-opacity" data-parsley-minlength="25" required>{{old('html',(isset($template)? $template->html : ''))}}</textarea>
                   </div>
               </div>
               <div class="form-group row">
                 <div class="col-md-6">
                     <label>Link URL* <small class="form-text text-muted"> Ex. parallax</small></label>
                     <input type="text" name="link_url" class="form-control form-opacity" required data-parsley-maxlength="100" data-parsley-minlength="3" value="{{old('link_url',(isset($template)?$template->link_url : ''))}}">
                 </div>
                   <div class="col-md-6">
                       <label>File*</label>
                       <input type="file" name="photo_image" class="form-control form-opacity" isset($template)? '' : 'required'  accept="image/x-png,image/gif,image/jpeg">
                   </div>
               </div>
               @if (isset($template))
               <div class="form-group row">
                   <div class="col-md-6">
                       <label>Old File</label>
                       <img src="{{url('storage/app/public/'.$template->imgpreview)}}" style="border-radius:20px;"/>
                   </div>
               </div>
               @endif
               <div class="form-group row">
                   <div class="col-md-12 text-center">
                       <input type="submit" class="btn submit" value="Save">
                   </div>
               </div>
           </form>
         </div>
       </div>
     </div>
  </section>
</section>
@endsection

@push('css')
<style>
  select.form-control {
    border-radius: 25px !important;
  }

  input.parsley-success,
  select.parsley-success,
  textarea.parsley-success {
    /* color: green; */
    background-color: #DFF0D8;
    border: 1px solid #D6E9C6;
  }

  input.parsley-error,
  select.parsley-error,
  textarea.parsley-error {
    /* color: red; */
    background-color: #F2DEDE;
    border: 1px solid #EED3D7;
  }

  .parsley-errors-list {
    /* margin: 0px 0 3px; */
    padding: 0;
    color: red;
    font-weight: bold;
    list-style-type: none;
    font-size: 0.9em;
    line-height: 0.9em;
    opacity: 0;

    transition: all .3s ease-in;
    -o-transition: all .3s ease-in;
    -moz-transition: all .3s ease-in;
    -webkit-transition: all .3s ease-in;
  }

  .parsley-errors-list.filled {
    opacity: 1;
  }
</style>
@endpush

@push('script')
<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/js/parsley.min.js')}}"></script>
<script>
$(function () {
  thisform.init();
}), thisform = {
  init : function () {
    $('#form-validated').parsley();
    $('.form-masked').inputmask( {'autoUnmask' : true});
  },
}
</script>
@endpush
