@extends('layouts.default')

@section('title','Pink Invitation')

@section('music')
<div class="gla_music_icon">
    <i class="ti ti-music"></i>
</div>
<div class="gla_music_icon_cont">
    <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/108238095&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" allow="autoplay"></iframe>
</div>
@endsection

@section('content')
<div class="gla_page gla_image_bck" data-image="{{asset('assets/images/wedding_m/sweet-ice-cream-photography-87191.jpg')}}" id="gla_page" >
<div class="gla_over" data-color="#3d0402" data-opacity="0.3"></div>
    <a href="#gla_page" class="gla_top ti ti-angle-up gla_go"></a>
    <a href="#gla_page" class="gla_top ti ti-angle-up gla_go"></a>

    <section id="gla_content" class="gla_content">
        <div class="gla_invitation_container">
            <div class="gla_invitation_i gla_invitation_ii gla_image_bck" data-image="{{asset('assets/images/invitations/inv_i/back4.jpg')}}">
                <p><img src="{{asset('assets/images/invitations/inv_i/save_the_date_red.gif')}}" alt="" height="240"></p>
                <br><br>
                <h2>Kevin & Julie</h2>
                <h4>{{$date['date']}}</h4>
                <h4>St. Thomas's Church, Bristol</h4>
            </div>
        </div>
    </section>

</div>
@endsection
