@extends('layouts.default')

@section('title','Parallax')

@section('music')
<div class="gla_music_icon">
    <i class="ti ti-music"></i>
</div>
<div class="gla_music_icon_cont">
    <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/66757544&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" allow="autoplay"></iframe>
</div>
@endsection

@section('content')
<div class="gla_slider gla_image_bck gla_wht_txt gla_fixed"  data-image="{{asset('assets/images/wedding/mark_liz/10503327696_43f22603da_k.jpg')}}" data-stellar-background-ratio="0.8">
  <div class="gla_over" data-color="#9abab6" data-opacity="0.2"></div>
  <div class="container">
    <div class="gla_slide_txt gla_slide_center_bottom text-center">
      <div class="gla_slide_midtitle">We're Getting Married</div>
      <div class="gla_slide_subtitle">Mark & Liz</div>
    </div>
  </div>
  <a class="gla_scroll_down gla_go" href="#gla_content">
  <b></b>
  Scroll
  </a>
</div>
<section id="gla_content" class="gla_content">
  <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.4" data-image="{{asset('assets/images/wedding/mark_liz/10502734756_d295257d36_k.jpg')}}">
    <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
    <div class="container text-left">
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <h2>Our Story</h2>
          <h3 class="gla_subtitle">The Fourth of July</h3>
          <p>My fiancé proposed on the Fourth of July. My mother asked us to go to the backyard to get some chairs and he took me by the shed where we could see all of the fireworks. He kissed me, then he took the ring box out of his pocket and asked me to be his wife. He was shaking a little. The proposal was a little silly but perfect, just like him." — Jeska Cords</p>
        </div>
      </div>
    </div>
  </section>
  <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.4" data-image="{{asset('assets/images/wedding/mark_liz/10503392955_fb46d6b158_k.jpg')}}">
    <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
    <div class="container text-right">
      <p><img src="{{asset('assets/images/animations/savethedate_wh.gif')}}" data-bottom-top="@src:{{asset('assets/images/animations/savethedate_wh.gif')}}" height="150" alt=""></p>
      <h2>{{$date['date']}}</h2>
      <h3 class="gla_subtitle">St. Thomas's Church,<br>Bristol, U.K.</h3>
      <div class="gla_countdown" data-year="{{$date['year']}}" data-month="{{$date['month']}}" data-day="{{$date['day']}}"></div>
    </div>
  </section>
  <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.4" data-image="{{asset('assets/images/wedding/mark_liz/10502663484_d6f61198b4_k.jpg')}}">
    <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
    <div class="container text-left">
      <p><img src="{{asset('assets/images/animations/just_wh.gif')}}" data-bottom-top="@src:{{asset('assets/images/animations/just_wh.gif')}}" height="150" alt=""></p>
      <h3>You’re wonderful. <br>Can you be wonderful <br>FOREVER?"</h3>
      <p class="gla_subtitle">— Brennan. A true master of words.</p>
    </div>
  </section>
  <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.4" data-image="{{asset('assets/images/wedding/mark_liz/10503022515_d96d642143_k.jpg')}}">
    <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
    <div class="container text-right">
      <div class="row">
        <div class="col-md-6 col-sm-12">
          <br>
        </div>
        <div class="col-md-6 col-sm-12">
          <p><img src="{{asset('assets/images/animations/rsvp_wh.gif')}}" data-bottom-top="@src:{{asset('assets/images/animations/rsvp_wh.gif')}}" height="180" alt=""></p>
          <form action="https://formspree.io/your@mail.com" method="POST">
            <div class="row">
              <div class="col-md-6">
                <label>Your name*</label>
                <input type="text" name="name" class="form-control form-opacity">
              </div>
              <div class="col-md-6">
                <label>Your e-mail*</label>
                <input type="text" name="email" class="form-control form-opacity">
              </div>
              <div class="col-md-6">
                <label>Will you attend?</label>
                <input type="radio" name="attend" value="Yes, I will be there">
                <span>Yes, I will be there</span><br>
                <input type="radio" name="attend" value="Sorry, I can't come">
                <span>Sorry, I can't come</span>
              </div>
              <div class="col-md-6">
                <label>Meal preference</label>
                <select name="meal" class="form-control form-opacity">
                  <option value="I eat anything">I eat anything</option>
                  <option value="Beef">Beef</option>
                  <option value="Chicken">Chicken</option>
                  <option value="Vegetarian">Vegetarian</option>
                </select>
              </div>
              <div class="col-md-12">
                <label>Notes</label>
                <textarea name="message" class="form-control form-opacity"></textarea>
              </div>
              <div class="col-md-12">
                <input type="submit" class="btn submit" value="Send">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
  <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.4" data-image="{{asset('assets/images/wedding/mark_liz/10503209034_51e5fc8878_k.jpg')}}">
    <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
    <div class="container text-center">
      <p><img src="{{asset('assets/images/animations/thnyou_wh.gif')}}" alt="" height="200" data-bottom-top="@src:{{asset('assets/images/animations/thnyou_wh.gif')}}"></p>
    </div>
  </section>
</section>
@endsection
