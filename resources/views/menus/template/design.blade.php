<div class="gla_slider gla_image_bck gla_wht_txt gla_fixed"  data-image="~*01*~" data-stellar-background-ratio="0.8">
   <div class="gla_over" data-color="#9abab6" data-opacity="0.2"></div>
   <div class="container">
     <div class="gla_slide_txt gla_slide_center_bottom text-center">
       <div class="gla_slide_midtitle">~*02*~</div>
       <div class="gla_slide_subtitle">~*03*~</div>
     </div>
   </div>
   <a class="gla_scroll_down gla_go" href="#gla_content">
   <b></b>
   Scroll
   </a>
 </div>
 <section id="gla_content" class="gla_content">
   <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.4" data-image="~*04*~">
     <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
     <div class="container text-left">
       <div class="row">
         <div class="col-md-6 col-sm-12">
           <h2>~*05*~</h2>
           <h3 class="gla_subtitle">~*06*~</h3>
           <p>~*07*~</p>
         </div>
       </div>
     </div>
   </section>
   <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.4" data-image="~*08*~">
     <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
     <div class="container text-right">
       <p><img src="~*09*~" data-bottom-top="@src:~*09*~" height="150" alt=""></p>
       <h2>~*10*~</h2>
       <h3 class="gla_subtitle">~*11*~</h3>
       <div class="gla_countdown" data-year="~*12*~" data-month="~*13*~" data-day="~*14*~"></div>
     </div>
   </section>
   <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.4" data-image="~*15*~">
     <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
     <div class="container text-left">
       <p><img src="~*16*~" data-bottom-top="@src:~*16*~" height="150" alt=""></p>
       <h3>~*17*~</h3>
       <p class="gla_subtitle">~*18*~</p>
     </div>
   </section>
   <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.4" data-image="~*19*~">
     <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
     <div class="container text-center">
       <p><img src="~*20*~" alt="" height="200" data-bottom-top="@src:~*20*~"></p>
     </div>
   </section>
 </section>
