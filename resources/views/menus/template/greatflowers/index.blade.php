@extends('layouts.default')

@section('title','Great Flowers')

@section('music')
<div class="gla_music_icon">
    <i class="ti ti-music"></i>
</div>
<div class="gla_music_icon_cont">
    <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/47701713&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" allow="autoplay"></iframe>
</div>
@endsection

@section('content')
<div class="gla_slider gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.2" data-image="{{asset('assets/images/wedding/ian_kelsey/14629241938_afd1c2c19f_k.jpg')}}">
    <div class="gla_slider_flower">
         <div class="gla_slider_flower_c1 gla_slider_flower_ii gla_slider_flower_item" data-start="top:0px; left:0px" data-200-start="top:-300px; left:-300px"></div>
        <div class="gla_slider_flower_c2 gla_slider_flower_ii gla_slider_flower_item" data-start="top:0px; right:0px" data-200-start="top:-300px; right:-300px"></div>
        <div class="gla_slider_flower_c3 gla_slider_flower_ii gla_slider_flower_item" data-start="bottom:0px; right:0px" data-200-start="bottom:-300px; right:-300px"></div>
        <div class="gla_slider_flower_c4 gla_slider_flower_ii gla_slider_flower_item" data-start="bottom:0px; left:0px" data-200-start="bottom:-300px; left:-300px"></div>
        <div class="gla_slider_flower_c5 gla_slider_flower_ii gla_slider_flower_item" data-start="top:0px; left:50%" data-200-start="top:-300px; left:50%"></div>
        <div class="gla_slider_flower_c6 gla_slider_flower_ii gla_slider_flower_item" data-start="bottom:0px; left:50%" data-200-start="bottom:-300px; left:50%"></div>
    </div>
    <div class="gla_over" data-color="#000" data-opacity="0.2"></div>
    <div class="container">
        <div class="gla_slide_txt gla_slide_center_middle text-center">
            <p><img src="{{asset('assets/images/animations/ourwedding_wh.gif')}}" data-top-bottom="@src:{{asset('assets/images/animations/ourwedding_wh.gif')}}" alt="" height="150"></p>
        </div>
    </div>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section gla_lg_padding gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.2" data-image="{{asset('assets/images/wedding/ian_kelsey/14814210441_d5f33a00f1_k.jpg')}}">
         <div class="gla_slider_flower" data-bottom-top="@class:gla_slider_flower active" data--200-bottom="@class:gla_slider_flower no_active">
            <div class="gla_slider_flower_c1 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c2 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c3 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c4 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c5 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c6 gla_slider_flower_ii gla_slider_flower_item"></div>
        </div>
        <div class="gla_over" data-color="#000" data-opacity="0.3"></div>
        <div class="container text-center">
            <p><img src="{{asset('assets/images/animations/savethedate_wh.gif')}}" data-bottom-top="@src:{{asset('assets/images/animations/savethedate_wh.gif')}}" height="150" alt=""></p>
            <h2>{{$date['date']}}</h2>
            <h3 class="gla_subtitle">St. Thomas's Church,<br>Bristol, U.K.</h3>
            <div class="gla_countdown" data-year="{{$date['year']}}" data-month="{{$date['month']}}" data-day="{{$date['day']}}"></div>
        </div>
    </section>

    <section class="gla_section gla_lg_padding gla_image_bck gla_fixed" data-stellar-background-ratio="0.2" data-image="{{asset('assets/images/wedding/ian_kelsey/14815177364_46f0b9d71e_k.jpg')}}">
        <div class="gla_slider_flower" data-bottom-top="@class:gla_slider_flower active" data--200-bottom="@class:gla_slider_flower no_active">
             <div class="gla_slider_flower_c1 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c2 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c3 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c4 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c5 gla_slider_flower_ii gla_slider_flower_item"></div>
            <div class="gla_slider_flower_c6 gla_slider_flower_ii gla_slider_flower_item"></div>
        </div>
        <div class="gla_over" data-color="#fbe9df" data-opacity="0.5"></div>
        <div class="container text-center">
            <p><img src="{{asset('assets/images/animations/just_st.gif')}}" data-bottom-top="@src:{{asset('assets/images/animations/just.gif')}}" height="150" alt=""></p>
            <div class="gla_slide_midtitle">See you at the<br>wedding!</div>
            <p><a href="#" class="btn">RSVP</a></p>
        </div>
    </section>
</section>
@endsection
