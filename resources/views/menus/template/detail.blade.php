@extends('layouts.default')
@section('title','Simple Flowers')
@section('music')
<div class="gla_music_icon">
   <i class="ti ti-music"></i>
</div>
<div class="gla_music_icon_cont">
   <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/108238095&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" allow="autoplay"></iframe>
</div>
<div class="gla_floating_container">
  @if(Auth::check())
  <i class="ti ti-pencil" id="edit_event"></i>
  @else
  <a href="{{url('login')}}">
    <i class="ti ti-pencil" style="color:white;"></i>
  </a>
  @endif
</div>
@endsection

@section('content')
{!! $template_design->rendered !!}

<div class="modal fade" id="event_template" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
   <div class="modal-content">
     <form class="form-horizontal" id="form_template" name="form_template" method="POST" enctype="multipart/form-data" action="{{url('p/'.$link_url)}}">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
         <h4 class="modal-title" id="myModalLabel">Edit Template</h4>
       </div>
       <div class="modal-body">
        @csrf
       <div class="panel-body">
         <div class="col-md-12">
           @if (isset($template_design))
            <input type="hidden" name="eid" value="{{$template_design->eid}}">
           @endif
           @if (isset($previews))
             <div class="form-group row">
               <div class="col-md-12">
                 <!-- <label>Event's Title* <small class="form-text text-muted"> Ex. The Wedding Of Kevin & Alexandra</small></label> -->
                 <label>Event's Title</label>
                 <input type="text" name="event_title" class="form-control form-opacity" placeholder="Ex. The Wedding Of Kevin & Alexandra" value="{{$previews->event->nama}}">
               </div>
             </div>
             @foreach ($previews->event->event_design->text as $td)
               @if($td->template_design_detail->types == 'text' || $td->template_design_detail->types == 'date')
               <div class="form-group row">
                 <div class="col-md-12">
                   <label>{{$td->template_design_detail->name}}</label>
                   @if($td->template_design_detail->types == 'text')
                    <input type="text" name="template[text][{{$td->template_design_detail_id}}]" class="form-control form-opacity" placeholder="{{$td->template_design_detail->example}}" value="{{$td->value}}">
                   @elseif ($td->template_design_detail->types == 'date')
                    <input type="date" name="template[text][{{$td->template_design_detail_id}}]" class="form-control form-opacity" value="{{$td->value}}">
                   @endif
                 </div>
               </div>
               @endif
             @endforeach
             @foreach ($previews->event->event_design->file as $td)
               @if($td->template_design_detail->types == 'image')
               <div class="form-group row">
                 <div class="col-md-12">
                   <label>{{$td->template_design_detail->name}}</label>
                   @if($td->template_design_detail->types == 'image')
                    <input type="file" name="template[file][{{$td->template_design_detail_id}}]" class="dropify" data-default-file="{{$td->value}}" />
                   @endif
                 </div>
               </div>
               @endif
             @endforeach
           @else
             <div class="form-group row">
               <div class="col-md-12">
                 <label>Event's Title</label>
                 <input type="text" name="event_title" class="form-control form-opacity" placeholder="Ex. The Wedding Of Kevin & Alexandra">
               </div>
             </div>
             @foreach ($template_design->template_design_details as $td)
               @if($td->types == 'text' || $td->types == 'date')
               <div class="form-group row">
                 <div class="col-md-12">
                   <label>{{$td->name}}</label>
                   @if($td->types == 'text')
                    <input type="text" name="template[text][{{$td->id}}]" class="form-control form-opacity" placeholder="{{$td->example}}">
                   @elseif ($td->types == 'date')
                    <input type="date" name="template[text][{{$td->id}}]" class="form-control form-opacity">
                   @endif
                 </div>
               </div>
               @endif
             @endforeach
             @foreach ($template_design->template_design_details as $td)
               @if($td->types == 'image')
               <div class="form-group row">
                 <div class="col-md-12">
                   <label>{{$td->name}}</label>
                   @if($td->types == 'image')
                    <input type="file" name="template[file][{{$td->id}}]" class="dropify" data-default-file="{{$td->html}}" />
                   @endif
                 </div>
               </div>
               @endif
             @endforeach
           @endif
           <input type="hidden" name="event_id" value="{{$template_design->id}}">
           <input type="hidden" name="event_name" value="{{$template_design->nama}}">
         </div>
        </div>
       </div>
       <div class="modal-footer">
         <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
         <button type="submit" name="preview" class="btn btn-warning">Preview</button>
         <button type="submit" name="save" class="btn btn-info">Save</button>
       </div>
     </form>
   </div>
 </div>
</div>
@endsection

@push('css')
<link href="{{asset('assets/lib/dropify/css/dropify.min.css')}}" rel="stylesheet" type="text/css" />
<style>
.gla_floating_container{
  position: fixed;
  color: rgb(238, 238, 238);
  bottom: 45px;
  left: 50px;
  z-index: 2;
  font-size: 24px;
  cursor: pointer;
  transition: all 0.3s ease 0s;
  border-radius: 50px;
  padding: 20px;
  background :  rgb(28, 136, 224);
  border: 2px solid rgb(28, 136, 224);
}
</style>
@endpush
@push('script')
<script src="{{asset('assets/lib/dropify/js/dropify.min.js')}}" type="text/javascript"></script>
<script>
$(function() {
  thisform.d_init();
}), thisform = {
  d_init : function() {
    $('.dropify').dropify();
  },
};

$(function(){
 $(document).on('click','#edit_event',function(e){
   e.preventDefault();
   $("#event_template").modal('show');
 });
});
</script>
@endpush
