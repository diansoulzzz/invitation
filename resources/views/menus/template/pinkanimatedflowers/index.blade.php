@extends('layouts.default')

@section('title','Pink Animated Flowers')

@section('music')
<div class="gla_music_icon">
    <i class="ti ti-music"></i>
</div>
<div class="gla_music_icon_cont">
    <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/66757544&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" allow="autoplay"></iframe>
</div>
@endsection

@section('content')
<div class="gla_slider gla_image_bck  gla_wht_txt gla_fixed"  data-image="{{asset('assets/images/wedding/lauren_affonso/14400933225_3b844816c3_k.jpg')}}" data-stellar-background-ratio="0.8">
    <div class="gla_over" data-color="#1e1d2d" data-opacity="0.2"></div>
    <div class="container">
        <div class="gla_slide_txt gla_slide_center_middle text-center">
             <div class="gla_flower gla_flower2">
                <div class="gla_flower2_name_l">Lauren <b>Save The Date</b></div>
                <div class="gla_flower2_name_r">Affonso <b>{{$date['date']}}</b></div>
                <img src="{{asset('assets/images/wedding/lauren_affonso/14214412498_73d4296c75_k_sm.jpg')}}" alt="">
            </div>
        </div>
    </div>
    <a class="gla_scroll_down gla_go" href="#gla_content">
        <b></b>
        Scroll
    </a>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section gla_image_bck" data-color="#fafafd">
        <div class="container text-center">
            <p><img src="{{asset('assets/images/animations/flower5.gif')}}" data-bottom-top="@src:{{asset('assets/images/animations/flower5.gif')}}; opacity:1" class="gla_animated_flower" height="150" alt=""></p>
            <h2>Our Story</h2>
            <h3 class="gla_subtitle">The Fourth of July</h3>
            <p>My fiancé proposed on the Fourth of July. My mother asked us to go to the backyard to get some chairs and he took me by the shed where we could see all of the fireworks. He kissed me, then he took the ring box out of his pocket and asked me to be his wife. He was shaking a little. The proposal was a little silly but perfect, just like him." — Jeska Cords</p>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.8" data-image="{{asset('assets/images/wedding/lauren_affonso/14214309838_1a8f80c65a_k.jpg')}}">
        <div class="gla_over" data-color="#1e1d2d" data-opacity="0.4"></div>
        <div class="container text-center">
            <p><img src="{{asset('assets/images/animations/savethedate_wh.gif')}}" data-bottom-top="@src:{{asset('assets/images/animations/savethedate_wh.gif')}}" height="150" alt=""></p>
            <h2>{{$date['date']}}</h2>
            <h3 class="gla_subtitle">St. Thomas's Church, <br>Bristol, U.K.</h3>
            <div class="gla_countdown" data-year="{{$date['year']}}" data-month="{{$date['month']}}" data-day="{{$date['day']}}"></div>
        </div>
    </section>

    <section class="gla_section">
        <div class="container text-center">
            <p><img src="{{asset('assets/images/animations/flower6.gif')}}" data-bottom-top="@src:{{asset('assets/images/animations/flower6.gif')}}; opacity:1" class="gla_animated_flower" height="110" alt=""></p>
            <h2>Wedding Details</h2>
            <h3 class="gla_subtitle">When & Where</h3>
            <p>Our ceremony and reception will be held at the Liberty House. Located on the Hudson River, it has a beautiful, unobstructed view of the World Trade One building and a convenient ferry that runs between it and Manhattan.</p>
            <div class="row text-center">
                <div class="col-md-4 gla_round_block">
                    <div class="gla_round_im gla_image_bck" data-image="{{asset('assets/images/wedding/lauren_affonso/600x600/14214580180_4dbce3ed37_k.jpg')}}"></div>
                    <h3>The Reception</h3>
                    <p>4:00 PM – 5:30 PM<br>
                    St. Thomas's Church,<br>
                    Bristol<br>
                    U.K.<br>
                    +1 777-123-4567</p>
                    <a href="#" class="btn">View Map</a>
                </div>
                <div class="col-md-4 gla_round_block">
                    <div class="gla_round_im gla_image_bck" data-image="{{asset('assets/images/wedding/lauren_affonso/600x600/14214514238_c7a691994d_k.jpg')}}"></div>
                    <h3>The Ceremony</h3>
                    <p>4:00 PM – 5:30 PM<br>
                    St. Thomas's Church,<br>
                    Bristol<br>
                    U.K.<br>
                    +1 777-123-4567</p>
                    <a href="#" class="btn">View Map</a>
                </div>
                <div class="col-md-4 gla_round_block">
                    <div class="gla_round_im gla_image_bck" data-image="{{asset('assets/images/wedding/lauren_affonso/600x600/14214610370_ccda70fba1_k.jpg')}}"></div>
                    <h3>The Afterparty</h3>
                    <p>4:00 PM – 5:30 PM<br>
                    St. Thomas's Church,<br>
                    Bristol<br>
                    U.K.<br>
                    +1 777-123-4567</p>
                    <a href="#" class="btn">View Map</a>
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.2" data-image="{{asset('assets/images/wedding/lauren_affonso/14378154996_a9923cfa6f_k.jpg')}}">
        <div class="gla_over" data-color="#1e1d2d" data-opacity="0.4"></div>
        <div class="container text-center">
            <p><img src="{{asset('assets/images/animations/rsvp_wh.gif')}}" data-bottom-top="@src:{{asset('assets/images/animations/rsvp_wh.gif')}}" height="200" alt=""></p>
            <div class="row">
                <div class="col-md-8 col-md-push-2">
                    <form action="https://formspree.io/your@mail.com" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Your name*</label>
                                <input type="text" name="name" class="form-control form-opacity">
                            </div>
                            <div class="col-md-6">
                                <label>Your e-mail*</label>
                                <input type="text" name="email" class="form-control form-opacity">
                            </div>
                            <div class="col-md-6">
                                <label>Will you attend?</label>
                                <input type="radio" name="attend" value="Yes, I will be there">
                                <span>Yes, I will be there</span><br>
                                <input type="radio" name="attend" value="Sorry, I can't come">
                                <span>Sorry, I can't come</span>
                            </div>
                            <div class="col-md-6">
                                <label>Meal preference</label>
                                <select name="meal" class="form-control form-opacity">
                                    <option value="I eat anything">I eat anything</option>
                                    <option value="Beef">Beef</option>
                                    <option value="Chicken">Chicken</option>
                                    <option value="Vegetarian">Vegetarian</option>
                                </select>
                            </div>
                            <div class="col-md-12">
                                <label>Notes</label>
                                <textarea name="message" class="form-control form-opacity"></textarea>
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="btn submit" value="Send">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck">
        <div class="container text-center">
            <p><img src="{{asset('assets/images/animations/flower7.gif')}}" data-bottom-top="@src:{{asset('assets/images/animations/flower7.gif')}}; opacity:1" class="gla_animated_flower" height="110" alt=""></p>
            <h2>The Day They Got Engaged</h2>
            <p>Andy and Jeska met in university in the Graphic Design program. They both remember each other from orientation, but it wasn’t love at first sight, that’s for sure. Andy remembers Jeska as a ‘snooty art bitch (having been in the visual arts program at the time), and she remembers Andy being an ‘arrogant computer nerd’, boasting his knowledge of Macs over the other students.</p>
            <div class="button-group filter-button-group">
                <a data-filter="*">Show All</a>
                <a data-filter=".engagement">Engagement</a>
                <a data-filter=".ceremony">Ceremony</a>
            </div>
            <div class="gla_portfolio_no_padding grid">
                <div class="col-sm-4 gla_anim_box grid-item engagement">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214345610_af0e495ff2_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214345610_af0e495ff2_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item engagement">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214347748_db9bfb6322_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214347748_db9bfb6322_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item ceremony">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214358898_8eed03f1d6_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214358898_8eed03f1d6_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item engagement">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214387908_8ad395d6d0_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214387908_8ad395d6d0_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item ceremony">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214397640_d49d0e765d_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214397640_d49d0e765d_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item engagement">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214429749_ad31345b6d_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214429749_ad31345b6d_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item ceremony">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214471159_96ea39117f_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214471159_96ea39117f_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item engagement">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214473720_7f2af51372_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214473720_7f2af51372_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item ceremony">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214610370_ccda70fba1_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214610370_ccda70fba1_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item ceremony">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14377952686_252620ebb7_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14377952686_252620ebb7_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item engagement">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14397823931_a8e8cf3fa7_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14397823931_a8e8cf3fa7_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item ceremony">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14399848562_abaad23e7a_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14399848562_abaad23e7a_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item engagement">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14399908894_b8858a5476_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14399908894_b8858a5476_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item engagement">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14401393205_af78b0dd2f_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14401393205_af78b0dd2f_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item ceremony">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14401390305_9a369d1e4d_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14401390305_9a369d1e4d_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item engagement">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14401083075_5bf66d3082_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14401083075_5bf66d3082_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item engagement">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14399773152_759f6d4057_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14399773152_759f6d4057_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item ceremony">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14400933225_3b844816c3_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14400933225_3b844816c3_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
             </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.8" data-image="{{asset('assets/images/wedding/lauren_affonso/14214387908_8ad395d6d0_k.jpg')}}">
        <div class="gla_over" data-color="#1e1d2d" data-opacity="0.4"></div>
        <div class="container text-center">
            <h2>Registry</h2>
            <p>We’re lucky enough to have nearly everything we need for our home already. And since neither of us has ever been outside of North America, we want our honeymoon to be extra special! If you want to help make it unforgettable, you can contribute using the link to the right. If you would like to give us something to update our home, we’ve compiled a short registry as well.</p>
        </div>
    </section>

    <section class="gla_section gla_image_bck" data-color="#fff">
        <div class="container text-center">
            <h2>The Wedding Party</h2>
            <div class="button-group filter-button-group">
                <a data-filter="*">Show All</a>
                <a data-filter=".groomsmen">Groomsmen</a>
                <a data-filter=".bridesmaids">Bridesmaids</a>
            </div>
            <div class="gla_portfolio_no_padding grid">
                <div class="col-sm-4 gla_anim_box grid-item bridesmaids">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14421186043_a817e20aa5_k-ryan-moreno.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14421186043_a817e20aa5_k.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            Andrew & Joanna
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item bridesmaids">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14401255335_785c7e78a4_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14401255335_785c7e78a4_k.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            Stewart & Ann
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item bridesmaids">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214456709_b71037d1ed_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214456709_b71037d1ed_k.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            Mark & Jenny
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item broombridesmaidssmen">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14421584103_4393f08b6c_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14421584103_4393f08b6c_k.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            Izzy & Katy
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item groomsmen">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214839668_c094232d97_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214839668_c094232d97_k.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            Bob
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item groomsmen">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214805748_41d9615733_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214805748_41d9615733_k.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            Mark
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item groomsmen">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214429749_ad31345b6d_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214429749_ad31345b6d_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item groomsmen">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214473720_7f2af51372_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214473720_7f2af51372_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item groomsmen">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14401083075_5bf66d3082_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14401083075_5bf66d3082_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item groomsmen">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14401497035_5c4a8f0b0a_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14401497035_5c4a8f0b0a_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item groomsmen">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14399707332_ca2503be41_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14399707332_ca2503be41_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item groomsmen">
                    <div class="gla_shop_item">
                        <a href="{{asset('assets/images/wedding/lauren_affonso/14214662727_11e5c719f7_k.jpg')}}" class="lightbox">
                            <img src="{{asset('assets/images/wedding/lauren_affonso/600x600/14214662727_11e5c719f7_k.jpg')}}" alt="">
                        </a>
                    </div>
                </div>
             </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_fixed gla_wht_txt" data-stellar-background-ratio="0.8" data-image="{{asset('assets/images/wedding/lauren_affonso/14377952686_252620ebb7_k.jpg')}}">
        <div class="gla_over" data-color="#1e1d2d" data-opacity="0.4"></div>
        <div class="container text-center">
            <p><img src="{{asset('assets/images/animations/thnyou_wh.gif')}}" alt="" height="200" data-bottom-top="@src:{{asset('assets/images/animations/thnyou_wh.gif')}}"></p>
        </div>
    </section>
</section>
@endsection
