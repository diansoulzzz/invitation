@extends('layouts.default')
@section('title','Simple Flowers')
@section('music')
<div class="gla_music_icon">
   <i class="ti ti-music"></i>
</div>
<div class="gla_music_icon_cont">
   <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/108238095&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" allow="autoplay"></iframe>
</div>
@endsection
@section('content')
  {!! $template_design->html !!}
@endsection
