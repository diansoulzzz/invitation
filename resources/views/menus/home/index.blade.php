@extends('layouts.default')

@section('title','Home')

@section('content')<section id="gla_content" class="gla_content">
  <section class="gla_section gla_image_bck" data-color="rgba(235, 202, 29, 0.9)">
    <div class="container text-center">
      <h3>3Vite The Best Digital Invitation Card</h3>
      <h4 class="gla_subtitle">Lets find your next experience</h4>
      <div class="row">
        <form method="get" action="{{url('search')}}">
          <div class="col-md-6 col-md-offset-2">
            <input type="text" name="q" class="form-control" placeholder="Search Invitation Designs">
          </div>
          <div class="col-md-2">
            <input type="submit" class="btn btn-auto" value="Search">
          </div>
        </form>
      </div>
    </div>
  </section>
  @if(false)
  <section class="gla_section">
    <div class="container text-center">
      <h4>Latest Event</h4>
      <!-- <h3 class="gla_subtitle">Browse our blog for tips and inspiration. From Real Weddings to the latest trends, let us inspire you!</h3> -->
      <div class="gla_icon_boxes row text-left">
        <div class="col-md-4 col-sm-6">
          <a href="#" class="gla_news_block">
            <span class="gla_news_img">
            <span class="gla_over" data-image="images/wedding_m/600x600/tom-pumford-226386.jpg"></span>
            </span>
            <span class="gla_news_title">James & Nicola's Elegant Woodland Barn Weddin...</span>
            <span class="gla_news_author">Real Weddings | 10 December</span>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa similique porro officiis nobis nulla quidem nihil iste veniam ut sit, maiores.</p>
          </a>
        </div>
        <div class="col-md-4 col-sm-6">
          <a href="#" class="gla_news_block">
            <span class="gla_news_img">
            <span class="gla_over" data-image="images/wedding_m/600x600/tamara-menzi-224980.jpg"></span>
            </span>
            <span class="gla_news_title">6 Charitable Ways to Give Back on Your Weddin...</span>
            <span class="gla_news_author">Things we love | 10 December</span>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa similique porro officiis nobis nulla quidem nihil iste veniam ut sit, maiores.</p>
          </a>
        </div>
        <div class="col-md-4 col-sm-6">
          <a href="#" class="gla_news_block">
            <span class="gla_news_img">
            <span class="gla_over" data-image="images/wedding_m/600x600/tom-pumford-226929.jpg"></span>
            </span>
            <span class="gla_news_title">Natalia & Xavier's Parisian Engagement Shoot</span>
            <span class="gla_news_author">Real Weddings | 10 December</span>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa similique porro officiis nobis nulla quidem nihil iste veniam ut sit, maiores.</p>
          </a>
        </div>
      </div>
      <p><a href="#" class="btn">View More Inspiration</a></p>
    </div>
  </section>
  @endif

  <section class="gla_section gla_section_sml_padding">
    <div class="container">
      <h4 class="text-left">Trending Template</h4>
      <div class="gla_icon_boxes gla_partners row">
        @foreach ($gbl_template_trending as $key => $template)
        <div class="gla_partner_box">
          <a href="{{url('p/'.$template->link_url)}}">
            <img src="{{$template->imgpreview}}" alt="{{$template->nama}}">
            <!-- <img src="{{Storage::disk('public')->url($template->imgpreview)}}" alt="{{$template->nama}}"> -->
          </a>
        </div>
        @endforeach
      </div>
    </div>
  </section>
</section>
@endsection
