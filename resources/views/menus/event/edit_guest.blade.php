@extends('layouts.default')

@section('title','Guest')

@section('content')
<div class="gla_page_title gla_image_bck gla_wht_txt" data-color="#282828">
   <div class="container text-left">
      <div class="row">
         <div class="col-md-8">
            <h1 class="gla_h1_title">Edit Guest</h1>
            <h3>This page may you to edit your guest </h3>
         </div>
      </div>
   </div>
</div>
<form class="form-horizontal" method="POST" id="form_guest" action="{{url()->current()}}" enctype="multipart/form-data">
@csrf
@if ($errors->any())
   <div class="alert alert-danger">
     <span aria-hidden="true" class="alert-icon icon_blocked"></span><strong>Error! </strong>There are some problems.
       <ul>
           @foreach ($errors->all() as $error)
               <li>{{ $error }}</li>
           @endforeach
       </ul>
   </div>
@endif
<section id="gla_content" class="gla_content">
  <section class="gla_section">
     <div class="container">
       @if(session('status'))
       <div class="alert {{session('status')['alert']}}">
         <span aria-hidden="true" class="alert-icon icon_blocked"></span><strong>{{session('status')['status']}} </strong>{{session('status')['message']}}
       </div>
       @endif
       <div class="form-group row">
           <div class="col-md-6">
               <label>Guest's Name* <small class="form-text text-muted"> Ex. Daivalentineno Janitra Salim</small></label>
                 <input type="text" name="guest_name" id="guest_name" class="form-control form-opacity" data-parsley-maxlength="300" data-parsley-minlength="3" value="{{$guest->nama}}">
           </div>
           <div class="col-md-6">
               <label>Address* <small class="form-text text-muted"> Ex. North Citraland Block D6 Number 6</small></label>
                 <input type="text" name="address" id="address" class="form-control form-opacity" data-parsley-maxlength="300" data-parsley-minlength="3" value="{{$guest->alamat}}">
           </div>
       </div>
       <div class="form-group row">
           <div class="col-md-6">
               <label>Phone* <small class="form-text text-muted"> Ex. 089695969367</small></label>
                 <input type="text" name="phone" id="phone" onkeypress="return isNumberKey(event)" class="form-control form-opacity" data-parsley-maxlength="300" data-parsley-minlength="3" value="{{$guest->telepon}}">
           </div>
           <div class="col-md-6">
               <label>Email* <small class="form-text text-muted"> Ex. daivalentinenojs@gmail.com</small></label>
                 <input type="email" name="email" id="email" class="form-control form-opacity" data-parsley-maxlength="300" data-parsley-minlength="3" value="{{$guest->email}}">
           </div>
       </div>
       <div class="form-group row">
           <div class="col-md-6">
               <label>Whats App* <small class="form-text text-muted"> Ex. 089695969367</small></label>
                 <input type="text" name="whatsapp" id="whatsapp" onkeypress="return isNumberKey(event)" class="form-control form-opacity" data-parsley-maxlength="300" data-parsley-minlength="3" value="{{$guest->whatsapp}}">
           </div>
           <div class="col-md-6">
               <label>Line* <small class="form-text text-muted"> Ex. 089695969367</small></label>
                 <input type="text" name="line" id="line" class="form-control form-opacity" data-parsley-maxlength="300" data-parsley-minlength="3" value="{{$guest->line}}">
           </div>
       </div>
       <div class="form-group row">
         <div class="col-md-6">
           <label>Category* <small class="form-text text-muted"> Ex. Family</small></label>
           <select class="form-control select col-md-6" name="id_category" id="id_category" data-live-search="true">
             @if($guest->jenis == 1)
             <option selected value="1">Family</option>
             @else
             <option value="1">Family</option>
             @endif

             @if($guest->jenis == 2)
             <option selected value="2">Couple</option>
             @else
             <option value="2">Couple</option>
             @endif

             @if($guest->jenis == 3)
             <option selected value="3">Single</option>
             @else
             <option value="3">Single</option>
             @endif
           </select>
         </div>
         <div class="col-md-6">
               <input type="hidden" name="id_event" id="id_event" class="form-control form-opacity" value="{{$did}}">
         </div>
       </div>
       <div class="col-md-12">
         <div class="form-group">
               <div class="col-md-5">
               </div>
               <div class="col-md-7"><br>
                  <button type="button" id="addRow" class="btn btn-warning" name="addRow">Update Guest</button><br><br><br>
               </div>
         </div>
       </div>
     </div>
  </section>
</section>
</form>
@endsection

@push('css')
<link href="{{asset('assets/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('assets/css/jquery.dataTables.min.css')}}" rel="stylesheet">
@endpush

@push('script')
<script src="{{asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script>
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if ((charCode > 31 && (charCode < 48 || charCode > 57)))
        return false;
    return true;
}

$('#addRow').on( 'click', function () {
  $('#form_guest').submit();
});
</script>
@endpush
