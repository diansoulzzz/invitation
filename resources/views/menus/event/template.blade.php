@extends('layouts.default')

@section('title','Create Event Template')

@section('content')
<div class="gla_page_title gla_image_bck gla_wht_txt" data-color="#282828">
   <div class="container text-left">
      <div class="row">
         <div class="col-md-12">
            <h1 class="gla_h1_title">Create Event Template</h1>
            <h3>Please choose one template which you want to use</h3>
         </div>
      </div>
   </div>
</div>
<section id="gla_content" class="gla_content">
  <section class="gla_section">
     <div class="container">
       <div class="row">
         <div class="col-md-12 gla_main_sidebar">
           <form id="form-validated" action="{{url('event/')}}" method="GET">
               @if ($errors->any())
                  <div class="alert alert-danger">
                    <span aria-hidden="true" class="alert-icon icon_blocked"></span><strong>Error! </strong>There are some problems.
                      <ul>
                          @foreach ($errors->all() as $error)
                              <li>{{ $error }}</li>
                          @endforeach
                      </ul>
                  </div>
              @endif
              <div class="form-group row">
                  <div class="col-md-6">
                      <label>Template* <small class="form-text text-muted"> Ex. Parallax</small></label>
                      <select name="design" id="design" class="form-control form-opacity" required>
                          <?php $last_category_name = ''; ?>
                          @foreach ($category as $cat)
                              @if($last_category_name == '' || $last_category_name != $cat['category_name'])
                                <?php $last_category_name = $cat['category_name']; ?>
                                <optgroup label="{{$cat['category_name']}}">
                              @endif
                                  <option value="{{$cat['link_url']}}">{{$cat['nama']}}</option>
                              @if($last_category_name == '' || $last_category_name != $cat['category_name'])
                                </optgroup>
                              @endif
                          @endforeach
                      </select>
                  </div>
              </div>
              <div class="form-group row">
                   <div class="col-md-12 text-center">
                       <input type="submit" class="btn submit" value="Continue">
                   </div>
              </div>
           </form>
         </div>
       </div>
     </div>
  </section>
</section>
@endsection

@push('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style>
  select.form-control {
    border-radius: 25px !important;
  }

  input.parsley-success,
  select.parsley-success,
  textarea.parsley-success {
    /* color: green; */
    background-color: #DFF0D8;
    border: 1px solid #D6E9C6;
  }

  input.parsley-error,
  select.parsley-error,
  textarea.parsley-error {
    /* color: red; */
    background-color: #F2DEDE;
    border: 1px solid #EED3D7;
  }

  .parsley-errors-list {
    /* margin: 0px 0 3px; */
    padding: 0;
    color: red;
    font-weight: bold;
    list-style-type: none;
    font-size: 0.9em;
    line-height: 0.9em;
    opacity: 0;

    transition: all .3s ease-in;
    -o-transition: all .3s ease-in;
    -moz-transition: all .3s ease-in;
    -webkit-transition: all .3s ease-in;
  }

  .parsley-errors-list.filled {
    opacity: 1;
  }
</style>
@endpush

@push('script')
<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/js/parsley.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script>
$(function () {
  thisform.init();
}), thisform = {
  init : function () {
    $('#form-validated').parsley();
    $('.form-masked').inputmask( {'autoUnmask' : true});
  },
}
</script>
@endpush
