@extends('layouts.default')
@section('title','Simple Flowers')
@section('music')
<div class="gla_music_icon">
   <i class="ti ti-music"></i>
</div>
<div class="gla_music_icon_cont">
   <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/108238095&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" allow="autoplay"></iframe>
</div>

<div class="gla_floating_container">
   <i class="ti ti-pencil"  id="edit_event"></i>
</div>

@endsection
@section('content')
<!-- <div class="gla_page_title gla_image_bck gla_wht_txt" data-color="#282828">
   <div class="container text-left">
     <div class="form-group row">
          <div class="col-md-12">
              <input type="button" class="btn submit" id="edit_event" value="Edit">
          </div>
     </div>
   </div>
</div> -->
{!! $template_design->rendered !!}

<div class="modal fade" id="event_template" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
   <div class="modal-dialog">
       <div class="modal-content">
           <div class="modal-header">
               <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Tutup</span></button>
               <h4 class="modal-title" id="myModalLabel">Edit Template</h4>
           </div>
           <div class="modal-body">
           <form class="form-horizontal" id="form_template" name="form_template" method="POST">
              @csrf
              <div class="modal-body">
                 <div class="panel-body">
                   <div class="col-md-12">
                     @if (isset($template_design_detail))
                     <input type="hidden" name="eid" value="{{$template_design_detail->eid}}">
                     @endif
                     <div class="form-group row">
                         <div class="col-md-12">
                             <label>Event's Title* <small class="form-text text-muted"> Ex. The Wedding Of Kevin & Alexandra</small></label>
                               <input type="text" name="event_title" class="form-control form-opacity" required data-parsley-maxlength="300" data-parsley-minlength="3" value="{{old('event_title',(isset($template_design_detail)?$template_design_detail->name:''))}}">
                         </div>
                     </div>
                     @foreach ($template_detail as $td)
                       @if($td['types'] == 'text' || $td['types'] == 'image' || $td['types'] == 'date')
                       <div class="form-group row">
                           <div class="col-md-12">
                               <label>{{$td['detail_name']}}* <small class="form-text text-muted"> Ex. {{$td['example']}}</small></label>
                               @if($td['types'] == 'text')
                                <input type="text" name="template[{{$td['id_detail']}}]" class="form-control form-opacity" required data-parsley-maxlength="300" data-parsley-minlength="3" value="{{old('$td[\'id_detail\']',(isset($template_design_detail)?$template_design_detail->name:''))}}">
                               @elseif ($td['types'] == 'image')
                               <input type="file" name="template[{{$td['id_detail']}}]" class="form-control form-opacity" required value="{{old('$td[\'id_detail\']',(isset($template_design_detail)?$template_design_detail->name:''))}}">
                               @elseif ($td['types'] == 'date')
                               <input type="date" name="template[{{$td['id_detail']}}]" class="form-control form-opacity" required value="{{old('$td[\'id_detail\']',(isset($template_design_detail)?$template_design_detail->name:''))}}">
                               @endif
                           </div>
                       </div>
                       @endif
                     @endforeach
                     <input type="hidden" name="event_id" value="{{$template_detail[0]['id']}}">
                     <input type="hidden" name="event_name" value="{{$template_detail[0]['nama']}}">
                     <div class="form-group" style="text-align:center;">
                            <input type="submit" id="btn_save" name="btn_save" value="Save" class="btn btn-success">
                     </div>
                   </div>
                  </div>
                </div>
              </form>
           </div>
           <div class="modal-footer">
               <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
           </div>
       </div>
   </div>
</div>
@endsection
@push('css')
<style>
.gla_floating_container{
    position: fixed;
    color: rgb(238, 238, 238);
    bottom: 45px;
    left: 50px;
    z-index: 2;
    font-size: 24px;
    cursor: pointer;
    transition: all 0.3s ease 0s;
    border-radius: 50px;
    padding: 20px;
    background :  rgb(28, 136, 224);
    border: 2px solid rgb(28, 136, 224);
}

</style>
@endpush
@push('script')
<script>
$(function(){
   $(document).on('click','#edit_event',function(e){
       e.preventDefault();
       $("#event_template").modal('show');
   });
});
</script>
@endpush
