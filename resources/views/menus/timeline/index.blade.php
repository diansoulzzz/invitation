@extends('layouts.default')

@section('title','Home')

@section('content')
<div class="gla_page_title gla_image_bck gla_wht_txt" data-color="#282828">
   <div class="container text-left">
      <div class="row">
         <div class="col-md-8">
            <h1 class="gla_h1_title">Our Time Line</h1>
            <h3>Today's Event</h3>
         </div>
      </div>
   </div>
</div>
<section id="gla_content" class="gla_content">
   <section class="gla_section">
      <div class="container">
         <div class="row">
            <div class="col-md-8 col-xs-12">
               @foreach($event as $ev)
               <div class="gla_post_item">
                  <div class="gla_post_img">
                     <img src="{{url('storage/app/public/'.$ev->background_home_1)}}" alt="">
                  </div>
                  <div class="gla_post_title">
                     <h3>
                       {{$ev->event_details->nama_acara}}
                     </h3>
                  </div>
                  <div class="gla_post_info">
                      {{date_format($ev->event_schedules[0]->jam_mulai,"d F Y")}}
                     <span class="slash-divider">/</span>
                     <span>
                       {{$ev->event_details->nama_pria}}
                     </span>
                     <span class="slash-divider">/</span>
                     <span>
                      {{$ev->event_details->nama_wanita}}
                     </span>
                  </div>
                  <p>
                    <span>
                      {{$ev->event_details->cerita_awal}}
                    </span>
                  </p>
                  <div class="gla_post_more clearfix">
                     <div class="pull-left">
                        <a href="" class="btn">READ MORE</a>
                     </div>
                     <div class="pull-right" >
                        <!-- <a href="" class="gla_post_more_link"><i class="ti ti-comment"></i><span>14</span></a>
                        <a href="" class="gla_post_more_link"><i class="ti ti-heart"></i><span>154</span></a>
                        <a href="" class="dropdown-toggle gla_post_more_link" data-toggle="dropdown" aria-expanded="false" >
                        <i class="ti ti-sharethis"></i>
                        </a>
                        <ul class="gla_post_more_social_menu dropdown-menu dropdown-menu-right" role="menu">
                           <li><a href=""><i class="ti ti-facebook"></i></a>
                           </li>
                           <li><a href=""><i class="ti ti-twitter"></i></a>
                           <li><a href=""><i class="ti ti-instagram"></i></a>
                        </ul> -->
                     </div>
                  </div>
               </div>
               @endforeach



            <nav class="gla_blog_pag">
                  <ul class="pagination">
                     {{ $event->links() }}
                  </ul>
               </nav>
            </div>
            <div class="col-md-3 col-md-push-1 hidden-xs hidden-sm">
               <div class="widget">
                  <h6 class="title">3Vite Management</h6>
                  <p>
                     CEO : Alexandra Sefanie <br>
                     COO : Daivalentineno Janitra Salim <br>
                     CTO : Dian Yulius <br>
                  </p>
               </div>
               <div class="widget">
                  <h6 class="title">Search Blog</h6>
                  <form>
                     <input class="form-control" type="text" placeholder="Enter Your Keywords" />
                  </form>
               </div>
               <div class="widget">
                  <h6 class="title">Event Categories</h6>
                  <ul class="list-unstyled">
                     <li>
                        Wedding Party
                     </li>
                     <li>
                        Birthday Party
                     </li>
                     <li>
                        Birthday Dinner
                     </li>
                  </ul>
               </div>
               <div class="widget">
                  <h6 class="title">Recent Posts</h6>
                  <ul class="list-unstyled recent-posts">
                    @foreach ($post as $p)
                      <li>
                         {{$p->event_details->nama_acara}}
                         <span class="date">{{date_format($p->event_schedules[0]->jam_mulai,"d F Y")}}</span>
                      </li>
                    @endforeach
                  </ul>
               </div>
               <div class="widget bg-secondary p24">
                  <h6 class="title">Subscribe Now</h6>
                  <p>
                     Subscribe to our newsletter.
                  </p>
                  <form>
                     <input type="text" class="form-control" name="email" placeholder="Email Address" />
                     <input type="submit" class="btn btn-default no-margin" value="Subscribe" />
                  </form>
               </div>
            </div>
         </div>
      </div>
   </section>
</section>
@endsection
