@extends('layouts.default')

@section('title','Shop')

@section('music')
<div class="gla_music_icon">
    <i class="ti ti-music"></i>
</div>
<div class="gla_music_icon_cont">
    <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/108238095&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" allow="autoplay"></iframe>
</div>
@endsection

@section('content')
<div class="gla_page_title gla_image_bck gla_wht_txt" data-color="#282828">
  <div class="container text-left">
    <div class="row">
      <div class="col-md-8">
        <h1 class="gla_h1_title">Shop</h1>
        <h3>Enjoy our accesories</h3>
      </div>
    </div>
  </div>
</div>
<section id="gla_content" class="gla_content">
    <section class="gla_section">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-xs-12">
                    <div class="gla_shop_header">
                        <div class="row">
                            <div class="col-md-6">
                                Showing 1–10 of 24 results
                            </div>
                            <div class="col-md-6 text-right">
                                <select name="type">
                                    <option value="" selected="selected">Sort By</option>
                                    <option value="">What's new</option>
                                    <option value="">Price high to low</option>
                                    <option value="">Price low to high</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 gla_anim_box">
                            <div class="gla_shop_item">
                                <span class="gla_shop_item_sale">Sale</span>
                                <span class="gla_shop_item_slider">
                                    <img src="{{asset('assets/images/wedding_m/600x600/alvin-mahmudov-181259.jpg')}}" alt="">
                                </span>
                                <a href="#" class="gla_shop_item_title">
                                    Vintage Floral
                                    <b><s>$120.36</s> $12.96</b>
                                </a>
                                <div class="gla_shop_item_links">
                                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 gla_anim_box">
                            <div class="gla_shop_item">
                                <span class="gla_shop_item_slider">
                                    <img src="{{asset('assets/images/wedding_m/600x600/anne-edgar-119373.jpg')}}" alt="">
                                </span>
                                <a href="#" class="gla_shop_item_title">
                                    Lost Ink
                                    <b>$13.16</b>
                                </a>
                                <div class="gla_shop_item_links">
                                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 gla_anim_box">
                            <div class="gla_shop_item">
                                <span class="gla_shop_item_slider">
                                    <img src="{{asset('assets/images/wedding_m/600x600/brooke-cagle-193346.jpg')}}" alt="">
                                </span>
                                <a href="#" class="gla_shop_item_title">
                                    New Look
                                    <b>$10.96</b>
                                </a>
                                <div class="gla_shop_item_links">
                                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 gla_anim_box">
                            <div class="gla_shop_item">
                                <span class="gla_shop_item_slider">
                                    <img src="{{asset('assets/images/wedding_m/600x600/brandon-morgan-17707.jpg')}}" alt="">
                                </span>

                                <a href="#" class="gla_shop_item_title">
                                    Full Midi
                                    <b>$8.28</b>
                                </a>
                                <div class="gla_shop_item_links">
                                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="row">
                        <div class="col-md-3 gla_anim_box">
                            <div class="gla_shop_item">
                                <span class="gla_shop_item_slider">
                                    <img src="{{asset('assets/images/wedding_m/600x600/brooke-cagle-193349.jpg')}}" alt="">
                                </span>
                                <a href="#" class="gla_shop_item_title">
                                    Vintage Floral
                                    <b><s>$120.36</s> $12.96</b>
                                </a>
                                <div class="gla_shop_item_links">
                                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 gla_anim_box">
                            <div class="gla_shop_item">
                                <span class="gla_shop_item_slider">
                                    <img src="{{asset('assets/images/wedding_m/600x600/caroline-veronez-131137.jpg')}}" alt="">
                                </span>
                                <a href="#" class="gla_shop_item_title">
                                    Lost Ink
                                    <b>$13.16</b>
                                </a>
                                <div class="gla_shop_item_links">
                                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 gla_anim_box">
                            <div class="gla_shop_item">
                                <span class="gla_shop_item_slider">
                                    <img src="{{asset('assets/images/wedding_m/600x600/emma-frances-logan-barker-200055.jpg')}}" alt="">
                                </span>
                                <a href="#" class="gla_shop_item_title">
                                    New Look
                                    <b>$10.96</b>
                                </a>
                                <div class="gla_shop_item_links">
                                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 gla_anim_box">
                            <div class="gla_shop_item">
                                <span class="gla_shop_item_slider">
                                    <img src="{{asset('assets/images/wedding_m/600x600/anete-lusina-147373.jpg')}}" alt="">
                                </span>
                                <a href="#" class="gla_shop_item_title">
                                    Full Midi
                                    <b>$8.28</b>
                                </a>
                                <div class="gla_shop_item_links">
                                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                </div>
                <div class="row">
                        <div class="col-md-3 gla_anim_box">
                            <div class="gla_shop_item">
                                <span class="gla_shop_item_slider">
                                    <img src="{{asset('assets/images/wedding_m/600x600/tamara-menzi-224980.jpg')}}" alt="">
                                </span>
                                <a href="#" class="gla_shop_item_title">
                                    Vintage Floral
                                    <b><s>$120.36</s> $12.96</b>
                                </a>
                                <div class="gla_shop_item_links">
                                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 gla_anim_box">
                            <div class="gla_shop_item">
                                <span class="gla_shop_item_slider">
                                    <img src="{{asset('assets/images/wedding_m/600x600/tom-pumford-229944.jpg')}}" alt="">
                                </span>
                                <a href="#" class="gla_shop_item_title">
                                    Lost Ink
                                    <b>$13.16</b>
                                </a>
                                <div class="gla_shop_item_links">
                                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 gla_anim_box">
                            <div class="gla_shop_item">
                                <span class="gla_shop_item_slider">
                                    <img src="{{asset('assets/images/wedding_m/600x600/tom-pumford-230331.jpg')}}" alt="">
                                </span>
                                <a href="#" class="gla_shop_item_title">
                                    New Look
                                    <b>$10.96</b>
                                </a>
                                <div class="gla_shop_item_links">
                                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 gla_anim_box">
                            <div class="gla_shop_item">
                                <span class="gla_shop_item_slider">
                                    <img src="{{asset('assets/images/wedding_m/600x600/tom-pumford-241908.jpg')}}" alt="">
                                </span>
                                <a href="#" class="gla_shop_item_title">
                                    Full Midi
                                    <b>$8.28</b>
                                </a>
                                <div class="gla_shop_item_links">
                                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <nav class="gla_blog_pag">
                        <ul class="pagination">
                            <li><a href="#"><i class="ti ti-angle-left"></i></a></li>
                            <li class="active" ><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#"><i class="ti ti-angle-right"></i></a></li>
                        </ul>
                    </nav>
                </div>

                <div class="col-md-3 col-md-push-1 hidden-xs hidden-sm">
                    <div class="widget">
                        <h6 class="title">About The Author</h6>
                        <p>
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua.
                        </p>
                    </div>
                    <div class="widget">
                        <h6 class="title">Search Blog</h6>
                        <form>
                            <input class="form-control" type="text" placeholder="Enter Your Keywords" />
                        </form>
                    </div>
                    <div class="widget">
                        <h6 class="title">Blog Categories</h6>
                        <ul class="list-unstyled">
                            <li>
                                <a href="#">Business</a>
                            </li>
                            <li>
                                <a href="#">Creative</a>
                            </li>
                            <li>
                                <a href="#">Photography</a>
                            </li>
                            <li>
                                <a href="#">Freelance</a>
                            </li>
                        </ul>
                    </div>
                    <div class="widget">
                        <h6 class="title">Recent Posts</h6>
                        <ul class="list-unstyled recent-posts">
                            <li>
                                <a href="#">Ut enim ad minim veniam, quis nostrud</a>
                                <span class="date">November 10, 2016</span>
                            </li>
                            <li>
                                <a href="#">Excepteur sint occaecat</a>
                                <span class="date">November 08, 2016</span>
                            </li>
                            <li>
                                <a href="#">Duis aute irure dolor in reprehenderit in voluptate velit</a>
                                <span class="date">November 05, 2016</span>
                            </li>
                        </ul>
                    </div>
                    <div class="widget bg-secondary p24">
                        <h6 class="title">Subscribe Now</h6>
                        <p>
                            Subscribe to our newsletter.
                        </p>
                        <form>
                            <input type="text" class="form-control" name="email" placeholder="Email Address" />
                            <input type="submit" class="btn btn-default no-margin" value="Subscribe" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection
