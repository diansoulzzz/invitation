@extends('layouts.default')
@section('title','Log In / Register')
@section('content')
<div class="gla_page_title gla_image_bck gla_wht_txt" data-color="#282828">
   <div class="container text-left">
      <div class="row">
         <div class="col-md-12">
            <h1 class="gla_h1_title">Log In / Register</h1>
            <h3>Please fill your data correctly</h3>
         </div>
      </div>
   </div>
</div>
<section id="gla_content" class="gla_content">
   <section class="gla_section">
      <div class="container">
         <div class="row">
            <div class="col-md-12 gla_main_sidebar">
               @if ($errors->any())
               <div class="alert alert-danger">
                  <span aria-hidden="true" class="alert-icon icon_blocked"></span><strong>Error! </strong>There are some problems.
                  <ul>
                     @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                     @endforeach
                  </ul>
               </div>
               @endif

               @if(session('status'))
               <div class="alert {{session('status')['alert']}}">
                 <span aria-hidden="true" class="alert-icon icon_blocked"></span><strong>{{session('status')['status']}} </strong>{{session('status')['message']}}
               </div>
               @endif

               <div class="tabs">
                  <ul class="nav nav-tabs bootstrap-tabs great-tabs">
                     <li class="active"><a href="#about-tab2" class="a-inv" data-toggle="tab"><i class="ti ti-thought"></i>Login</a></li>
                     <li><a href="#contact-tab2" class="a-inv" data-toggle="tab"><i class="ti ti-ruler-pencil"></i>Register</a></li>
                  </ul>
                  <div class="tab-content">
                     <div class="tab-pane fade in active" id="about-tab2">
                        <form id="form-validated" action="{{url()->current()}}" method="POST">
                           @csrf
                           <div class="form-group row">
                              <div class="col-md-3">
                              </div>
                              <div class="col-md-6">
                                 <label>Email* <small class="form-text text-muted"> Ex. daivalentinenojs@gmail.com</small></label>
                                 <input type="email" name="email" class="form-control form-opacity" required data-parsley-maxlength="85" data-parsley-minlength="5">
                              </div>
                           </div>
                           <div class="form-group row">
                              <div class="col-md-3">
                              </div>
                              <div class="col-md-6">
                                 <label>Password*</label>
                                 <input type="password" name="password" class="form-control form-opacity form-masked" data-parsley-minlength="5" required>
                              </div>
                           </div>
                           <div class="form-group row">
                              <div class="col-md-12 text-center">
                                 <input type="submit" class="btn submit" value="Login">
                              </div>
                           </div>
                        </form>
                     </div>
                     <div class="tab-pane fade" id="contact-tab2">
                        <form id="form-validated" action="{{url('register')}}" method="POST">
                           @csrf
                           <div class="form-group row">
                              <div class="col-md-6">
                                 <label>Name* <small class="form-text text-muted"> Ex. Daivalentineno Janitra Salim</small></label>
                                 <input type="text" name="name" class="form-control form-opacity" required data-parsley-maxlength="85" data-parsley-minlength="5" value="{{old('name',(isset($user)?$user->nama:''))}}">
                              </div>
                              <div class="col-md-6">
                                 <label>Telephone* <small class="form-text text-muted"> Ex. 081 1234567890</small></label>
                                 <input type="text" name="telephone" class="form-control form-opacity form-masked" data-parsley-minlength="10" data-parsley-minlength="14" required data-inputmask="'mask': '999-999999999'" data-parsley-type="digits" value="{{old('telephone',(isset($user)? $user->telepon : ''))}}">
                              </div>
                           </div>
                           <div class="form-group row">
                              <div class="col-md-6">
                                 <label>Birthday* <small class="form-text text-muted"> Ex. 01/24/1995</small></label>
                                 <input type="date" name="birthday" class="form-control form-opacity" value="{{old('birthday',(isset($user)? $user->tanggal_lahir : ''))}}" required>
                              </div>
                              <div class="col-md-6">
                                 <label>Gender* <small class="form-text text-muted"> Ex. Male</small></label>
                                 <select name="gender" class="form-control form-opacity" required>
                                 <option value="1" {{(old('gender',(isset($user)? $user->jenis_kelamin : ''))==1) ? 'selected' : ''}}>Male</option>
                                 <option value="2" {{(old('gender',(isset($user)? $user->jenis_kelamin : ''))==2) ? 'selected' : ''}}>Female</option>
                                 </select>
                              </div>
                           </div>
                           <div class="form-group row">
                              <div class="col-md-6">
                                 <label>Email* <small class="form-text text-muted"> Ex. daivalentineno@3vite.com</small></label>
                                 <input type="email" name="email" class="form-control form-opacity" data-parsley-minlength="12" data-parsley-minlength="50" value="{{old('email',(isset($user)? $user->email : ''))}}" required {{(isset($user))? 'disabled':''}}>
                              </div>
                           </div>
                           @if (!isset($user))
                           <div class="form-group row">
                              <div class="col-md-6">
                                 <label>Password*</label>
                                 <input type="password" name="password" class="form-control form-opacity" required data-parsley-minlength="6">
                              </div>
                              <div class="col-md-6">
                                 <label>Confirmation Password*</label>
                                 <input type="password" name="password_confirmation" class="form-control form-opacity" required data-parsley-minlength="6">
                              </div>
                           </div>
                           @endif
                           <div class="form-group row">
                              <div class="col-md-12">
                                 <label>Address* <small class="form-text text-muted"> Ex. North Citraland D6 Number 6, Surabaya, East Java, Indonesia</small></label>
                                 <textarea name="address" rows="3" cols="80" class="form-control form-opacity" data-parsley-minlength="15" required>{{old('address',(isset($user)? $user->alamat : ''))}}</textarea>
                              </div>
                           </div>
                           <div class="form-group row">
                              <div class="col-md-12 text-center">
                                 <input type="submit" class="btn submit" value="Save">
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
</section>
@endsection
@push('css')
<style>
   select.form-control {
   border-radius: 25px !important;
   }
   input.parsley-success,
   select.parsley-success,
   textarea.parsley-success {
   /* color: green; */
   background-color: #DFF0D8;
   border: 1px solid #D6E9C6;
   }
   input.parsley-error,
   select.parsley-error,
   textarea.parsley-error {
   /* color: red; */
   background-color: #F2DEDE;
   border: 1px solid #EED3D7;
   }
   .parsley-errors-list {
   /* margin: 0px 0 3px; */
   padding: 0;
   color: red;
   font-weight: bold;
   list-style-type: none;
   font-size: 0.9em;
   line-height: 0.9em;
   opacity: 0;
   transition: all .3s ease-in;
   -o-transition: all .3s ease-in;
   -moz-transition: all .3s ease-in;
   -webkit-transition: all .3s ease-in;
   }
   .parsley-errors-list.filled {
   opacity: 1;
   }
</style>
@endpush
@push('script')
<script src="{{asset('assets/js/jquery.inputmask.bundle.js')}}"></script>
<script src="{{asset('assets/js/parsley.min.js')}}"></script>
<script>
   $(function () {
     thisform.init();
   }), thisform = {
     init : function () {
       $('#form-validated').parsley();
       $('.form-masked').inputmask( {'autoUnmask' : true});
     },
   }
</script>
@endpush
