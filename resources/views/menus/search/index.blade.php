@extends('layouts.default', ['nav' => 'class="gla_light_nav gla_image_bck gla_wht_txt" data-color="#000"'])

@section('title','Home')
@section('content')
<section id="gla_content" class="gla_content">
  <section class="gla_section">
    <div class="container">
      <form id="form-filter" method="get" action="{{url()->current()}}">
        <div class="row">
          <div class="col-md-3">
            <!-- <div class="widget">
              <h6 class="title">Search Blog</h6>
              <form>
                <input class="form-control input-filter" type="text" name="q" placeholder="Enter Your Keywords" />
              </form>
            </div> -->
            <div class="widget">
              <h6 class="title">Event Categories</h6>
              <div class="form-group m-form__group checkbox">
                @foreach($gbl_event_category as $key => $category)
                  <label>
                    <input name="s[]" class="input-filter" value="{{$category->id}}" type="checkbox" {{(is_array(Request::input('s')) && in_array($category->id, Request::input('s')) ? 'checked' : '')}}> {{ucwords($category->nama)}}
                    <span></span>
                  </label>
                @endforeach
              </div>
            </div>
          </div>
          <div class="col-md-8">
            <div class="gla_shop_header">
              <div class="row">
                <div class="col-md-6">
                  <p>Showing {{number_format($template_designs->total())}} ({{number_format($template_designs->firstItem())}} - {{number_format($template_designs->lastItem())}} of {{number_format($template_designs->total())}})</p>
                </div>
                <div class="col-md-3 text-right pull-right">
                   <select class="form-control input-filter" id="sort_by" name="sb">
                     <option value="">Relevant</option>
                     <option value="price_asc" {{Request::input('sb')=='price_asc' ? 'selected' : ''}}>Price Low</option>
                     <option value="price_desc" {{Request::input('sb')=='price_desc' ? 'selected' : ''}}>Price High</option>
                   </select>
                </div>
              </div>
            </div>
            <div class="row">
              @foreach ($template_designs as $key => $template_design)
                <div class="col-md-4 gla_anim_box">
                <div class="gla_shop_item">
                  <!-- <span class="gla_shop_item_sale">Type Premium/No</span> -->
                  <span class="gla_shop_item_slider">
                    <img src="{{$template_design->imgpreview}}" alt="{{$template_design->nama}}">
                    <!-- <img src="{{Storage::disk('public')->url($template_design->imgpreview)}}" alt="{{$template_design->nama}}"> -->
                  </span>
                  <a href="{{url('p/'.$template_design->link_url)}}" class="gla_shop_item_title">
                    {{$template_design->nama}}
                    <!-- <b><s>$120.36</s> $12.96</b> -->
                  </a>
                  <!-- <div class="gla_shop_item_links">
                    <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                  </div> -->
                </div>
              </div>
              @endforeach
            </div>
            <nav class="gla_blog_pag">
              {{ $template_designs->appends(Request::all())->links() }}
            </nav>
          </div>
        </div>
      </form>
    </div>
  </section>
</section>
@endsection

@push('script')
<script>
$(document).ready(function() {
  $('.input-filter').on('change',function(){
    $('#form-filter').submit();
  });
});
</script>

@endpush
