@extends('layouts.default')

@section('title','Wedding Planner')

@section('music')
<div class="gla_music_icon">
    <i class="ti ti-music"></i>
</div>
<div class="gla_music_icon_cont">
    <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/108238095&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" allow="autoplay"></iframe>
</div>
@endsection

@section('content')
<div class="gla_slider gla_image_bck  gla_wht_txt gla_fixed" data-image="{{asset('assets/images/wedding_m/tamara-menzi-224980.jpg')}}" data-stellar-background-ratio="0.8">
    <div class="gla_over" data-color="#282828" data-opacity="0.4"></div>
    <div class="container">
        <div class="gla_slide_txt gla_slide_center_middle text-center">
            <div class="gla_slide_midtitle">The Stress Free Way To Plan Your Wedding</div>
            <a href="#" class="btn">Sign Up For Free Planning Tools</a>
        </div>
    </div>
    <a class="gla_scroll_down gla_go" href="#gla_content">
        <b></b>
        Scroll
    </a>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section gla_image_bck" data-color="#f5efe4">
        <div class="container text-center">
            <h2>Find Your Dream Wedding Venue And Suppliers</h2>
            <h3 class="gla_subtitle">3Vite is the platform displaying detailed supplier prices & availability</h3>
            <div class="row">
                <div class="col-md-3">
                    <select name="service" class="form-control">
                        <option disabled selected value="">What are you looking for?</option>
                        <option value="wedding-venues">Venues</option>
                        <option value="wedding-dresses">Wedding Dresses</option>
                        <option value="wedding-photography">Photography</option>
                        <option value="wedding-music-and-entertainment">Music &amp; Entertainment</option>
                        <option value="wedding-planners">Wedding Planners</option>
                        <option value="wedding-accessories">Accessories</option>
                        <option value="bridesmaid-dresses">Bridesmaid Dresses</option>
                        <option value="wedding-cakes">Cakes</option>
                        <option value="wedding-caterers">Caterers</option>
                        <option value="wedding-celebrants">Celebrants</option>
                        <option value="wedding-decor-and-styling">Decor &amp; Styling</option>
                        <option value="wedding-favours">Favours</option>
                        <option value="wedding-florists">Flowers</option>
                        <option value="groomswear">Groomswear</option>
                        <option value="wedding-hair-and-beauty">Hair &amp; Beauty</option>
                        <option value="honeymoons">Honeymoons</option>
                        <option value="wedding-jewellery">Jewellery</option>
                        <option value="wedding-marquees">Marquees</option>
                        <option value="wedding-props-and-photo-booths">Photo Booths</option>
                        <option value="stag-and-hen">Stag &amp; Hen</option>
                        <option value="wedding-stationery">Stationery</option>
                        <option value="wedding-toastmasters-and-speeches">Toastmasters</option>
                        <option value="wedding-transport">Transport</option>
                        <option value="wedding-videography">Videography</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <select name="location" class="form-control">
                        <option disabled selected value="">Where is your wedding?</option>
                        <option value="aberdeen-and-deeside">Aberdeen &amp; Deeside</option>
                        <option value="argyll">Argyll</option>
                        <option value="auvergne-rhone-alpes">Auvergne-Rh&ocirc;ne-Alpes</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <input type="text" name="date" class="form-control date_picker" placeholder="Do you know the date?">
                </div>
                <div class="col-md-3">
                    <input type="submit" class="btn btn-auto" value="Search">
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck" data-color="#fff">
        <div class="container text-center">
            <h2>Free Wedding Planning Tools</h2>
            <div class="gla_icon_boxes row">
                <div data-animation="animation_blocks" data-bottom="@class:noactive" data--100-bottom="@class:active">
                    <div class="gla_icon_box col-md-3 col-sm-6">
                        <i class="flaticon-018-smartphone"></i>
                        <div class="gla_icon_box_content">
                            <h4><b>Budget Calculator</b></h4>
                            Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do
                        </div>
                    </div>
                    <div class="gla_icon_box col-md-3 col-sm-6">
                        <i class="flaticon-027-wedding-invitation"></i>
                        <div class="gla_icon_box_content">
                            <h4><b>Guest List Tracker</b></h4>
                            Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do
                        </div>
                    </div>
                    <div class="gla_icon_box col-md-3 col-sm-6">
                        <i class="flaticon-019-plate-1"></i>
                        <div class="gla_icon_box_content">
                            <h4><b>Table Planner</b></h4>
                            Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do
                        </div>
                    </div>
                    <div class="gla_icon_box col-md-3 col-sm-6">
                        <i class="flaticon-020-gift"></i>
                        <div class="gla_icon_box_content">
                            <h4><b>Gift List</b></h4>
                            Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck" data-color="#d8e5e9">
        <div class="container text-center">
            <h2>Wedding Venues & Suppliers</h2>
            <p>From wedding venues to wedding dress boutiques, wedding invitations to wedding bands, search our comprehensive supplier listings and compare by price, availability, location and reviews</p>
            <div class="row gla_shop">
                    <div class="col-md-3 gla_anim_box">
                        <div class="gla_shop_item">
                            <span class="gla_shop_item_slider">
                                <img src="{{asset('assets/images/wedding_m/600x600/photo-nic-co-uk-nic-224379.jpg')}}" alt="">
                            </span>
                            <a href="#" class="gla_shop_item_title">
                                Wedding Dresses
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 gla_anim_box">
                        <div class="gla_shop_item">
                            <span class="gla_shop_item_slider">
                                <img src="{{asset('assets/images/wedding_m/600x600/brooke-cagle-193349.jpg')}}" alt="">
                            </span>
                            <a href="#" class="gla_shop_item_title">
                                Photography
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 gla_anim_box">
                        <div class="gla_shop_item">
                            <span class="gla_shop_item_slider">
                                <img src="{{asset('assets/images/wedding_m/600x600/mitchell-orr-179532.jpg')}}" alt="">
                            </span>
                            <a href="#" class="gla_shop_item_title">
                                Music & Entertainment
                            </a>
                        </div>
                    </div>
                    <div class="col-md-3 gla_anim_box">
                        <div class="gla_shop_item">
                            <span class="gla_shop_item_slider">
                                <img src="{{asset('assets/images/wedding_m/600x600/ben-white-131955.jpg')}}" alt="">
                            </span>

                            <a href="#" class="gla_shop_item_title">
                                Venues
                            </a>
                        </div>
                    </div>
            </div>
            <p><a href="#" class="btn">More Suppliers</a></p>
        </div>
    </section>

    <section class="gla_section">
        <div class="container text-center">
            <h2>Get Inspired</h2>
            <h3 class="gla_subtitle">Browse our blog for tips and inspiration. From Real Weddings to the latest trends, let us inspire you!</h3>
            <div class="gla_icon_boxes row text-left">
                <div class="col-md-4 col-sm-6">
                    <a href="#" class="gla_news_block">
                        <span class="gla_news_img">
                            <span class="gla_over"data-image="{{asset('assets/images/wedding_m/600x600/tom-pumford-226386.jpg')}}"></span>
                        </span>
                        <span class="gla_news_title">James & Nicola's Elegant Woodland Barn Weddin...</span>
                        <span class="gla_news_author">Real Weddings | 10 December</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa similique porro officiis nobis nulla quidem nihil iste veniam ut sit, maiores.</p>
                    </a>
                </div>
                <div class="col-md-4 col-sm-6">
                    <a href="#" class="gla_news_block">
                        <span class="gla_news_img">
                            <span class="gla_over"data-image="{{asset('assets/images/wedding_m/600x600/tamara-menzi-224980.jpg')}}"></span>
                        </span>
                        <span class="gla_news_title">6 Charitable Ways to Give Back on Your Weddin...</span>
                        <span class="gla_news_author">Things we love | 10 December</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa similique porro officiis nobis nulla quidem nihil iste veniam ut sit, maiores.</p>
                    </a>
                </div>
                <div class="col-md-4 col-sm-6">
                    <a href="#" class="gla_news_block">
                        <span class="gla_news_img">
                            <span class="gla_over"data-image="{{asset('assets/images/wedding_m/600x600/tom-pumford-226929.jpg')}}"></span>
                        </span>
                        <span class="gla_news_title">Natalia & Xavier's Parisian Engagement Shoot</span>
                        <span class="gla_news_author">Real Weddings | 10 December</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa similique porro officiis nobis nulla quidem nihil iste veniam ut sit, maiores.</p>
                    </a>
                </div>
            </div>
            <p><a href="#" class="btn">View More Inspiration</a></p>
        </div>
    </section>

    <section class="gla_section gla_image_bck" data-color="#f5efe4">
        <div class="container text-center">
            <h2>Our happy clients</h2>
            <div class="gla_team_slider_single">
                <div class="gla_reviews_item">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam magni aperiam perferendis quas eveniet. Aspernatur nisi debitis mollitia perspiciatis. Aspernatur fugiat velit vel excepturi explicabo dolore! Voluptatem, quo dolores accusantium.</p>
                    <div class="gla_reviews_item_img">
                        <img src="{{asset('assets/images/faces/100x100/1qyw0de2xc8-clem-onojeghuo.jpg')}}" alt="">
                    </div>
                    <p>Mirabelle Noelene</p>
                </div>
                <div class="gla_reviews_item">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam magni aperiam perferendis quas eveniet. Aspernatur nisi debitis mollitia perspiciatis. Aspernatur fugiat velit vel excepturi explicabo dolore! Voluptatem, quo dolores accusantium.</p>
                    <div class="gla_reviews_item_img">
                        <img src="{{asset('assets/images/faces/100x100/4g1xtqiq06o-andrew-branch.jpg')}}" alt="">
                    </div>
                    <p>Claude Allegra</p>
                </div>
                <div class="gla_reviews_item">
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam magni aperiam perferendis quas eveniet. Aspernatur nisi debitis mollitia perspiciatis. Aspernatur fugiat velit vel excepturi explicabo dolore! Voluptatem, quo dolores accusantium.</p>
                    <div class="gla_reviews_item_img">
                        <img src="{{asset('assets/images/faces/100x100/6sb3h9ekz04-matthew-kane.jpg')}}" alt="">
                    </div>
                    <p>Emmaline Cynthia</p>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection
