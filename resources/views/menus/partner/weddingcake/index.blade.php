@extends('layouts.default')

@section('title','Wedding Cakes')

@section('music')
<div class="gla_music_icon">
    <i class="ti ti-music"></i>
</div>
<div class="gla_music_icon_cont">
    <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/108238095&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" allow="autoplay"></iframe>
</div>
@endsection

@section('content')
<div class="gla_slider gla_wht_txt">
    <div class="gla_slider_carousel">
        <div class="gla_slider gla_image_bck gla_wht_txt" data-image="{{asset('assets/images/cakes/brooke-lark-212309.jpg')}}">
            <div class="gla_over" data-color="#f5efe4" data-opacity="0"></div>
            <div class="container">
                <div class="gla_slide_txt gla_slide_left_middle text-left">
                    <div class="gla_slide_midtitle">Taster Cakes</div>
                    <div class="gla_slide_subtitle">We also provide taster cakes, giving you <br>the opportunity to try our daintly but<br> delicious range of cakes first</div>
                    <p><a href="#" class="btn">Try Now</a></p>
                </div>
            </div>
        </div>
        <div class="gla_slider gla_image_bck" data-image="{{asset('assets/images/sweet/1hh57ohkdcc-toa-heftiba.jpg')}}">
            <div class="gla_over" data-color="#f5efe4" data-opacity="0"></div>
            <div class="container">
                <div class="gla_slide_txt gla_slide_right_middle text-right">
                    <div class="gla_slide_midtitle">Cutting Bars</div>
                    <div class="gla_slide_subtitle">Our cutting bars provide the extra portions, <br>so every guest has a taste of your <br>heavenly cake</div>
                    <p><a href="#" class="btn">Try Now</a></p>
                </div>
            </div>
        </div>
    </div>

    <a class="gla_scroll_down gla_go" href="#gla_content">
        <b></b>
        Scroll
    </a>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section gla_image_bck" data-color="#fff">
        <div class="container text-center">
            <h2>How to Order</h2>
            <div class="gla_icon_boxes row">
                <div data-animation="animation_blocks" data-bottom="@class:noactive" data--100-bottom="@class:active">
                    <div class="gla_icon_box col-md-3 col-sm-6">
                        <i class="flaticon-041-cake"></i>
                        <div class="gla_icon_box_content">
                            <a href="#"></a>
                            <h4><b>Pick your filling</b></h4>
                            Order tasting cakes to sample a selection of sponges for your big day
                            <br><u>Order tasting cakes</u>
                        </div>
                    </div>
                    <div class="gla_icon_box col-md-3 col-sm-6">
                        <i class="flaticon-010-love"></i>
                        <div class="gla_icon_box_content">
                            <a href="#"></a>
                            <h4><b>Choose your cake</b></h4>
                            Select from a wide range of traditional and contemporary wedding cakes
                            <br><u>Shop all wedding cakes</u>
                        </div>
                    </div>
                    <div class="gla_icon_box col-md-3 col-sm-6">
                        <i class="flaticon-016-calendar"></i>
                        <div class="gla_icon_box_content">
                            <h4><b>Order on time</b></h4>
                            Wedding cakes take a minimum of 21 days for delivery and can be ordered three months in advance
                        </div>
                    </div>
                    <div class="gla_icon_box col-md-3 col-sm-6">
                        <i class="flaticon-006-hand"></i>
                        <div class="gla_icon_box_content">
                            <a href="#"></a>
                            <h4><b>Collect your order</b></h4>
                            Your cake will be ready to collect on your chosen day
                            <br><u>Find your nearest store</u>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck" data-color="#f2f2f2">
        <div class="container text-center">
            <h2>Shop by category</h2>
            <div class="row gla_icon_boxes">
                    <div class="col-md-4 col-sm-6">
                        <a href="#" class="gla_news_block">
                            <div class="gla_news_img">
                                <div class="gla_over gla_over_img" data-color="#866a6a" data-opacity="0.2"></div>
                                <span class="gla_over" data-image="{{asset('assets/images/cakes/600x600/jeremy-wong-254456.jpg')}}"></span>
                            </div>
                            <span class="gla_news_title text-center">Traditional wedding cakes</span>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <a href="#" class="gla_news_block">
                            <div class="gla_news_img">
                                <div class="gla_over gla_over_img" data-color="#866a6a" data-opacity="0.2"></div>
                                <span class="gla_over" data-image="{{asset('assets/images/cakes/600x600/marcie-douglass-204244.jpg')}}"></span>
                            </div>
                            <span class="gla_news_title text-center">Contemporary wedding cakes</span>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <a href="#" class="gla_news_block">

                            <div class="gla_news_img">
                                <div class="gla_over gla_over_img" data-color="#866a6a" data-opacity="0.2"></div>
                                <span class="gla_over" data-image="{{asset('assets/images/cakes/600x600/brooke-lark-203842.jpg')}}"></span>
                            </div>
                            <span class="gla_news_title text-center">Cupcakes</span>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <a href="#" class="gla_news_block">

                            <div class="gla_news_img">
                                <div class="gla_over gla_over_img" data-color="#866a6a" data-opacity="0.2"></div>
                                <span class="gla_over" data-image="{{asset('assets/images/cakes/600x600/amy-treasure-69666.jpg')}}"></span>
                            </div>
                            <span class="gla_news_title text-center">Cutting bars and tasting cakes</span>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <a href="#" class="gla_news_block">

                            <div class="gla_news_img">
                                <div class="gla_over gla_over_img" data-color="#866a6a" data-opacity="0.2"></div>
                                <span class="gla_over" data-image="{{asset('assets/images/cakes/600x600/toa-heftiba-227081.jpg')}}"></span>
                            </div>
                            <span class="gla_news_title text-center">Savoury wedding cakes</span>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <a href="#" class="gla_news_block">

                            <div class="gla_news_img">
                                <div class="gla_over gla_over_img" data-color="#866a6a" data-opacity="0.2"></div>
                                <span class="gla_over" data-image="{{asset('assets/images/cakes/600x600/toa-heftiba-134254.jpg')}}"></span>
                            </div>
                            <span class="gla_news_title text-center">Chocolate wedding cakes</span>
                        </a>
                    </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck" data-color="#fff">
        <div class="container text-center">
            <h2>Finishing touches</h2>
            <p>There's so much to think about for your big day – let us help you tick off your list with our range of flowers, favours and wedding-ready clothing</p>
            <div class="gla_icon_boxes row text-left">
                <div class="col-md-4 col-sm-6">
                    <a href="#" class="gla_news_block">
                        <span class="gla_news_img">
                            <span class="gla_over" data-image="{{asset('assets/images/wedding_m/600x600/tamara-menzi-224980.jpg')}}"></span>
                        </span>
                        <span class="gla_news_title">Wedding Flowers</span>
                        <p>From bouquets to buttonholes and roses to lillies, find your dream wedding blooms</p>
                    </a>
                </div>
                <div class="col-md-4 col-sm-6">
                    <a href="#" class="gla_news_block">
                        <span class="gla_news_img">
                            <span class="gla_over" data-image="{{asset('assets/images/wedding/600x600/llwjwo200fo-drew-coffman.jpg')}}"></span>
                        </span>
                        <span class="gla_news_title">Wedding favours</span>
                        <p>Give your guests something sweet to take home on the day with our little gifts</p>
                    </a>
                </div>
                <div class="col-md-4 col-sm-6">
                    <a href="#" class="gla_news_block">
                        <span class="gla_news_img">
                            <span class="gla_over" data-image="{{asset('assets/images/wedding/600x600/iviped6yayq-scott-webb.jpg')}}"></span>
                        </span>
                        <span class="gla_news_title">3Vite wedding shop</span>
                        <p>Whether you're looking for wedding outfits or how to decorate a cake, find it all at our shop</p>
                    </a>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection
