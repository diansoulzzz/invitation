@extends('layouts.default')

@section('title','Wedding Flower')

@section('music')
<div class="gla_music_icon">
    <i class="ti ti-music"></i>
</div>
<div class="gla_music_icon_cont">
    <iframe src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/108238095&amp;auto_play=true&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true" allow="autoplay"></iframe>
</div>
@endsection

@section('content')
<div class="gla_slider gla_image_bck  gla_wht_txt gla_fixed"  data-image="{{asset('assets/images/wedding_m/sweet-ice-cream-photography-123164_2.jpg')}}" data-stellar-background-ratio="0.8">
    <div class="gla_over" data-color="#282828" data-opacity="0.5"></div>
    <div class="container">
        <div class="gla_slide_txt gla_slide_center_middle text-center">
            <p><img src="{{asset('assets/images/animations/flowers.gif')}}" data-top-bottom="@src:{{asset('assets/images/animations/flowers.gif')}}" alt="" height="150"></p>
            <div class="gla_slide_midtitle">Begin Your Dream Wedding</div>
            <p><a href="#" class="btn">More</a></p>
        </div>
    </div>
    <a class="gla_scroll_down gla_go" href="#gla_content">
        <b></b>
        Scroll
    </a>
</div>

<section id="gla_content" class="gla_content">
    <section class="gla_section gla_section_sml_padding gla_image_bck">
        <div class="container text-center">
            <div class="row gla_icon_boxes">
                    <div class="col-md-4 col-sm-6">
                        <a href="#" class="gla_news_block">
                            <div class="gla_news_img">
                                <div class="gla_over gla_over_img" data-color="#866a6a" data-opacity="0.2"></div>
                                <span class="gla_over" data-image="{{asset('assets/images/wedding_m/600x600/zivile-arunas-211579.jpg')}}"></span>
                            </div>
                            <span class="gla_news_title text-center">Bridesmaid Bouquets</span>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <a href="#" class="gla_news_block">

                            <div class="gla_news_img">
                                <div class="gla_over gla_over_img" data-color="#866a6a" data-opacity="0.2"></div>
                                <span class="gla_over" data-image="{{asset('assets/images/wedding_m/600x600/tamara-menzi-224980.jpg')}}"></span>
                            </div>
                            <span class="gla_news_title text-center">Bridal Bouquets</span>
                        </a>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <a href="#" class="gla_news_block">

                            <div class="gla_news_img">
                                <div class="gla_over gla_over_img" data-color="#866a6a" data-opacity="0.2"></div>
                                <span class="gla_over" data-image="{{asset('assets/images/wedding_m/600x600/alvin-mahmudov-181259.jpg')}}"></span>
                            </div>
                            <span class="gla_news_title text-center">Corsages & Boutonnieres</span>
                        </a>
                    </div>
            </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.2" data-image="{{asset('assets/images/wedding_m/alvin-mahmudov-181259.jpg')}}">
        <div class="gla_over" data-color="#282828" data-opacity="0.5"></div>
        <div class="container text-center">
            <h2>Our Services</h2>
            <h3 class="gla_subtitle">Elegant and Inexpensive</h3>
            <div class="gla_icon_boxes row">
                <div data-animation="animation_blocks" data-bottom="@class:noactive" data--100-bottom="@class:active">
                    <div class="gla_icon_box col-md-3 col-sm-6">
                        <i class="flaticon-024-rose"></i>
                        <div class="gla_icon_box_content">
                            <h4><b>Flower Design</b></h4>
                            Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do
                        </div>
                    </div>
                    <div class="gla_icon_box col-md-3 col-sm-6">
                        <i class="flaticon-013-bells"></i>
                        <div class="gla_icon_box_content">
                            <h4><b>Best Restaurant</b></h4>
                            Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do
                        </div>
                    </div>
                    <div class="gla_icon_box col-md-3 col-sm-6">
                        <i class="flaticon-041-cake"></i>
                        <div class="gla_icon_box_content">
                            <h4><b>Wedding Cake</b></h4>
                            Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do
                        </div>
                    </div>
                    <div class="gla_icon_box col-md-3 col-sm-6">
                        <i class="flaticon-005-camera-2"></i>
                        <div class="gla_icon_box_content">
                            <h4><b>Photoboot</b></h4>
                            Lorem ipsum dolor sit amet, consectetur adipisicingelit, sed do
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="gla_section">
        <div class="container text-center">
            <h2>Get Inspired</h2>
            <h3 class="gla_subtitle">Browse our blog for tips and inspiration. From Real Weddings to the latest trends, let us inspire you!</h3>
            <div class="gla_icon_boxes row text-left">
                <div class="col-md-4 col-sm-6">
                    <a href="#" class="gla_news_block">
                        <span class="gla_news_img">
                            <span class="gla_over" data-image="{{asset('assets/images/wedding_m/600x600/tom-pumford-226386.jpg')}}"></span>
                        </span>
                        <span class="gla_news_title">James & Nicola's Elegant Woodland Barn Weddin...</span>
                        <span class="gla_news_author">Real Weddings | 10 December</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa similique porro officiis nobis nulla quidem nihil iste veniam ut sit, maiores.</p>
                    </a>
                </div>
                <div class="col-md-4 col-sm-6">
                    <a href="#" class="gla_news_block">
                        <span class="gla_news_img">
                            <span class="gla_over" data-image="{{asset('assets/images/wedding_m/600x600/tamara-menzi-224980.jpg')}}"></span>
                        </span>
                        <span class="gla_news_title">6 Charitable Ways to Give Back on Your Weddin...</span>
                        <span class="gla_news_author">Things we love | 10 December</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa similique porro officiis nobis nulla quidem nihil iste veniam ut sit, maiores.</p>
                    </a>
                </div>
                <div class="col-md-4 col-sm-6">
                    <a href="#" class="gla_news_block">
                        <span class="gla_news_img">
                            <span class="gla_over" data-image="{{asset('assets/images/wedding_m/600x600/tom-pumford-226929.jpg')}}"></span>
                        </span>
                        <span class="gla_news_title">Natalia & Xavier's Parisian Engagement Shoot</span>
                        <span class="gla_news_author">Real Weddings | 10 December</span>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa similique porro officiis nobis nulla quidem nihil iste veniam ut sit, maiores.</p>
                    </a>
                </div>
            </div>
            <p><a href="#" class="btn">View More Inspiration</a></p>
        </div>
    </section>

    <section class="gla_section gla_image_bck" data-color="#f2f2f2">
        <div class="container text-center">
            <h2>Wedding Collection</h2>
            <h3 class="gla_subtitle">Popular in Wedding Flower Collections</h3>
            <div class="gla_portfolio grid">
                <div class="col-sm-4 gla_anim_box grid-item">
                    <div class="gla_shop_item">
                        <a href="#">
                            <img src="{{asset('assets/images/wedding_m/600x600/jez-timms-761.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            <i>Same Day, Fresh from a Florist</i><br>
                            The Classic White Bouquet<br>
                            <b>$974.99</b>
                        </a>
                        <div class="gla_shop_item_links">
                            <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item">
                    <div class="gla_shop_item">
                        <a href="#">
                            <img src="{{asset('assets/images/wedding_m/600x600/lanty-250628.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            <i>Same Day, Fresh from a Florist</i><br>
                            The Classic White Bouquet<br>
                            <b>$364.99</b>
                        </a>
                        <div class="gla_shop_item_links">
                            <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item">
                    <div class="gla_shop_item">
                        <a href="#">
                            <img src="{{asset('assets/images/wedding_m/600x600/chuttersnap-179159.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            <i>Same Day, Fresh from a Florist</i><br>
                            The Classic White Bouquet<br>
                            <b>$334.99</b>
                        </a>
                        <div class="gla_shop_item_links">
                            <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item">
                    <div class="gla_shop_item">
                        <a href="#">
                            <img src="{{asset('assets/images/wedding_m/600x600/beatriz-perez-moya-191993.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            <i>Same Day, Fresh from a Florist</i><br>
                            The Classic White Bouquet<br>
                            <b>$809.99</b>
                        </a>
                        <div class="gla_shop_item_links">
                            <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item">
                    <div class="gla_shop_item">
                        <a href="#">
                            <img src="{{asset('assets/images/wedding_m/600x600/drew-coffman-100876.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            <i>Same Day, Fresh from a Florist</i><br>
                            The Classic White Bouquet<br>
                            <b>$349.99</b>
                        </a>
                        <div class="gla_shop_item_links">
                            <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item">
                    <div class="gla_shop_item">
                        <a href="#">
                            <img src="{{asset('assets/images/wedding_m/600x600/pro-image-photography-126737.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            <i>Same Day, Fresh from a Florist</i><br>
                            The Classic White Bouquet<br>
                            <b>$284.99</b>
                        </a>
                        <div class="gla_shop_item_links">
                            <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item">
                    <div class="gla_shop_item">
                        <a href="#">
                            <img src="{{asset('assets/images/wedding_m/600x600/sweet-ice-cream-photography-87191.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            <i>Same Day, Fresh from a Florist</i><br>
                            The Classic White Bouquet<br>
                            <b>$974.99</b>
                        </a>
                        <div class="gla_shop_item_links">
                            <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item">
                    <div class="gla_shop_item">
                        <a href="#">
                            <img src="{{asset('assets/images/wedding_m/600x600/tamara-menzi-224981.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            <i>Same Day, Fresh from a Florist</i><br>
                            The Classic White Bouquet<br>
                            <b>$279.99</b>
                        </a>
                        <div class="gla_shop_item_links">
                            <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-4 gla_anim_box grid-item">
                    <div class="gla_shop_item">
                        <a href="#">
                            <img src="{{asset('assets/images/wedding_m/600x600/sweet-ice-cream-photography-123164.jpg')}}" alt="">
                        </a>
                        <a href="#" class="gla_shop_item_title">
                            <i>Same Day, Fresh from a Florist</i><br>
                            The Classic White Bouquet<br>
                            <b>$39.99</b>
                        </a>
                        <div class="gla_shop_item_links">
                            <a href="#shop"><i class="ti ti-shopping-cart"></i></a>
                        </div>
                    </div>
                </div>
             </div>
        </div>
    </section>

    <section class="gla_section gla_image_bck gla_wht_txt gla_fixed" data-stellar-background-ratio="0.8" data-image="{{asset('assets/images/wedding_m/zivile-arunas-211579.jpg')}}">
        <div class="gla_over" data-color="#282828" data-opacity="0.5"></div>
        <div class="container text-center">
            <h2>Contact Us</h2>
            <h3 class="gla_subtitle">Write</h3>
            <div class="row">
                <div class="col-md-8 col-md-push-2">
                    <form action="https://formspree.io/your@mail.com" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Your name*</label>
                                <input type="text" name="name" class="form-control form-opacity">
                            </div>
                            <div class="col-md-6">
                                <label>Your e-mail*</label>
                                <input type="text" name="email" class="form-control form-opacity">
                            </div>
                            <div class="col-md-12">
                                <input type="submit" class="btn submit" value="Send">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</section>
@endsection
