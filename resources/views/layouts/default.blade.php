<!DOCTYPE html>
<html lang="en">
  <head>
    <title>@yield('title')</title>
    @include('includes.meta')
    @stack('css')
  </head>
  <body class="gla_middle_titles">
    <div class="gla_page" id="gla_page">
      <a href="#gla_page" class="gla_top ti ti-angle-up gla_go"></a>
      @yield('music')
      @include('includes.header', ['nav' => (isset($nav) ? $nav : 'class="gla_light_nav gla_transp_nav"' )])
      @yield('content')
      @if(Request::is('/') || Request::is('partner/weddingplanner') || Request::is('partner/weddingflower') || Request::is('partner/weddingcake') || Request::is('shop') || Request::is('aboutus'))
        @include('includes.footer')
      @endif
    </div>
    @include('includes.script')
    @stack('script')
  </body>
</html>
