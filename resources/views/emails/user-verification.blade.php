Hello, <br><br>
Thank you for registering your data to 3Vite. <br>
Your register form almost done, please verify your email by clicking link below : <br>
<a href="{{url('verification/'.$eid)}}">{{url('verification/'.$eid)}}</a> <br><br>

Regards, <br>
3Vite Team
