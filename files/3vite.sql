-- MySQL Script generated by MySQL Workbench
-- Wed Aug 29 15:31:11 2018
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema 3vite
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `3vite` ;

-- -----------------------------------------------------
-- Schema 3vite
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `3vite` DEFAULT CHARACTER SET utf8 ;
USE `3vite` ;

-- -----------------------------------------------------
-- Table `3vite`.`event_category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `3vite`.`event_category` ;

CREATE TABLE IF NOT EXISTS `3vite`.`event_category` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(100) NULL DEFAULT NULL,
  `keterangan` TEXT NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `link_url` VARCHAR(200) NULL,
  `publish` TINYINT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 4
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `3vite`.`template_design`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `3vite`.`template_design` ;

CREATE TABLE IF NOT EXISTS `3vite`.`template_design` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(100) NULL DEFAULT NULL,
  `imgpreview` TEXT NULL DEFAULT NULL,
  `html` LONGTEXT NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `event_category_id` INT(11) NOT NULL,
  `link_url` VARCHAR(200) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_template_design_event_category1_idx` (`event_category_id` ASC),
  CONSTRAINT `fk_template_design_event_category1`
    FOREIGN KEY (`event_category_id`)
    REFERENCES `3vite`.`event_category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 7
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `3vite`.`users`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `3vite`.`users` ;

CREATE TABLE IF NOT EXISTS `3vite`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(200) NOT NULL,
  `email` VARCHAR(200) NOT NULL,
  `password` TEXT NOT NULL,
  `alamat` VARCHAR(500) NOT NULL,
  `telepon` VARCHAR(50) NOT NULL,
  `jenis_kelamin` INT(11) NULL DEFAULT NULL,
  `tanggal_lahir` DATE NULL DEFAULT NULL,
  `jabatan` INT(11) NOT NULL,
  `status` INT(11) NOT NULL,
  `remember_token` VARCHAR(100) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `verified_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `3vite`.`event`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `3vite`.`event` ;

CREATE TABLE IF NOT EXISTS `3vite`.`event` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(200) NOT NULL,
  `quote` TEXT NULL DEFAULT NULL,
  `author` TEXT NULL DEFAULT NULL,
  `background_home_1` TEXT NULL DEFAULT NULL,
  `background_home_2` TEXT NULL DEFAULT NULL,
  `background_schedule` TEXT NULL DEFAULT NULL,
  `backgorund_quotes` TEXT NULL DEFAULT NULL,
  `background_ending` TEXT NULL DEFAULT NULL,
  `users_id` INT(11) NOT NULL,
  `template_design_id` INT(11) NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `link_url` VARCHAR(200) NULL,
  `publish` TINYINT NULL,
  `event_category_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_event_users_idx` (`users_id` ASC),
  INDEX `fk_event_template_design1_idx` (`template_design_id` ASC),
  INDEX `fk_event_event_category1_idx` (`event_category_id` ASC),
  CONSTRAINT `fk_event_template_design1`
    FOREIGN KEY (`template_design_id`)
    REFERENCES `3vite`.`template_design` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_users`
    FOREIGN KEY (`users_id`)
    REFERENCES `3vite`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_event_category1`
    FOREIGN KEY (`event_category_id`)
    REFERENCES `3vite`.`event_category` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `3vite`.`event_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `3vite`.`event_detail` ;

CREATE TABLE IF NOT EXISTS `3vite`.`event_detail` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `event_id` INT(11) NOT NULL,
  `nama_pria` VARCHAR(200) NULL DEFAULT NULL,
  `nama_wanita` VARCHAR(200) NULL DEFAULT NULL,
  `nama_acara` VARCHAR(200) NULL DEFAULT NULL,
  `judul_cerita` TEXT NULL DEFAULT NULL,
  `cerita_awal` TEXT NULL DEFAULT NULL,
  `cerita_tengah` TEXT NULL DEFAULT NULL,
  `cerita_akhir` TEXT NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_event_detail_event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_event_detail_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `3vite`.`event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `3vite`.`event_image`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `3vite`.`event_image` ;

CREATE TABLE IF NOT EXISTS `3vite`.`event_image` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `gambar` TEXT NULL DEFAULT NULL,
  `event_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_event_image_event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_event_image_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `3vite`.`event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `3vite`.`event_schedule`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `3vite`.`event_schedule` ;

CREATE TABLE IF NOT EXISTS `3vite`.`event_schedule` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `judul` TEXT NULL DEFAULT NULL,
  `tempat` TEXT NULL DEFAULT NULL,
  `kota` VARCHAR(200) NULL DEFAULT NULL,
  `negara` VARCHAR(200) NULL DEFAULT NULL,
  `telepon` VARCHAR(45) NULL DEFAULT NULL,
  `jam_mulai` DATETIME NULL DEFAULT NULL,
  `jam_selesai` DATETIME NULL DEFAULT NULL,
  `utama` INT(11) NULL DEFAULT NULL,
  `gambar` TEXT NULL DEFAULT NULL,
  `latiitude` DECIMAL(14,8) NULL DEFAULT NULL,
  `longitude` DECIMAL(14,8) NULL DEFAULT NULL,
  `dresscode` VARCHAR(45) NULL DEFAULT NULL,
  `dresscode_detail` VARCHAR(45) NULL DEFAULT NULL,
  `gift` VARCHAR(45) NULL DEFAULT NULL,
  `gift_detail` VARCHAR(45) NULL DEFAULT NULL,
  `event_id` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_event_schedule_event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_event_schedule_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `3vite`.`event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `3vite`.`guest`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `3vite`.`guest` ;

CREATE TABLE IF NOT EXISTS `3vite`.`guest` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nama` VARCHAR(200) NOT NULL,
  `alamat` TEXT NOT NULL,
  `telepon` VARCHAR(45) NULL DEFAULT NULL,
  `email` VARCHAR(200) NULL DEFAULT NULL,
  `whatsapp` VARCHAR(200) NULL DEFAULT NULL,
  `line` VARCHAR(200) NULL DEFAULT NULL,
  `jenis` INT(11) NULL DEFAULT NULL,
  `kehadiran` INT(11) NULL DEFAULT NULL,
  `qrcode` TEXT NULL DEFAULT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  `event_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_guest_event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_guest_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `3vite`.`event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 6
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `3vite`.`template_design_detail`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `3vite`.`template_design_detail` ;

CREATE TABLE IF NOT EXISTS `3vite`.`template_design_detail` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `unique_name` VARCHAR(50) NOT NULL,
  `html` LONGTEXT NOT NULL,
  `types` VARCHAR(50) NOT NULL,
  `template_design_detail_id` INT(11) NULL DEFAULT NULL,
  `template_design_id` INT(11) NOT NULL,
  `created_at` DATETIME NULL DEFAULT NULL,
  `updated_at` DATETIME NULL DEFAULT NULL,
  `deleted_at` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_template_design_detail_template_design_detail1_idx` (`template_design_detail_id` ASC),
  INDEX `fk_template_design_detail_template_design1_idx` (`template_design_id` ASC),
  CONSTRAINT `fk_template_design_detail_template_design1`
    FOREIGN KEY (`template_design_id`)
    REFERENCES `3vite`.`template_design` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_template_design_detail_template_design_detail1`
    FOREIGN KEY (`template_design_detail_id`)
    REFERENCES `3vite`.`template_design_detail` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `3vite`.`event_design`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `3vite`.`event_design` ;

CREATE TABLE IF NOT EXISTS `3vite`.`event_design` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `template_design_detail_id` INT(11) NOT NULL,
  `event_id` INT(11) NOT NULL,
  `value` TEXT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_event_design_template_design_detail1_idx` (`template_design_detail_id` ASC),
  INDEX `fk_event_design_event1_idx` (`event_id` ASC),
  CONSTRAINT `fk_event_design_template_design_detail1`
    FOREIGN KEY (`template_design_detail_id`)
    REFERENCES `3vite`.`template_design_detail` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_design_event1`
    FOREIGN KEY (`event_id`)
    REFERENCES `3vite`.`event` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
