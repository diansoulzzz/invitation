-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: event
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `quote` text,
  `author` text,
  `background_home_1` text,
  `background_home_2` text,
  `background_schedule` text,
  `backgorund_quotes` text,
  `background_ending` text,
  `users_id` int(11) NOT NULL,
  `template_kategori_id` int(11) NOT NULL,
  `template_design_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_users_idx` (`users_id`),
  KEY `fk_event_template_kategori1_idx` (`template_kategori_id`),
  KEY `fk_event_template_design1_idx` (`template_design_id`),
  CONSTRAINT `fk_event_template_design1` FOREIGN KEY (`template_design_id`) REFERENCES `template_design` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_template_kategori1` FOREIGN KEY (`template_kategori_id`) REFERENCES `template_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,'The Fourth of July','You’re wonderful. Can you be wonderful forever?','Brennan. A true master of words','home_top/1.jpg','home_middle/1.jpg','schedule/1.jpg','quotes/1.jpg','ending/1.jpg',5,1,1,'2018-08-24 09:32:00','2018-08-24 09:32:00',NULL),(2,'The Fourth of August','You’re wonderful. Can you be wonderful forever?','Brennan. A true master of words','home_top/2.jpg','home_middle/2.jpg','schedule/2.jpg','quotes/2.jpg','ending/2.jpg',5,1,2,'2018-08-24 09:32:00','2018-08-24 09:32:00',NULL);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_detail`
--

DROP TABLE IF EXISTS `event_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `nama_pria` varchar(200) DEFAULT NULL,
  `nama_wanita` varchar(200) DEFAULT NULL,
  `nama_acara` varchar(200) DEFAULT NULL,
  `judul_cerita` text,
  `cerita_awal` text,
  `cerita_tengah` text,
  `cerita_akhir` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_detail_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_detail_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_detail`
--

LOCK TABLES `event_detail` WRITE;
/*!40000 ALTER TABLE `event_detail` DISABLE KEYS */;
INSERT INTO `event_detail` VALUES (1,1,'Marselinus Steven','Nerissa Arviana','The Wedding of Steven and Nerissa','The Fourth of July','My fiancé proposed on the Fourth of July. My mother asked us to go to the backyard to get some chairs and he took me by the shed where we could see all of the fireworks. He kissed me, then he took the ring box out of his pocket and asked me to be his wife. He was shaking a little. The proposal was a little silly but perfect, just like him.\" — Jeska Cords','Andy and Jeska met in university in the Graphic Design program. They both remember each other from orientation, but it wasn’t love at first sight, that’s for sure. Andy remembers Jeska as a ‘snooty art bitch (having been in the visual arts program at the time), and she remembers Andy being an ‘arrogant computer nerd’, boasting his knowledge of Macs over the other students.','We’re lucky enough to have nearly everything we need for our home already. And since neither of us has ever been outside of North America, we want our honeymoon to be extra special! If you want to help make it unforgettable, you can contribute using the link to the right. If you would like to give us something to update our home, we’ve compiled a short registry as well.','2018-08-24 09:32:00','2018-08-24 09:32:00',NULL),(2,2,'Dian Yulius','Alexandra','The Wedding of Yulius and Alexandra','The Fourth of August','My fiancé proposed on the Fourth of July. My mother asked us to go to the backyard to get some chairs and he took me by the shed where we could see all of the fireworks. He kissed me, then he took the ring box out of his pocket and asked me to be his wife. He was shaking a little. The proposal was a little silly but perfect, just like him.\" — Jeska Cords','Andy and Jeska met in university in the Graphic Design program. They both remember each other from orientation, but it wasn’t love at first sight, that’s for sure. Andy remembers Jeska as a ‘snooty art bitch (having been in the visual arts program at the time), and she remembers Andy being an ‘arrogant computer nerd’, boasting his knowledge of Macs over the other students.','We’re lucky enough to have nearly everything we need for our home already. And since neither of us has ever been outside of North America, we want our honeymoon to be extra special! If you want to help make it unforgettable, you can contribute using the link to the right. If you would like to give us something to update our home, we’ve compiled a short registry as well.','2018-08-24 09:32:00','2018-08-24 09:32:00',NULL);
/*!40000 ALTER TABLE `event_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_image`
--

DROP TABLE IF EXISTS `event_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` text,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_image_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_image_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_image`
--

LOCK TABLES `event_image` WRITE;
/*!40000 ALTER TABLE `event_image` DISABLE KEYS */;
INSERT INTO `event_image` VALUES (1,'event/1/1.jpg',1),(2,'event/1/2.jpg',1),(3,'event/1/3.jpg',1),(4,'event/2/1.jpg',2),(5,'event/2/2.jpg',2);
/*!40000 ALTER TABLE `event_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_schedule`
--

DROP TABLE IF EXISTS `event_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` text,
  `tempat` text,
  `kota` varchar(200) DEFAULT NULL,
  `negara` varchar(200) DEFAULT NULL,
  `telepon` varchar(45) DEFAULT NULL,
  `jam_mulai` datetime DEFAULT NULL,
  `jam_selesai` datetime DEFAULT NULL,
  `utama` int(11) DEFAULT NULL,
  `gambar` text,
  `latiitude` decimal(14,8) DEFAULT NULL,
  `longitude` decimal(14,8) DEFAULT NULL,
  `dresscode` varchar(45) DEFAULT NULL,
  `dresscode_detail` varchar(45) DEFAULT NULL,
  `gift` varchar(45) DEFAULT NULL,
  `gift_detail` varchar(45) DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_schedule_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_schedule_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_schedule`
--

LOCK TABLES `event_schedule` WRITE;
/*!40000 ALTER TABLE `event_schedule` DISABLE KEYS */;
INSERT INTO `event_schedule` VALUES (1,'The Reception','St. Thomas\'s Church','Bristol','U.K.','+1 777-123-4567','2018-08-24 12:00:00','2018-08-24 14:00:00',0,NULL,NULL,NULL,'Black And White','Formal Glamoure','Money, Cake, Flower','No Card',1,'2018-08-24 09:32:00','2018-08-24 09:32:00',NULL),(2,'The Wedding Party','Kowloon','Bristol','U.K.','+1 777-123-4567','2018-08-24 16:00:00','2018-08-24 18:00:00',1,NULL,NULL,NULL,'Black And White','Formal Glamoure','Money, Cake, Flower','No Card',1,'2018-08-24 09:32:00','2018-08-24 09:32:00',NULL),(3,'The Reception','St Yakobus Church','Surabaya','Indonesia','0313717385','2018-08-25 11:00:00','2018-08-24 12:00:00',0,NULL,NULL,NULL,'Red Glamoure','Formal','Money','No Flower',2,'2018-08-24 09:32:00','2018-08-24 09:32:00',NULL),(4,'The Wedding Party','Zhang Palace','Surabaya','Indonesia','0313717385','2018-08-25 16:00:00','2018-08-24 20:00:00',1,NULL,NULL,NULL,'Blue And White','Formal','Money','No Flower',2,'2018-08-24 09:32:00','2018-08-24 09:32:00',NULL),(5,'After Party','M1','Surabaya','Indonesia','0313717385','2018-08-25 22:00:00','2018-08-24 23:00:00',0,NULL,NULL,NULL,'Black','Formal','Money','No Flower',2,'2018-08-24 09:32:00','2018-08-24 09:32:00',NULL);
/*!40000 ALTER TABLE `event_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guest`
--

DROP TABLE IF EXISTS `guest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(45) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `whatsapp` varchar(200) DEFAULT NULL,
  `line` varchar(200) DEFAULT NULL,
  `jenis` int(11) DEFAULT NULL,
  `kehadiran` int(11) DEFAULT NULL,
  `qrcode` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_guest_event1_idx` (`event_id`),
  CONSTRAINT `fk_guest_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guest`
--

LOCK TABLES `guest` WRITE;
/*!40000 ALTER TABLE `guest` DISABLE KEYS */;
INSERT INTO `guest` VALUES (1,'Daivalentineno Janitra Salim','Bukit Palma Block D6 Nomor 6','089695969367','daivalentinenojs@gmail.com','089695969367','@daiva24',1,1,NULL,'2018-08-24 09:32:00','2018-08-24 09:32:00',NULL,1),(2,'Marselinus Steven Brian Susantio','Rungkut Mapan Barat V / 111','081357604022','marselinus_stev28@gmail.com','081357604022','@marselinus',1,1,NULL,'2018-08-24 09:32:00','2018-08-24 09:32:00',NULL,1),(3,'Handy Sunyoto Jie','Sahabat 111, Surabaya','081355685985','handy@gmail.com','081355685985','@handy',1,0,NULL,'2018-08-24 09:32:00','2018-08-24 09:32:00',NULL,2),(4,'Hadi Kusuma Poernomo','Teman 59, Surabaya','081225533685','hadi@gmail.com','081225533685','@hadi',1,1,NULL,'2018-08-24 09:32:00','2018-08-24 09:32:00',NULL,2),(5,'Andreas Tedja','Rumah 123, Surabaya','081596856368','andreas@gmail.com','081596856368','@andreas',1,1,NULL,'2018-08-24 09:32:00','2018-08-24 09:32:00',NULL,2);
/*!40000 ALTER TABLE `guest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_design`
--

DROP TABLE IF EXISTS `template_design`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `imgpreview` text,
  `html` longtext,
  `template_kategori_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_template_design_template_kategori1_idx` (`template_kategori_id`),
  CONSTRAINT `fk_template_design_template_kategori1` FOREIGN KEY (`template_kategori_id`) REFERENCES `template_kategori` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_design`
--

LOCK TABLES `template_design` WRITE;
/*!40000 ALTER TABLE `template_design` DISABLE KEYS */;
INSERT INTO `template_design` VALUES (1,'Parallax','template/1/WtuWbidcZtk4Wuu71N3MFn9ZXAObvkni9XIopA3n.jpeg','<div></div><div></div><div></div>',1,'2018-08-08 09:32:00','2018-08-08 09:32:00',NULL),(2,'Landing','template/2/pm6AsGk3ErxvyGjGOXJhvMk5lGO2No7AfTGsXAfh.jpeg','<div></div><div></div><div></div>',1,'2018-08-08 09:33:01','2018-08-08 09:33:01',NULL),(3,'Abstract Flower','template/3/4Nw1NRP6nZnnpJ53Jwe26KxlYmd4q6q24mIT7VB7.jpeg','<div></div><div></div><div></div>',1,'2018-08-08 09:42:49','2018-08-08 14:57:37',NULL),(4,'Simple Flower','template/4/4qAIRggf4gOsFMJ5rmOr4LpzHdgS1cC4Cy6q6qi8.jpeg','<div></div><div></div><div></div>',1,'2018-08-08 09:45:36','2018-08-08 09:45:37',NULL),(5,'Great Flower','template/5/NzIEfwbxHprqfQJm2gOEbxFovqNqL5npsA1iv1Jr.jpeg','<div></div><div></div><div></div>',2,'2018-08-08 09:47:30','2018-08-08 14:53:35',NULL),(6,'Animated Flower','template/6/4p9MOp18A4mxtXjKBkAfBcfkJpTdWaz9ifuwJeze.jpeg','<div></div><div></div><div></div>',1,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL);
/*!40000 ALTER TABLE `template_design` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_design_detail`
--

DROP TABLE IF EXISTS `template_design_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_design_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_name` varchar(50) NOT NULL,
  `html` longtext NOT NULL,
  `types` varchar(50) NOT NULL,
  `template_design_detail_id` int(11) DEFAULT NULL,
  `template_design_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_template_design_detail_template_design_detail1_idx` (`template_design_detail_id`),
  KEY `fk_template_design_detail_template_design1_idx` (`template_design_id`),
  CONSTRAINT `fk_template_design_detail_template_design1` FOREIGN KEY (`template_design_id`) REFERENCES `template_design` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_template_design_detail_template_design_detail1` FOREIGN KEY (`template_design_detail_id`) REFERENCES `template_design_detail` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_design_detail`
--

LOCK TABLES `template_design_detail` WRITE;
/*!40000 ALTER TABLE `template_design_detail` DISABLE KEYS */;
INSERT INTO `template_design_detail` VALUES (1,'parallax_name','<div></div>','text',NULL,1,NULL,NULL,NULL),(2,'landing_name','<div></div>','text',NULL,2,'2018-08-20 09:03:32','2018-08-20 09:22:23',NULL),(3,'parallax_quote','<div></div>','text',1,1,'2018-08-20 09:04:14','2018-08-20 09:04:14',NULL),(4,'landing_quote','<div></div>','text',2,2,'2018-08-20 09:18:16','2018-08-20 09:18:16',NULL);
/*!40000 ALTER TABLE `template_design_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_kategori`
--

DROP TABLE IF EXISTS `template_kategori`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_kategori` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_kategori`
--

LOCK TABLES `template_kategori` WRITE;
/*!40000 ALTER TABLE `template_kategori` DISABLE KEYS */;
INSERT INTO `template_kategori` VALUES (1,'Wedding Party','Wedding Party Description','2018-08-07 14:27:47','2018-08-07 14:27:47',NULL),(2,'Birthday Party','Birthday Party Description','2018-08-07 14:28:13','2018-08-07 14:28:13',NULL),(3,'Birthday Dinner','Birthday Dinner Description','2018-08-07 14:28:28','2018-08-07 14:29:01',NULL);
/*!40000 ALTER TABLE `template_kategori` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `telepon` varchar(50) NOT NULL,
  `jenis_kelamin` int(11) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jabatan` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Daivalentineno J S','daivalentineno@gmail.com','$2y$10$nrqfWygCazagzKD.JaJxH.kQn/7j2YApcy3jSuXMvFqWU4jK5784m','North Citraland D6 Number 6, Surabaya','089-695969367',1,'1995-01-24',1,1,'j4iuIMtiQMI52yxeRPY3l3Od3nlllhpRyTnxBUROiLHmBpE28K3HZjgoZ3PK','2018-08-07 14:23:12',NULL,'2018-08-07 14:25:51',NULL),(2,'Marselinus Steven Brian Susantio','steven@gmail.com','$2y$10$3y/8cJJINhzAXLjue826XuuIhrrBmVJh3qXFRf7n/3EOl20ktZ3hC','Jln Sahabat 111, Surabaya','081-357604022',1,'1996-04-28',2,1,'gK5R0lHvcbzBjLSkCYtqzwhwSYRzKmysRlqpLcxC3HpS2oTdA6NM0VioRGTp','2018-08-07 14:23:51',NULL,'2018-08-07 14:23:51',NULL),(3,'Dian Yulius','yulius@gmail.com','$2y$10$n6ycz52D0UcWXstWCn8UGe9D0LOs7IOnR6r1dmcUL.KC3tJ4fS1TO','Jln Teman 59, Surabaya','081-357859655',1,'1995-11-01',1,2,'pa69iD7Sb6BY5QPBg3FVhm2ajPNYZHkzAd4UEFdzjvBg2q6LCgZrhJ1wnSgu','2018-08-07 14:26:41','2018-08-23 08:48:33','2018-08-23 08:48:33',NULL),(4,'Daivalentineno Janitra Salim','daivalentinenojs@gmail.com','$2y$10$n6ycz52D0UcWXstWCn8UGe9D0LOs7IOnR6r1dmcUL.KC3tJ4fS1TO','North Citraland D6 Number 6, Surabaya, East Java, Indonesia','089-695969367',1,'1995-01-24',2,2,'TOOpT3Zy99z0RF9pr6n1ApOLdWYnlf80kLBqnnLaE1mJ4hxaS0SFRUfEvNJs','2018-08-23 08:57:36','2018-08-23 08:58:25','2018-08-28 15:00:43',NULL),(5,'Handy Sunyono Jie','caesardaipo@gmail.com','$2y$10$VXUGx69gUltso..Qtt8wh.uHM0z0ajl90e9sv4dLsxn.4Ri0wbTFm','Jln Titip Yuk 111, Surabaya','081-357604022',1,'1996-04-28',2,2,'Gbq4ASwUsBXzp3w1Qy2kmvahpVfFHmrcowaGY4JcV06mSR8EWvfT6ihWjaDw','2018-08-23 09:37:10','2018-08-23 09:43:38','2018-08-28 15:41:20',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-29 10:18:09
