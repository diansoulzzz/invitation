-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: 3vite
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `quote` text,
  `author` text,
  `background_home_1` text,
  `background_home_2` text,
  `background_schedule` text,
  `backgorund_quotes` text,
  `background_ending` text,
  `users_id` int(11) NOT NULL,
  `template_design_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `link_url` varchar(200) DEFAULT NULL,
  `publish` tinyint(4) DEFAULT NULL,
  `event_category_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_users_idx` (`users_id`),
  KEY `fk_event_template_design1_idx` (`template_design_id`),
  KEY `fk_event_event_category1_idx` (`event_category_id`),
  CONSTRAINT `fk_event_event_category1` FOREIGN KEY (`event_category_id`) REFERENCES `event_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_template_design1` FOREIGN KEY (`template_design_id`) REFERENCES `template_design` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_users` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_category`
--

DROP TABLE IF EXISTS `event_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `keterangan` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `link_url` varchar(200) DEFAULT NULL,
  `publish` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_category`
--

LOCK TABLES `event_category` WRITE;
/*!40000 ALTER TABLE `event_category` DISABLE KEYS */;
INSERT INTO `event_category` VALUES (4,'Wedding','nikah','2018-01-01 00:00:00','2018-01-01 00:00:00',NULL,'wedding',1),(5,'Birthday','ulang tahun','2018-01-01 00:00:00','2018-01-01 00:00:00',NULL,'birthday',1);
/*!40000 ALTER TABLE `event_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_design`
--

DROP TABLE IF EXISTS `event_design`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `template_design_detail_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `value` text,
  PRIMARY KEY (`id`),
  KEY `fk_event_design_template_design_detail1_idx` (`template_design_detail_id`),
  KEY `fk_event_design_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_design_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_event_design_template_design_detail1` FOREIGN KEY (`template_design_detail_id`) REFERENCES `template_design_detail` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_design`
--

LOCK TABLES `event_design` WRITE;
/*!40000 ALTER TABLE `event_design` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_design` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_detail`
--

DROP TABLE IF EXISTS `event_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_id` int(11) NOT NULL,
  `nama_pria` varchar(200) DEFAULT NULL,
  `nama_wanita` varchar(200) DEFAULT NULL,
  `nama_acara` varchar(200) DEFAULT NULL,
  `judul_cerita` text,
  `cerita_awal` text,
  `cerita_tengah` text,
  `cerita_akhir` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_detail_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_detail_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_detail`
--

LOCK TABLES `event_detail` WRITE;
/*!40000 ALTER TABLE `event_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_image`
--

DROP TABLE IF EXISTS `event_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gambar` text,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_image_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_image_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_image`
--

LOCK TABLES `event_image` WRITE;
/*!40000 ALTER TABLE `event_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_schedule`
--

DROP TABLE IF EXISTS `event_schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `judul` text,
  `tempat` text,
  `kota` varchar(200) DEFAULT NULL,
  `negara` varchar(200) DEFAULT NULL,
  `telepon` varchar(45) DEFAULT NULL,
  `jam_mulai` datetime DEFAULT NULL,
  `jam_selesai` datetime DEFAULT NULL,
  `utama` int(11) DEFAULT NULL,
  `gambar` text,
  `latiitude` decimal(14,8) DEFAULT NULL,
  `longitude` decimal(14,8) DEFAULT NULL,
  `dresscode` varchar(45) DEFAULT NULL,
  `dresscode_detail` varchar(45) DEFAULT NULL,
  `gift` varchar(45) DEFAULT NULL,
  `gift_detail` varchar(45) DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_event_schedule_event1_idx` (`event_id`),
  CONSTRAINT `fk_event_schedule_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_schedule`
--

LOCK TABLES `event_schedule` WRITE;
/*!40000 ALTER TABLE `event_schedule` DISABLE KEYS */;
/*!40000 ALTER TABLE `event_schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `guest`
--

DROP TABLE IF EXISTS `guest`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `guest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `alamat` text NOT NULL,
  `telepon` varchar(45) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `whatsapp` varchar(200) DEFAULT NULL,
  `line` varchar(200) DEFAULT NULL,
  `jenis` int(11) DEFAULT NULL,
  `kehadiran` int(11) DEFAULT NULL,
  `qrcode` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_guest_event1_idx` (`event_id`),
  CONSTRAINT `fk_guest_event1` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `guest`
--

LOCK TABLES `guest` WRITE;
/*!40000 ALTER TABLE `guest` DISABLE KEYS */;
/*!40000 ALTER TABLE `guest` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_design`
--

DROP TABLE IF EXISTS `template_design`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_design` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `imgpreview` text,
  `html` longtext,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  `event_category_id` int(11) NOT NULL,
  `link_url` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_template_design_event_category1_idx` (`event_category_id`),
  CONSTRAINT `fk_template_design_event_category1` FOREIGN KEY (`event_category_id`) REFERENCES `event_category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_design`
--

LOCK TABLES `template_design` WRITE;
/*!40000 ALTER TABLE `template_design` DISABLE KEYS */;
INSERT INTO `template_design` VALUES (1,'Parallax','template/1/WtuWbidcZtk4Wuu71N3MFn9ZXAObvkni9XIopA3n.jpeg','\n<div class=\"gla_slider gla_image_bck gla_wht_txt gla_fixed\"  data-image=\"{{asset(\'assets/images/wedding/mark_liz/10503327696_43f22603da_k.jpg\')}}\" data-stellar-background-ratio=\"0.8\">\n  <div class=\"gla_over\" data-color=\"#9abab6\" data-opacity=\"0.2\"></div>\n  <div class=\"container\">\n    <div class=\"gla_slide_txt gla_slide_center_bottom text-center\">\n      <div class=\"gla_slide_midtitle\">We\'re Getting Married</div>\n      <div class=\"gla_slide_subtitle\">Mark & Liz</div>\n    </div>\n  </div>\n  <a class=\"gla_scroll_down gla_go\" href=\"#gla_content\">\n  <b></b>\n  Scroll\n  </a>\n</div>\n<section id=\"gla_content\" class=\"gla_content\">\n  <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"{{asset(\'assets/images/wedding/mark_liz/10502734756_d295257d36_k.jpg\')}}\">\n    <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n    <div class=\"container text-left\">\n      <div class=\"row\">\n        <div class=\"col-md-6 col-sm-12\">\n          <h2>Our Story</h2>\n          <h3 class=\"gla_subtitle\">The Fourth of July</h3>\n          <p>My fiancé proposed on the Fourth of July. My mother asked us to go to the backyard to get some chairs and he took me by the shed where we could see all of the fireworks. He kissed me, then he took the ring box out of his pocket and asked me to be his wife. He was shaking a little. The proposal was a little silly but perfect, just like him.\" — Jeska Cords</p>\n        </div>\n      </div>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"{{asset(\'assets/images/wedding/mark_liz/10503392955_fb46d6b158_k.jpg\')}}\">\n    <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n    <div class=\"container text-right\">\n      <p><img src=\"{{asset(\'assets/images/animations/savethedate_wh.gif\')}}\" data-bottom-top=\"@src:{{asset(\'assets/images/animations/savethedate_wh.gif\')}}\" height=\"150\" alt=\"\"></p>\n      <h2>{{$date[\'date\']}}</h2>\n      <h3 class=\"gla_subtitle\">St. Thomas\'s Church,<br>Bristol, U.K.</h3>\n      <div class=\"gla_countdown\" data-year=\"{{$date[\'year\']}}\" data-month=\"{{$date[\'month\']}}\" data-day=\"{{$date[\'day\']}}\"></div>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"{{asset(\'assets/images/wedding/mark_liz/10502663484_d6f61198b4_k.jpg\')}}\">\n    <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n    <div class=\"container text-left\">\n      <p><img src=\"{{asset(\'assets/images/animations/just_wh.gif\')}}\" data-bottom-top=\"@src:{{asset(\'assets/images/animations/just_wh.gif\')}}\" height=\"150\" alt=\"\"></p>\n      <h3>You’re wonderful. <br>Can you be wonderful <br>FOREVER?\"</h3>\n      <p class=\"gla_subtitle\">— Brennan. A true master of words.</p>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"{{asset(\'assets/images/wedding/mark_liz/10503022515_d96d642143_k.jpg\')}}\">\n    <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n    <div class=\"container text-right\">\n      <div class=\"row\">\n        <div class=\"col-md-6 col-sm-12\">\n          <br>\n        </div>\n        <div class=\"col-md-6 col-sm-12\">\n          <p><img src=\"{{asset(\'assets/images/animations/rsvp_wh.gif\')}}\" data-bottom-top=\"@src:{{asset(\'assets/images/animations/rsvp_wh.gif\')}}\" height=\"180\" alt=\"\"></p>\n          <form action=\"https://formspree.io/your@mail.com\" method=\"POST\">\n            <div class=\"row\">\n              <div class=\"col-md-6\">\n                <label>Your name*</label>\n                <input type=\"text\" name=\"name\" class=\"form-control form-opacity\">\n              </div>\n              <div class=\"col-md-6\">\n                <label>Your e-mail*</label>\n                <input type=\"text\" name=\"email\" class=\"form-control form-opacity\">\n              </div>\n              <div class=\"col-md-6\">\n                <label>Will you attend?</label>\n                <input type=\"radio\" name=\"attend\" value=\"Yes, I will be there\">\n                <span>Yes, I will be there</span><br>\n                <input type=\"radio\" name=\"attend\" value=\"Sorry, I can\'t come\">\n                <span>Sorry, I can\'t come</span>\n              </div>\n              <div class=\"col-md-6\">\n                <label>Meal preference</label>\n                <select name=\"meal\" class=\"form-control form-opacity\">\n                  <option value=\"I eat anything\">I eat anything</option>\n                  <option value=\"Beef\">Beef</option>\n                  <option value=\"Chicken\">Chicken</option>\n                  <option value=\"Vegetarian\">Vegetarian</option>\n                </select>\n              </div>\n              <div class=\"col-md-12\">\n                <label>Notes</label>\n                <textarea name=\"message\" class=\"form-control form-opacity\"></textarea>\n              </div>\n              <div class=\"col-md-12\">\n                <input type=\"submit\" class=\"btn submit\" value=\"Send\">\n              </div>\n            </div>\n          </form>\n        </div>\n      </div>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"{{asset(\'assets/images/wedding/mark_liz/10503209034_51e5fc8878_k.jpg\')}}\">\n    <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n    <div class=\"container text-center\">\n      <p><img src=\"{{asset(\'assets/images/animations/thnyou_wh.gif\')}}\" alt=\"\" height=\"200\" data-bottom-top=\"@src:{{asset(\'assets/images/animations/thnyou_wh.gif\')}}\"></p>\n    </div>\n  </section>\n</section>','2018-08-08 15:00:00','2018-08-08 09:32:00',NULL,5,'parallax'),(2,'Landing','template/2/pm6AsGk3ErxvyGjGOXJhvMk5lGO2No7AfTGsXAfh.jpeg','<div class=\"gla_slider gla_image_bck  gla_wht_txt gla_fixed\"  data-image=\"{{asset(\'assets/images/wedding/carita_lee/15319475049_fc467e5874_k.jpg\')}}\" data-stellar-background-ratio=\"0.4\">\n  <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.5\"></div>\n  <div class=\"container\">\n    <div class=\"gla_slide_txt gla_slide_center_bottom text-center\">\n      <p><img src=\"{{asset(\'assets/images/badge.png\')}}\"  alt=\"\" height=\"350\"></p>\n    </div>\n  </div>\n  <a class=\"gla_scroll_down gla_go\" href=\"#gla_content\">\n  <b></b>\n  Scroll\n  </a>\n</div>\n<section id=\"gla_content\" class=\"gla_content\">\n  <section class=\"gla_section\">\n    <div class=\"container text-center\">\n      <div class=\"row text-center\">\n        <div class=\"col-md-4 gla_round_block\">\n          <div class=\"gla_round_im gla_image_bck\" data-image=\"{{asset(\'assets/images/wedding/carita_lee/600x600/15503304111_d927414239_k.jpg\')}}\"></div>\n          <h3>Carita Kimball</h3>\n        </div>\n        <div class=\"col-md-4 gla_round_block\">\n          <p><img src=\"{{asset(\'assets/images/animations/mrandmrs.gif\')}}\" data-bottom-top=\"@src:{{asset(\'assets/images/animations/mrandmrs.gif\')}}\" height=\"150\" alt=\"\"></p>\n          <h3>Are getting married<br>on {{$date[\'date\']}}</h3>\n          St. Thomas\'s Church,<br>Bristol, U.K.\n        </div>\n        <div class=\"col-md-4 gla_round_block\">\n          <div class=\"gla_round_im gla_image_bck\" data-image=\"{{asset(\'assets/images/wedding/carita_lee/600x600/15319460687_17d6396bac_k.jpg\')}}\"></div>\n          <h3>Lee Guarino</h3>\n        </div>\n      </div>\n      <div class=\"gla_countdown\" data-year=\"{{$date[\'year\']}}\" data-month=\"{{$date[\'month\']}}\" data-day=\"{{$date[\'day\']}}\"></div>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck\" data-color=\"#ecf2f0\">\n    <div class=\"container\">\n      <div class=\"row gla_auto_height\">\n        <div class=\"col-md-6 gla_image_bck\" data-color=\"#fff\">\n          <div class=\"gla_simple_block\">\n            <h2>Lee Guarino</h2>\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio mollitia, ipsa accusamus eius. Aspernatur ab sed minima, doloremque eligendi voluptatibus repellat unde, facilis natus ex ipsum eius atque suscipit fugit.</p>\n            <div class=\"gla_footer_social\">\n              <a href=\"#\"><i class=\"ti ti-facebook gla_icon_box\"></i></a>\n              <a href=\"#\"><i class=\"ti ti-instagram gla_icon_box\"></i></a>\n              <a href=\"#\"><i class=\"ti ti-google gla_icon_box\"></i></a>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-6 gla_image_bck\" data-image=\"{{asset(\'assets/images/wedding/carita_lee/15499990315_b11d1e7c7f_k.jpg\')}}\">\n        </div>\n        <div class=\"col-md-6 col-md-push-6 gla_image_bck\" data-color=\"#fff\">\n          <div class=\"gla_simple_block\">\n            <h2>Carita Kimball</h2>\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Distinctio mollitia, ipsa accusamus eius. Aspernatur ab sed minima, doloremque eligendi voluptatibus repellat unde, facilis natus ex ipsum eius atque suscipit fugit.</p>\n            <div class=\"gla_footer_social\">\n              <a href=\"#\"><i class=\"ti ti-facebook gla_icon_box\"></i></a>\n              <a href=\"#\"><i class=\"ti ti-instagram gla_icon_box\"></i></a>\n              <a href=\"#\"><i class=\"ti ti-google gla_icon_box\"></i></a>\n            </div>\n          </div>\n        </div>\n        <div class=\"col-md-6 col-md-pull-6 gla_image_bck\" data-image=\"{{asset(\'assets/images/wedding/carita_lee/15319753278_2c5a50cb74_k.jpg\')}}\">\n        </div>\n      </div>\n    </div>\n  </section>\n  <section class=\"gla_section\">\n    <div class=\"container text-center\">\n      <h2>When & Where</h2>\n      <div class=\"gla_icon_boxes row text-left\">\n        <div class=\"col-md-6 col-sm-6\">\n          <a href=\"#\" class=\"gla_news_block\">\n            <span class=\"gla_news_img\">\n            <span class=\"gla_over\" data-image=\"{{asset(\'assets/images/wedding_m/600x600/tom-pumford-226386.jpg\')}}\"></span>\n            </span>\n            <span class=\"gla_news_title\">Wedding Ceremony</span>\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa similique porro officiis nobis nulla quidem nihil iste veniam ut sit, maiores. <br><b>{{$date[\'date\']}}, St. Thomas\'s Church, <br>Bristol, U.K.</b></p>\n          </a>\n        </div>\n        <div class=\"col-md-6 col-sm-6\">\n          <a href=\"#\" class=\"gla_news_block\">\n            <span class=\"gla_news_img\">\n            <span class=\"gla_over\" data-image=\"{{asset(\'assets/images/wedding_m/600x600/tamara-menzi-224980.jpg\')}}\"></span>\n            </span>\n            <span class=\"gla_news_title\">Wedding Party</span>\n            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa similique porro officiis nobis nulla quidem nihil iste veniam ut sit, maiores. <br><b>{{$date[\'date\']}}, St. Thomas\'s Church, <br>Bristol, U.K.</b></p>\n          </a>\n        </div>\n      </div>\n    </div>\n  </section>\n  <section class=\"gla_section\">\n    <div class=\"gla_map\">\n      <iframe src=\"https://www.google.com/maps/d/embed?mid=1Db39qSrYMFfOT9Momg4l4VQEOng&amp;hl=en\" width=\"100%\" height=\"480\"></iframe>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck\">\n    <div class=\"container text-center\">\n      <h2>Registry</h2>\n      <p>We’re lucky enough to have nearly everything we need for our home already. And since neither of us has ever been outside of North America, we want our honeymoon to be extra special! If you want to help make it unforgettable, you can contribute using the link to the right. If you would like to give us something to update our home, we’ve compiled a short registry as well.</p>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.8\" data-image=\"{{asset(\'assets/images/wedding/carita_lee/15320532559_ee3e0d021d_k.jpg\')}}\">\n    <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.8\"></div>\n    <div class=\"container text-center\">\n      <h2>Will You Attend?</h2>\n      <h3 class=\"gla_subtitle\">Please sign your RSVP</h3>\n      <div class=\"row\">\n        <div class=\"col-md-8 col-md-push-2\">\n          <form action=\"https://formspree.io/your@mail.com\" method=\"POST\">\n            <div class=\"row\">\n              <div class=\"col-md-6\">\n                <label>Your name*</label>\n                <input type=\"text\" name=\"name\" class=\"form-control form-opacity\">\n              </div>\n              <div class=\"col-md-6\">\n                <label>Your e-mail*</label>\n                <input type=\"text\" name=\"email\" class=\"form-control form-opacity\">\n              </div>\n              <div class=\"col-md-6\">\n                <label>Will you attend?</label>\n                <input type=\"radio\" name=\"attend\" value=\"Yes, I will be there\">\n                <span>Yes, I will be there</span><br>\n                <input type=\"radio\" name=\"attend\" value=\"Sorry, I can\'t come\">\n                <span>Sorry, I can\'t come</span>\n              </div>\n              <div class=\"col-md-6\">\n                <label>Meal preference</label>\n                <select name=\"meal\" class=\"form-control form-opacity\">\n                  <option value=\"I eat anything\">I eat anything</option>\n                  <option value=\"Beef\">Beef</option>\n                  <option value=\"Chicken\">Chicken</option>\n                  <option value=\"Vegetarian\">Vegetarian</option>\n                </select>\n              </div>\n              <div class=\"col-md-12\">\n                <label>Notes</label>\n                <textarea name=\"message\" class=\"form-control form-opacity\"></textarea>\n              </div>\n              <div class=\"col-md-12\">\n                <input type=\"submit\" class=\"btn submit\" value=\"Send\">\n              </div>\n            </div>\n          </form>\n        </div>\n      </div>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck\" data-color=\"#fff\">\n    <div class=\"container text-center\">\n      <h2>The Wedding Party</h2>\n      <div class=\"row text-center\">\n        <div class=\"col-md-3 gla_round_block\">\n          <div class=\"gla_round_im gla_image_bck\" data-image=\"{{asset(\'assets/images/wedding/carita_lee/600x600/15319753278_2c5a50cb74_k.jpg\')}}\"></div>\n          <h4>Jessica Robbinson</h4>\n        </div>\n        <div class=\"col-md-3 gla_round_block\">\n          <div class=\"gla_round_im gla_image_bck\" data-image=\"{{asset(\'assets/images/wedding/carita_lee/600x600/15319361997_c1640fa738_k.jpg\')}}\"></div>\n          <h4>Ronald Down</h4>\n        </div>\n        <div class=\"col-md-3 gla_round_block\">\n          <div class=\"gla_round_im gla_image_bck\" data-image=\"{{asset(\'assets/images/wedding/carita_lee/600x600/15499035192_2d5a8a1e2e_k.jpg\')}}\"></div>\n          <h4>Marry Clay</h4>\n        </div>\n        <div class=\"col-md-3 gla_round_block\">\n          <div class=\"gla_round_im gla_image_bck\" data-image=\"{{asset(\'assets/images/wedding/carita_lee/600x600/15496676321_adf8baee9e_k.jpg\')}}\"></div>\n          <h4>John Pitterson</h4>\n        </div>\n      </div>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck gla_fixed gla_wht_txt\" data-stellar-background-ratio=\"0.8\" data-image=\"{{asset(\'assets/images/wedding/carita_lee/15502403291_7acc15ad4f_k.jpg\')}}\">\n    <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n    <div class=\"container text-center\">\n      <p><img src=\"{{asset(\'assets/images/animations/just_wh.gif\')}}\" data-bottom-top=\"@src:{{asset(\'assets/images/animations/just_wh.gif\')}}\" height=\"150\" alt=\"\"></p>\n      <h3>You’re wonderful. Can you be wonderful forever?\"</h3>\n      <p class=\"gla_subtitle\">— Brennan. A true master of words.</p>\n    </div>\n  </section>\n</section>','2018-08-08 15:00:00','2018-08-08 09:33:01',NULL,4,'landing'),(3,'Abstract Flower','template/3/4Nw1NRP6nZnnpJ53Jwe26KxlYmd4q6q24mIT7VB7.jpeg','<div class=\"gla_slider gla_image_bck gla_wht_txt gla_fixed\" data-image=\"{{asset(\'assets/images/wedding/mark_liz/10503327696_43f22603da_k.jpg\')}}\" data-stellar-background-ratio=\"0.8\">\n  <div class=\"gla_over\" data-color=\"#9abab6\" data-opacity=\"0.2\"></div>\n  <div class=\"container\">\n    <div class=\"gla_slide_txt gla_slide_center_bottom text-center\">\n      <div class=\"gla_slide_midtitle\">We\'re Getting Married</div>\n      <div class=\"gla_slide_subtitle\">Mark & Liz</div>\n    </div>\n  </div>\n  <a class=\"gla_scroll_down gla_go\" href=\"#gla_content\">\n  <b></b>\n  Scroll\n  </a>\n</div>\n<section id=\"gla_content\" class=\"gla_content\">\n  <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"{{asset(\'assets/images/wedding/mark_liz/10502734756_d295257d36_k.jpg\')}}\">\n    <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n    <div class=\"container text-left\">\n      <div class=\"row\">\n        <div class=\"col-md-6 col-sm-12\">\n          <h2>Our Story</h2>\n          <h3 class=\"gla_subtitle\">The Fourth of July</h3>\n          <p>My fiancé proposed on the Fourth of July. My mother asked us to go to the backyard to get some chairs and he took me by the shed where we could see all of the fireworks. He kissed me, then he took the ring box out of his pocket and asked me to be his wife. He was shaking a little. The proposal was a little silly but perfect, just like him.\" — Jeska Cords</p>\n        </div>\n      </div>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"{{asset(\'assets/images/wedding/mark_liz/10503392955_fb46d6b158_k.jpg\')}}\">\n    <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n    <div class=\"container text-right\">\n      <p><img src=\"{{asset(\'assets/images/animations/savethedate_wh.gif\')}}\" data-bottom-top=\"@src:{{asset(\'assets/images/animations/savethedate_wh.gif\')}}\" height=\"150\" alt=\"\"></p>\n      <h2></h2>\n      <h3 class=\"gla_subtitle\">St. Thomas\'s Church,<br>Bristol, U.K.</h3>\n      <div class=\"gla_countdown\" data-year=\"\" data-month=\"\" data-day=\"\"></div>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"{{asset(\'assets/images/wedding/mark_liz/10502663484_d6f61198b4_k.jpg\')}}\">\n    <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n    <div class=\"container text-left\">\n      <p><img src=\"{{asset(\'assets/images/animations/just_wh.gif\')}}\" data-bottom-top=\"@src:{{asset(\'assets/images/animations/just_wh.gif\')}}\" height=\"150\" alt=\"\"></p>\n      <h3>You’re wonderful. <br>Can you be wonderful <br>FOREVER?\"</h3>\n      <p class=\"gla_subtitle\">— Brennan. A true master of words.</p>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"{{asset(\'assets/images/wedding/mark_liz/10503022515_d96d642143_k.jpg\')}}\">\n    <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n    <div class=\"container text-right\">\n      <div class=\"row\">\n        <div class=\"col-md-6 col-sm-12\">\n          <br>\n        </div>\n        <div class=\"col-md-6 col-sm-12\">\n          <p><img src=\"{{asset(\'assets/images/animations/rsvp_wh.gif\')}}\" data-bottom-top=\"@src:{{asset(\'assets/images/animations/rsvp_wh.gif\')}}\" height=\"180\" alt=\"\"></p>\n          <form action=\"https://formspree.io/your@mail.com\" method=\"POST\">\n            <div class=\"row\">\n              <div class=\"col-md-6\">\n                <label>Your name*</label>\n                <input type=\"text\" name=\"name\" class=\"form-control form-opacity\">\n              </div>\n              <div class=\"col-md-6\">\n                <label>Your e-mail*</label>\n                <input type=\"text\" name=\"email\" class=\"form-control form-opacity\">\n              </div>\n              <div class=\"col-md-6\">\n                <label>Will you attend?</label>\n                <input type=\"radio\" name=\"attend\" value=\"Yes, I will be there\">\n                <span>Yes, I will be there</span><br>\n                <input type=\"radio\" name=\"attend\" value=\"Sorry, I can\'t come\">\n                <span>Sorry, I can\'t come</span>\n              </div>\n              <div class=\"col-md-6\">\n                <label>Meal preference</label>\n                <select name=\"meal\" class=\"form-control form-opacity\">\n                  <option value=\"I eat anything\">I eat anything</option>\n                  <option value=\"Beef\">Beef</option>\n                  <option value=\"Chicken\">Chicken</option>\n                  <option value=\"Vegetarian\">Vegetarian</option>\n                </select>\n              </div>\n              <div class=\"col-md-12\">\n                <label>Notes</label>\n                <textarea name=\"message\" class=\"form-control form-opacity\"></textarea>\n              </div>\n              <div class=\"col-md-12\">\n                <input type=\"submit\" class=\"btn submit\" value=\"Send\">\n              </div>\n            </div>\n          </form>\n        </div>\n      </div>\n    </div>\n  </section>\n  <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"{{asset(\'assets/images/wedding/mark_liz/10503209034_51e5fc8878_k.jpg\')}}\">\n    <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n    <div class=\"container text-center\">\n      <p><img src=\"{{asset(\'assets/images/animations/thnyou_wh.gif\')}}\" alt=\"\" height=\"200\" data-bottom-top=\"@src:{{asset(\'assets/images/animations/thnyou_wh.gif\')}}\"></p>\n    </div>\n  </section>\n</section>','2018-08-08 15:00:00','2018-08-08 09:42:49',NULL,5,'abstract-flower'),(4,'Simple Flower','template/4/4qAIRggf4gOsFMJ5rmOr4LpzHdgS1cC4Cy6q6qi8.jpeg','<div class=\"gla_slider gla_image_bck gla_wht_txt gla_fixed\"  data-image=\"~*01*~\" data-stellar-background-ratio=\"0.8\">\n   <div class=\"gla_over\" data-color=\"#9abab6\" data-opacity=\"0.2\"></div>\n   <div class=\"container\">\n     <div class=\"gla_slide_txt gla_slide_center_bottom text-center\">\n       <div class=\"gla_slide_midtitle\">~*02*~</div>\n       <div class=\"gla_slide_subtitle\">~*03*~</div>\n     </div>\n   </div>\n   <a class=\"gla_scroll_down gla_go\" href=\"#gla_content\">\n   <b></b>\n   Scroll\n   </a>\n </div>\n <section id=\"gla_content\" class=\"gla_content\">\n   <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"~*04*~\">\n     <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n     <div class=\"container text-left\">\n       <div class=\"row\">\n         <div class=\"col-md-6 col-sm-12\">\n           <h2>~*05*~</h2>\n           <h3 class=\"gla_subtitle\">~*06*~</h3>\n           <p>~*07*~</p>\n         </div>\n       </div>\n     </div>\n   </section>\n   <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"~*08*~\">\n     <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n     <div class=\"container text-right\">\n       <p><img src=\"~*09*~\" data-bottom-top=\"@src:~*09*~\" height=\"150\" alt=\"\"></p>\n       <h2>~*10*~</h2>\n       <h3 class=\"gla_subtitle\">~*11*~</h3>\n       <div class=\"gla_countdown\" data-year=\"~*12*~\" data-month=\"~*13*~\" data-day=\"~*14*~\"></div>\n     </div>\n   </section>\n   <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"~*15*~\">\n     <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n     <div class=\"container text-left\">\n       <p><img src=\"~*16*~\" data-bottom-top=\"@src:~*16*~\" height=\"150\" alt=\"\"></p>\n       <h3>~*17*~</h3>\n       <p class=\"gla_subtitle\">~*18*~</p>\n     </div>\n   </section>\n   <section class=\"gla_section gla_image_bck gla_wht_txt gla_fixed\" data-stellar-background-ratio=\"0.4\" data-image=\"~*19*~\">\n     <div class=\"gla_over\" data-color=\"#282828\" data-opacity=\"0.4\"></div>\n     <div class=\"container text-center\">\n       <p><img src=\"~*20*~\" alt=\"\" height=\"200\" data-bottom-top=\"@src:~*20*~\"></p>\n     </div>\n   </section>\n </section>','2018-08-08 15:00:00','2018-08-08 09:45:36',NULL,4,'simple-flower'),(5,'Great Flower','template/5/NzIEfwbxHprqfQJm2gOEbxFovqNqL5npsA1iv1Jr.jpeg','<div></div><div></div><div></div>','2018-08-08 15:00:00','2018-08-08 09:47:30',NULL,5,'great-flower'),(6,'Animated Flower','template/6/4p9MOp18A4mxtXjKBkAfBcfkJpTdWaz9ifuwJeze.jpeg','<div></div><div></div><div></div>','2018-08-08 15:00:00','2018-08-08 15:00:00',NULL,4,'animate-flower');
/*!40000 ALTER TABLE `template_design` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `template_design_detail`
--

DROP TABLE IF EXISTS `template_design_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `template_design_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `unique_name` varchar(50) NOT NULL,
  `html` longtext NOT NULL,
  `types` varchar(50) NOT NULL,
  `template_design_detail_id` int(11) DEFAULT NULL,
  `template_design_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_template_design_detail_template_design_detail1_idx` (`template_design_detail_id`),
  KEY `fk_template_design_detail_template_design1_idx` (`template_design_id`),
  CONSTRAINT `fk_template_design_detail_template_design1` FOREIGN KEY (`template_design_id`) REFERENCES `template_design` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_template_design_detail_template_design_detail1` FOREIGN KEY (`template_design_detail_id`) REFERENCES `template_design_detail` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `template_design_detail`
--

LOCK TABLES `template_design_detail` WRITE;
/*!40000 ALTER TABLE `template_design_detail` DISABLE KEYS */;
INSERT INTO `template_design_detail` VALUES (20,'~*01*~','https://cdn.newsapi.com.au/image/v1/c028cf3322130cc7d2aabdb21ea4721e','image',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(21,'~*02*~','a','text',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(22,'~*03*~','sd','text',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(23,'~*04*~','https://media.brstatic.com/2018/03/23135106/heres-how-much-you-should-expect-to-pay-this-wedding-season.jpg','image',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(24,'~*05*~','dsa','text',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(25,'~*06*~','User','text',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(26,'~*07*~','ABC','text',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(27,'~*08*~','https://peopledotcom.files.wordpress.com/2017/11/serena-williams-wedding-17.jpg','image',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(28,'~*09*~','https://www.weddingandweddingflowers.co.uk/site/images/articles/article4584.jpg','image',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(29,'~*10*~','YA','text',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(30,'~*11*~','BS','text',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(31,'~*12*~','2018','year',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(32,'~*13*~','12','month',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(33,'~*14*~','01','day',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(34,'~*15*~','https://www.glades.com.au/images/2017/glades-1707-wedding-6.jpg','image',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(35,'~*16*~','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcREV8Df7YjpQdNk4Qut8iBqo2M8HZSxgXWWIGf69rlTcvz8jbe3yQ','image',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(36,'~*17*~','asd','text',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(37,'~*18*~','asd','text',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(38,'~*19*~','https://peopledotcom.files.wordpress.com/2018/08/born-this-way-8.jpg?w=2000','image',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL),(39,'~*20*~','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQh-_yi6Bwr5RupPP-n-UCqhQCkuTZLAYmFPYgldSSDykiZeo3s','image',NULL,4,'2018-08-08 15:00:00','2018-08-08 15:00:00',NULL);
/*!40000 ALTER TABLE `template_design_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` text NOT NULL,
  `alamat` varchar(500) NOT NULL,
  `telepon` varchar(50) NOT NULL,
  `jenis_kelamin` int(11) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jabatan` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `verified_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (7,'yulius','dian.yulius@gmail.com','$2y$10$DYwlKGRrZoRpA1/nLHfcw.A/Sx7sPXP/ayzH2Y7mMsoqid5Y1sAYq','asda','123-123123213',1,'2018-08-09',2,2,NULL,'2018-08-29 08:03:57','2018-08-29 08:03:57','2018-08-29 08:03:57',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-29 17:29:38
